#include <iostream>
#include <omp.h>
#include <CImg.h>
#include <pwd.h>
#include <string>
#include <boost/filesystem.hpp>
#include <ctime>

using namespace std;
using namespace cimg_library;
using namespace boost::filesystem;

#define proc_num 4
#define verbose true

int radius = 5;
const string work_dir = string(getpwuid(getuid())->pw_dir) + "/projects/c/par/";


int get_dir_size(string dir_path)
{
    path p(dir_path);
    return int(count_if(
            directory_iterator(p),
            directory_iterator(),
            static_cast<bool(*)(const path&)>(is_regular_file) ));
}


int main(int argc, char** argv) {
    int start = clock();
    path p(work_dir + "images");
    directory_iterator end_it;
    int dir_size = get_dir_size(work_dir + "images");
    int batch_size = 0;
    if(argc == 1) {
        batch_size = lround(float(dir_size) / proc_num);
        if (batch_size * proc_num < dir_size)
            batch_size += 1;
    }
    else
        batch_size = stoi(argv[1]);
    cout << "Batch size:" << batch_size << '\n';
#pragma omp parallel num_threads(proc_num)
    {
        int pos = batch_size * omp_get_thread_num();
        directory_iterator it(p);
        for (int i = 0; i < pos;++i)
            ++it;
        for (int i = 0; it != end_it && i != batch_size; ++it,++i) {
#if verbose
            cout << "Thread " << omp_get_thread_num() << ':';
            cout << "Working on " << it->path().string() << "... ";
#endif
            CImg<unsigned char> image(it->path().string().c_str());
            image.blur(radius);
            image.save((work_dir + "blured/" + it->path().filename().string()).c_str());
#if verbose
            cout << "Done\n";
#endif
        }
    }
    cout << "Execution time:" << (clock()-start)/double(CLOCKS_PER_SEC)*1000 << "ms.";
}