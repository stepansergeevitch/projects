#include <iostream>
#include <mpi.h>
#include <CImg.h>
#include <pwd.h>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/lambda/bind.hpp>
#include <boost/math/special_functions/round.hpp>
#include <ctime>

using namespace std;
using namespace cimg_library;
using namespace boost::filesystem;
using namespace boost::lambda;
using namespace boost::math::detail;

#define verbose true

int radius = 5;
const string work_dir = string(getpwuid(getuid())->pw_dir) + "/projects/c/par/";

int get_dir_size(string dir_path)
{
    path p(dir_path);
    return int(count_if(
            directory_iterator(p),
            directory_iterator(),
            static_cast<bool(*)(const path&)>(is_regular_file) ));
}

int main(int argc, char** argv) {
    int proc_rank;
    char name[MPI_MAX_PROCESSOR_NAME + 1];
    int name_length;
    int proc_num;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &proc_num);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc_rank);
    double ms = clock();

    int dir_size = get_dir_size(work_dir + "images");
    int batch_size = 0;
    if(argc == 1) {
        batch_size = lround(float(dir_size) / proc_num);
        if (batch_size * proc_num < dir_size)
            batch_size += 1;
    }
    else
        batch_size = stoi(argv[1]);
    cout << "Batch size:" << batch_size << '\n';
    MPI_Bcast(&batch_size, 1, MPI_INT, 0, MPI_COMM_WORLD);


    int pos = batch_size * proc_rank;
    auto cur_iter = directory_iterator(path(work_dir + "images"));
    for (int i = 0;i < pos;++i)
        ++cur_iter;
    directory_iterator end_it;

    for(int i = 0;i < batch_size && cur_iter != end_it;++cur_iter,++i) {
#if verbose
        cout << "Thread " << proc_rank << ":";
        cout << "Working on " << cur_iter->path().string() << "... ";
#endif
        CImg<unsigned char> image(cur_iter->path().string().c_str());
        image.blur(radius);
        image.save((work_dir + "blured_mpi/" + cur_iter->path().filename().string()).c_str());
#if verbose
        cout << "Done working on " << cur_iter->path().string() << '\n';
#endif
    }

    MPI_Finalize();
    double t = clock() - ms;
#if verbose
    cout << "Execution time: " << fixed << (t * 1000) / double(CLOCKS_PER_SEC) << "ms\n";
#endif
}