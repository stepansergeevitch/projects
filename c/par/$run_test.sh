for BATCH_SIZE in 5 10 15 20
do
	echo "Tests with batch_size=$BATCH_SIZE"
	echo "Batсh size $BATCH_SIZE" >> results.txt
	echo $((time ./tmp/exec_1 $BATCH_SIZE) 2>&1 > /dev/null| grep real) >> results.txt
	echo "Segment finished"
	echo $((time mpirun -n 4 ./tmp/exec_2 $BATCH_SIZE) 2>&1 > /dev/null | grep real) >> results.txt
	echo "MPI finished"
	echo $((time ./tmp/exec_3 $BATCH_SIZE) 2>&1 > /dev/null| grep real) >> results.txt
	echo "OMP finished"
	echo "\n" >> results.txt
done