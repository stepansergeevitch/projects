//
// Created by Stepan Burlakov on 3/4/17.
//
#ifndef NN_TEST_MATRIX_H
#define NN_TEST_MATRIX_H
//
//#ifdef __cplusplus
//extern "C"
//{
//#endif
//#include <openblas/cblas.h>
//#ifdef __cplusplus
//}
//#endif

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
#include <math.h>
#include <memory.h>
#include <cblas/cblas.h>

#define DEBUG 0
#define THREADS_NUM  4


#define MIN(a,b) ((a) < (b))?a:b
#define MAX(a,b) ((a) > (b))?a:b;
#define IND(m,a,b) m->data[(a) * (m)->w + (b)]

struct matrixd{
    double *data;
    int h,w;
};
typedef struct matrixd matrix_s;
typedef struct matrixd* matrix;

struct sum_data_s{
    matrix a,b,res;
    int start_r,end_r;
};
typedef struct sum_data_s* sum_data;

matrix initMatrix(int h,int w)
{
    matrix res = malloc(sizeof(matrix_s));
    res->data = malloc(sizeof(double) * w * h);
    res->h = h;
    res->w = w;
    return res;
}

void deleteMatrix(matrix m)
{
    free(m->data);
    free(m);
}

void printMatrix(matrix m)
{
    printf("%dx%d\n",m->h,m->w);
    #pragma unroll
    for (int i = 0;i < m->h * m->w;++i)
    {
        printf("%f ", m->data[i]);
        if((i + 1) % m->h == 0)
            printf("\n");
    }
}

void fillRandom(matrix m)
{
    #pragma unroll
    for (int i = 0;i < m->h * m->w;++i)
            m->data[i] = rand()%1000;
}

matrix simpleCopy(matrix m)
{
    matrix res = initMatrix(m->h,m->w);
    #pragma unroll
    for (int i = 0;i < res->h * res->w;++i)
        res->data[i] = m->data[i];
    return res;
}

matrix copy(matrix m)
{
    matrix res = initMatrix(m->h,m->w);
    memcpy(res->data,m->data,sizeof(float) * res->w);
    return res;
}

int compare(matrix a,matrix b)
{
    if (a->h != b->h || a->w != b->w)
        return 0;
    for (int i = 0;i < a->h * a->w;++i)
        if(a->data[i] - b->data[i] != 0.)
            return 0;
    return 1;
}

matrix sum(matrix a,matrix b)
{
    if (a->h != b->h || a->w != b->w)
        return NULL;
    matrix res = initMatrix(a->h,a->w);
    #pragma unroll
    for (int i = 0;i < res->h;++i)
        res->data[i] = a->data[i] + b->data[i];
    return res;
}

matrix diff(matrix a,matrix b)
{
    if (a->h != b->h || a->w != b->w)
        return NULL;
    matrix res = initMatrix(a->h,a->w);
    #pragma unroll
    for (int i = 0;i < res->h;++i)
        res->data[i] = a->data[i] - b->data[i];
    return res;
}


#if DEBUG
void * _packDataForFastSum(matrix a,matrix b,matrix res,int start_r,int end_r)
{
    sum_data ptr = malloc(sizeof(struct sum_data_s));
    ptr->a = a;
    ptr->b = b;
    ptr->res = res;
    ptr->start_r = start_r;
    ptr->end_r = end_r;
    return (void*)ptr;

}

int val = 0;
double threads_sum_time = 0;
double threads_creation_time = 0;

void *_sumSubroutine(void* ptr)
{
    sum_data data = (sum_data)ptr;
    int k = val;
    ++val;
    printf("start n %d\n",k);
    #if DEBUG
    threads_sum_time -= clock();
    #endif
    #pragma unroll
    for (int i = data->start_r * data->res->w;i < data->end_r * data->res->w;++i)
    {
            data->res->data[i] = data->a->data[i] + data->b->data[i];
    }
    #if DEBUG
    threads_sum_time += clock();
    #endif
    printf("end n %d\n",k);
    return NULL;
}

matrix fastSum(matrix a,matrix b)
{
    if (a->h != b->h || a->w != b->w)
        return NULL;
    matrix res = initMatrix(a->h,a->w);
    #if DEBUG
    threads_creation_time -= clock();
    #endif
    pthread_t ** threads = malloc(sizeof(pthread_t*) * THREADS_NUM);
    for (int i = 0;i < THREADS_NUM;++i)
        threads[i] = malloc(sizeof(pthread_t*));
    int fixed_h = res->h;
    if(res->h != THREADS_NUM * (res->h/THREADS_NUM))
        fixed_h = THREADS_NUM * (res->h/THREADS_NUM + 1);
    int start_r = 0;
    int end_r = 0;
    #pragma unroll
    for(int i = 0;i < THREADS_NUM; ++i)
    {
        start_r = end_r;
        end_r = MIN(end_r + (fixed_h / THREADS_NUM),res->h);
        pthread_create(threads[i], NULL, _sumSubroutine, _packDataForFastSum(a, b, res,start_r,end_r));
    }

    #if DEBUG
    threads_creation_time += clock();
    printf("Threads creation time:%f s\n",threads_creation_time/CLOCKS_PER_SEC);
    #endif

    #pragma unroll
    for (int i = 0;i < THREADS_NUM;++i) {
        pthread_join(*threads[i], NULL);
    }

    #if DEBUG
    printf("Mutlithread sum time:%f s\n",threads_sum_time/CLOCKS_PER_SEC);
    printf("Mutlithread mean time:%f s\n",threads_sum_time/(CLOCKS_PER_SEC * THREADS_NUM));
    threads_sum_time = 0;
    #endif

    return res;
}
#endif

matrix E(int s)
{
    matrix res = initMatrix(s,s);
    for (int i = 0;i < s * s;++i)
        if (i%s == i/s)
            res->data[i] = 1;
        else
            res->data[i] = 0;
    return res;
}

matrix _E(int s)
{
    matrix res = initMatrix(s,s);
    for (int i = 0;i < s * s;++i)
        if (i%s == (s - 1) - i/s)
            res->data[i] = 1;
        else
            res->data[i] = 0;
    return res;
}


matrix simpleMult(matrix a,matrix b)
{
    if (a->w != b->h)
        return NULL;
    matrix res = initMatrix(a->h,b->w);
    #pragma unroll
    for(int i = 0;i < res->h;++i)
    {
        #pragma unroll
        for(int j = 0;j < res->w;++j )
        {
            IND(res,i,j) = 0;
            #pragma unroll
            for (int k = 0; k < a->w; ++k)
                IND(res,i,j) += IND(a,i,k) * IND(b,k,j);
        }
    }
    return res;
}

matrix mult(matrix a,matrix b)
{
    if (a->w != b->h)
        return NULL;
    matrix res = initMatrix(a->h,b->w);
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans,
                a->h, b->w, a->w, 1., (const double*)a->data, a->h, (const double*)b->data, b->h, 0., res->data, res->h);
    return res;
}

double dot(matrix a,matrix b)
{
    if (a->w != b->w || a->h != b->h)
        return NULL;
    return cblas_ddot(a->w * a->h,a->data,1,b->data,1);
}

void transposeV(matrix a)
{
    int t = a->h;
    a->h =a->w;
    a->w = t;
}

void reshape(matrix a,int h,int w)
{
    if(a->h * a->w != h*w)
        return;
    a->h = h;
    a->w = w;
}

void copyData(matrix a,matrix b)
{
    if(a->h * a->w != b->h*b->w)
        return;
    memcpy(b->data,a->data,a->h * a->w);
}

matrix m_norm(matrix a,double mean,double var)
{
    for(int i = 0;i < a->h * a->w;++i)
        a->data[i] = (a->data[i] - mean)/var;
    return a;
}

matrix m_relu(matrix a)
{
    for(int i = 0;i < a->h * a->w;++i)
        a->data[i] = MAX(a->data[i],0)
    return a;
}

matrix m_tanh(matrix a)
{
    for(int i = 0;i < a->h * a->w;++i)
        a->data[i] = tanh(a->data[i]);
    return a;
}

matrix m_vectorize(matrix a)
{
    reshape(a,a->h * a->w,1);
}



#endif //NN_TEST_MATRIX_H
