//
// Created by Stepan Burlakov on 3/7/17.
//

#ifndef LAYER_TEST_NN_H
#define LAYER_TEST_NN_H

#include "matrix.h"
#include "list.h"

#define F(l,f) (l)->flags & (f)

#define L_FINAL 0x1
#define L_RELU 0x2
#define L_TANH 0x4
#define L_SIGMOID 0x8
#define L_LINEAR 0x10
#define L_D_ACT L_RELU
#define L_MAXPOOL 0x20
#define L_BATCHNORM 0x40
#define L_MSELOSS 0x80
#define L_LOGLOSS 0x100
#define L_D_LOSS L_MSELOSS

struct layer_s{
    matrix W, input, output,W_grad,o_grad,i_grad;
    int in_s,out_s;
    int flags;
    double batch_mean;
    double batch_var;
};

typedef struct layer_s* layer;
typedef struct mlp_s* mlp;

layer initLayer(int i_s,int o_s,int flags)
{
    if(! flags & (L_RELU | L_TANH | L_SIGMOID | L_LINEAR))
        flags = flags|L_D_ACT;
    if(! flags & (L_MSELOSS | L_LOGLOSS))
        flags = flags | L_D_LOSS;
    layer res = malloc(sizeof(struct later_s));
    res->flags = flags;
    res->input = initMatrix(i_s,1);
    res->i_grad = initMatrix(1,i_s);
    res->W = initMatrix(o_s,i_s);
    res->W_grad = initMatrix(o_s,i_s);
    if(flags&L_FINAL)
    {
        res->output = initMatrix(o_s,1);
        res->o_grad = initMatrix(1,o_s);
    }
    return res;
}


void forwardStep(layer l)
{
    l->output = mult(l->W,l->input);
    if (F(l,L_RELU))
        m_relu(l->output);
    else if (F(l,L_RELU))
        m_tanh(l->output);
    if (F(l,L_BATCHNORM)){}
        //m_norm(l->output,l->batch_mean,l->batch_var);
}

void backwardStep(layer l)
{
    l->W_grad = sum(l->W_grad,mult(l->input,l->o_grad));
    l->i_grad = mult(l->o_grad,l->W);
}

#endif //LAYER_TEST_NN_H
