#include "matrix.h"


int main()
{
    srand((unsigned int)(time(NULL)));
    int n = 500;
    matrix a,b;
    a = initMatrix(n,n);
    fillRandom(a);
    b = initMatrix(n,n);
    fillRandom(b);

    clock_t begin = clock();
    matrix c1 = simpleMult(a,b);
    clock_t w_time = clock() - begin;
    printf("Simple mult:Time spent:%f s\n",(double)w_time/CLOCKS_PER_SEC);

    begin = clock();
    matrix c2 = fastMult(a,b);
    w_time = clock() - begin;
    printf("Parallel mult: Time spent:%f s\n",(double)w_time/CLOCKS_PER_SEC);

    printf("Matrix comparison:%d\n",compare(c1,c2));

//    printMatrix(a);
//    printMatrix(b);
//    printMatrix(c1);
//    printMatrix(c2);

    deleteMatrix(a);
    deleteMatrix(b);
    deleteMatrix(c1);
    deleteMatrix(c2);
}