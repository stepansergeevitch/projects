//
// Created by Stepan Burlakov on 3/7/17.
//

#ifndef LIST_TEST_NN_H
#define LIST_TEST_NN_H

#include <stdlib.h>
#include <memory.h>

struct node_s{
    struct node_s *next,*prev;
    void* data;
};

typedef struct node_s* node;

node initNode(node next,node prev,void *data)
{
    node res = malloc(sizeof(struct node_s));
    res->next = next;
    res->prev = prev;
    res->data = data;
    return res;
}

void deleteNode(node a)
{
    free(a->data);
    free(a);
}

void deleteList(node head)
{
    if(head == NULL)
        return;
    node next;
    while (head->next != NULL)
    {
        next = head->next;
        deleteNode(head);
        head = next;
    }
}

node nextN(node cur)
{
    if (cur == NULL)
        return NULL;
    return cur->next;
}

node prevN(node cur)
{
    if (cur == NULL)
        return NULL;
    return cur->prev;
}

void insertItem(node cur,void* data)
{
    node next = NULL;
    if(cur->next != NULL)
        next = cur->next;
    cur->next = initNode(next,cur,data);
    cur = nextN(cur);
}

node copyNode(node n,size_t data_size)
{
    if(n == NULL)
        return NULL;
    node res = initNode(NULL,NULL,n->data);
    node r_cur = res;
    node cur = n;
    while(nextN(cur))
    {
        r_cur->next = initNode(r_cur,NULL,malloc(data_size));
        memcpy(r_cur->next->data,nextN(cur)->data,data_size);
        cur = nextN(cur);
        r_cur = nextN(r_cur);
    }
    return res;
}





#endif //LIST_TEST_NN_H
