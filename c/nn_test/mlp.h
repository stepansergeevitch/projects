//
// Created by Stepan Burlakov on 3/7/17.
//

#ifndef NN_TEST_MLP_H
#define NN_TEST_MLP_H

#include "layer.h"

struct mlp_s{
    node head,cur;
    int flags;
};

#define P_CONNECTED 0x1

void connectLayers(mlp);

mlp initMlp(node layers,int flags)
{
    mlp res = malloc(sizeof(struct mlp_s));
    res->head = copyNode(layers,sizeof(struct layer_s));
    res->cur = copyNode(layers,sizeof(struct layer_s));
    if (flags & P_CONNECTED)
        flags ^= P_CONNECTED;
    res->flags = flags;
    connectLayers(res);
    return res;
}

void connectLayers(mlp p)
{
    p->cur = p->head;
    while(nextN(p->cur))
    {
        if(p->cur->data == NULL ||
           nextN(p->cur)->data == NULL ||
           ((layer) nextN(p->cur)->data)->input == NULL ||
           ((layer) nextN(p->cur)->data)->i_grad == NULL)
            return;
        ((layer) p->cur->data)->output = ((layer) nextN(p->cur)->data)->input;
        ((layer) p->cur->data)->o_grad = ((layer) nextN(p->cur)->data)->i_grad;
        p->cur = nextN(p->cur);
    }
    if(! p->flags & P_CONNECTED)
        p->flags ^= P_CONNECTED;
}

#endif //NN_TEST_MLP_H
