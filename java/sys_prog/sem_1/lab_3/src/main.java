/**
 * Created by stepan on 11/28/16.
 */
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

enum TokType {
        COMMENT("#.*"),
        STRING("['|\"].*?['|\"]"),
        KEYWORD("(?<!\\w)(?:(?<=import )\\*|False|class|finally|is|return|None|continue|for|lambda|try|True|def|from|nonlocal|while|del|global|with|as|elif|if|yield|assert|else|import|pass|break|except|in|raise)(?!\\w)"),
        OPERATOR("<>|[*|/|+|\\-|<|>|=|@|%]|not|and|or|<=|>="),
        TYPE("(?<!\\w)(?:int|long|float|complex)(?!\\w)"),
        NUMBER("0?[x|b]?[0-9]+[.]?[0-9]*e?-?[0-9]*L?"),
        IDENTIFIER("[a-zA-Z0-9_]+"),
        BRACKET("[\\[|\\]|(|)]"),
        PUNCTUATION("[.|,|:|;|.]"),
        WHITESPACE("\\s+"),
        UNEXPECTED("\\S+");

        public final String pattern;

        TokType(String pattern) {
            this.pattern = pattern;
        }
    }

class Token{
    public TokType type;
    public String text;

    public Token(TokType t, String tx){
        type = t;
        text = tx;
    }

    public String toString(){
        return String.format("%s:[%s]", type.name(),text);
    }
}


public class main {
    static ArrayList<Token> parse(String line){
        ArrayList<Token> tokens = new ArrayList<>();

        StringBuilder tokenPatternsBuffer = new StringBuilder();
        for (TokType tokenType : TokType.values())
            tokenPatternsBuffer.append(String.format("|(?<%s>%s)", tokenType.name(), tokenType.pattern));
        Pattern tokenPatterns = Pattern.compile(tokenPatternsBuffer.substring(1));

        Matcher matcher = tokenPatterns.matcher(line);
        while (matcher.find()) {
            for (TokType tokenType: TokType.values()) {
                if (matcher.group(tokenType.name()) != null) {
                    if (tokenType != TokType.WHITESPACE) {
                        tokens.add(new Token(tokenType, matcher.group(tokenType.name())));
                    }
                    break;
                }
            }
        }
        return tokens;
    }

    public static void main(String[] args){
        try {
            File f1 = new File(System.getProperty("user.dir") + "/input.txt");
            Scanner s = new Scanner(f1);
            File f2 = new File(System.getProperty("user.dir") + "/output.txt");
            BufferedWriter w = new BufferedWriter(new FileWriter(f2));
            String input;
            while(s.hasNext()) {
                input = s.nextLine();
                ArrayList<Token> t = parse(input);
                for (Token i: t) {
                    w.write(i.toString() + '\n');
                }
            }
            w.close();
        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }
    }

}
