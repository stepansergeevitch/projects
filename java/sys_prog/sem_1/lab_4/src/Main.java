import java.util.*;
import java.io.*;

class Message{
    boolean que = false;
    public synchronized void Question(String s){
        if (que){
            try{ wait(); }
            catch(InterruptedException ex) {ex.getMessage();}
        }
        System.out.println(s);
        que = true;
        notify();
    }
    public synchronized void Answer(String s){
        if (!que){
            try { wait(); }
            catch(InterruptedException ex) { ex.getMessage(); }
        }
        System.out.println(s);
        que = false;
        notify();
    }
}
class GThread implements Runnable{
    Message m;
    int x;
    public static boolean res;
    public Thread t;
    public GThread(int xx, Message mm){ 
        x = xx; 
        m = mm; 
        t = new Thread(Main.all, this, "G");
        t.start(); 
    }
    @Override
    public void run(){
        m.Answer("Hello! I'm going to find the sum of all binary digits of x .\n");
        res = g(x);
    }
    private boolean g(int x){
        int state = 1;
        if (FThread.res == false) return false;
        long start = System.currentTimeMillis();
        long end = System.currentTimeMillis();
        while(true){
            if (FThread.res == false) return false;
            if (state == 2) continue;
            end = System.currentTimeMillis();
            if ((end - start) > 5000){
                System.out.println("Thread asks: Do You still want me to continue?");
                System.out.println("Please input 0 for NO, 1 for YES, 2 for YES and don't ask again");
                try{
                    state = Integer.parseInt(Main.br.readLine());
                }
                catch(Exception ex){
                    System.out.println(ex.getMessage());
                }
                if (state == 0) break;
                start = System.currentTimeMillis();
            }
        }
        if (FThread.res == false) return false;
        boolean cur = false;
        int t = x;
        while (t > 0)
        {
            cur ^= t%2 == 1;
            t/=2;
        }
        return cur;
    }
}
class FThread implements Runnable{
    Message m;
    int x;
    public static boolean res;
    public Thread t;
    public FThread(int xx, Message mm){ 
        x = xx; 
        m = mm; 
        res = true;
        t = new Thread(Main.all, this, "F");
        t.start();
    }
    @Override
    public void run(){
        m.Question("Hello! I'm going to find if x is even.");
        res = f(x);
    }
    private boolean f(int x){
        return x%2 == 0;
    }
}
public class Main {

    /**
     * @param args the command line arguments
     */
    public static ThreadGroup all = new ThreadGroup("ALL");
    public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    public static void main(String[] args) {
        // TODO code application logic here
        
        try{
            System.out.print("Enter x:");
            int x = Integer.parseInt(br.readLine());
            Message m = new Message();
            FThread ff = new FThread(x, m);
            GThread gg = new GThread(x, m);
            while(true){
                if (all.activeCount() == 0) break;
            }
            System.out.println(FThread.res && GThread.res);
        }
        catch(Exception ex) { System.out.println(ex.getMessage()); }
        
    }
    
}
