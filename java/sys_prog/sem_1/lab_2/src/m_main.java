/**
 * Created by stepan on 10/24/16.
 */

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Stack;

class Pair<T1,T2>{
    public T1 f;
    public T2 s;

    public Pair(T1 _f,T2 _s) {
        f = _f;
        s = _s;
    }


}

class T extends Object
{
    public int s1,s2;
    char a;

    public T(int _s1,char _a,int _s2)
    {
        s1 = _s1;
        a = _a;
        s2 = _s2;
    }

    @Override
    public boolean equals(Object t2){
        if(t2 == null)
            return false;
        if(t2 instanceof T)
            return s1 == ((T)t2).s1 && a == ((T)t2).a && s2 == ((T)t2).s2;
        return false;
    }
}

class Automaton {
    HashSet<T> func;
    HashSet<Integer> s_states;
    HashSet<Integer> f_states;
    int al_size;

    public Automaton() {
        func = new HashSet<>();
        s_states = new HashSet<>();
        f_states = new HashSet<>();
        al_size = 0;
    }

    public void read(Scanner s) {
        al_size = s.nextInt();
        s.nextInt();
        s_states.add(s.nextInt());
        int size = s.nextInt();
        for(int i = 0;i < size;++i)
            f_states.add(s.nextInt());
        size = s.nextInt();
        for (int i = 0; i < size; ++i) {
            T tmp = new T(s.nextInt(), s.next().charAt(0), s.nextInt());
            if(!f_states.contains(tmp.s1))
                func.add(tmp);
        }
    }

    public void print(BufferedWriter w) {
        try {
            w.write("Start states:");
            for(int i:s_states)
                w.write(String.format("%d ",i));
            w.write("\nFinal states:");
            for(int i: f_states)
                w.write(String.format("%d ",i));
            w.write("\nFunction:\n");
            for (T i : func) {
                w.write(String.format("%d %c -> %d\n", i.s1, i.a, i.s2));
            }
        }
        catch(Exception ex)
        {
            System.out.print(ex.getMessage());
        }
    }

    private void reverse() {
        for (T i : func) {
            i.s1 ^= i.s2;
            i.s2 ^= i.s1;
            i.s1 ^= i.s2;
        }
        HashSet<Integer> tmp = s_states;
        s_states = f_states;
        f_states = tmp;
    }

    public void determine() {
        HashSet<T> n_func = new HashSet<>();

        ArrayList<HashSet<Integer>> states = new ArrayList<>();
        Stack<Integer> new_states = new Stack<>();
        states.add(new HashSet<>(s_states));

        new_states.push(0);
        while (!new_states.empty()) {
            int cur_ind = new_states.pop();
            HashSet<Integer> tmp_set = new HashSet<>();
            HashSet<Integer> cur_set = states.get(cur_ind);
            for (char a = 'a'; a < 'a' + al_size; ++a) {
                tmp_set.clear();
                    for (T i : func) {
                        if (i.a == a && cur_set.contains(i.s1))
                            tmp_set.add(i.s2);
                }
                if (!tmp_set.isEmpty()) {
                    boolean exists = false;
                    int ind = -1;
                    for (int i = 0; i < states.size(); ++i) {
                        if (states.get(i).equals(tmp_set)) {
                            exists = true;
                            ind = i;
                        }
                    }
                    if (exists)
                        n_func.add(new T(cur_ind + 1, a, ind + 1));
                    else {
                        states.add(new HashSet<>(tmp_set));
                        ind = states.size() - 1;
                        n_func.add(new T(cur_ind + 1, a, ind + 1));
                    }
                    if(!exists)
                        new_states.push(ind);
                }
            }
        }
        HashSet<Integer> n_f_states = new HashSet<>();
        for(int i: f_states)
            for(int j = 0;j < states.size();++j)
                if(states.get(j).contains(i))
                    n_f_states.add(j + 1);
        f_states = n_f_states;
        s_states.clear();
        s_states.add(1);
        func = n_func;
    }

    public void minimize() {
        reverse();
        determine();
        reverse();
        determine();
    }

    private boolean s_comparison(Automaton a2) {
        HashMap<Integer,Integer> accordance = new HashMap<>();
        HashSet<Integer> checked = new HashSet();
        checked.add((Integer)s_states.toArray()[0]);
        accordance.put((Integer)s_states.toArray()[0],(Integer)a2.s_states.toArray()[0]);
        Stack<Pair<Integer,Integer>> st = new Stack<>();
        st.push(new Pair(s_states.toArray()[0],a2.s_states.toArray()[0]));

        while(!st.empty())
        {
            Pair<Integer,Integer> cur_s = st.pop();
            Pair<Integer,Integer> tmp_s = new Pair(0,0);
            for(char a = 'a';a < al_size + 'a';++a) {
                for (T i : func)
                    if (i.a == a && i.s1 == cur_s.f)
                        tmp_s.f = i.s2;
                for (T i : a2.func)
                    if (i.a == a && i.s1 == cur_s.s)
                        tmp_s.s = i.s2;
                if(tmp_s.f == 0)
                {
                    if(tmp_s.s == 0)
                        continue;
                    else
                        return false;
                }
                else {
                    if (accordance.containsKey(tmp_s.f)) {
                        if (accordance.get(tmp_s.f) != tmp_s.s)
                            return false;
                    } else {
                        accordance.put(tmp_s.f, tmp_s.s);
                    }
                    if (!checked.contains(tmp_s.f)) {
                        st.push(new Pair(tmp_s.f, tmp_s.s));
                        checked.add(tmp_s.f);
                    }
                }
            }

        }
        return true;
    }

    public boolean comparison(Automaton a2) {
        minimize();
        a2.minimize();
        return s_comparison(a2);
    }

}



public class m_main{

    public static void main(String[] args)
    {
        try {
            File f1 = new File(System.getProperty("user.dir") + "/input.txt");
            Scanner s = new Scanner(f1);
            File f2 = new File(System.getProperty("user.dir") + "/output.txt");
            BufferedWriter w = new BufferedWriter(new FileWriter(f2));
            Automaton a1 = new Automaton();
            a1.read(s);
            a1.determine();
            a1.print(w);
            w.close();

        }
        catch(Exception ex)
        {
            System.out.println(ex.getMessage());
        }




    }
}
