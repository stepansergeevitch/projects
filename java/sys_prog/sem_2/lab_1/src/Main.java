import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import JavaTeacherLib.*;

class myLang {
    private LinkedList<Node> lang;
    private LinkedList<TableNode> lexTable;

    myLang(LinkedList<Node> _lang, LinkedList<TableNode> _lexTable){
        this.lang = _lang;
        this.lexTable = _lexTable;
    }
}

class solver{
    int lowCode = -2147483648;
    int upCode = 268435456;

    int lexCode(String lexema, int lexemaClass, LinkedList<TableNode> lexTable) {
        Iterator it = lexTable.iterator();

        TableNode tmp;
        do {
            if(!it.hasNext()) {
                return 0;
            }

            tmp = (TableNode)it.next();
        } while(!tmp.getLexemaText().equals(lexema) || (tmp.getLexemaCode() & -16777216) != lexemaClass);

        return tmp.getLexemaCode();
    }

    public myLang readGrammar(String fname) {
        char[] lex = new char[180];
        int[] rule = new int[80];

        LinkedList<Node> lang = new LinkedList<Node>();
        LinkedList<TableNode> lexTable = new LinkedList<TableNode>();

        BufferedReader stream;
        try {
            stream = new BufferedReader(new FileReader(fname.trim()));
        } catch (FileNotFoundException var24) {
            System.out.print("Файл:" + fname.trim() + " не відкрито\n");
            return null;
        }

        for(int ii = 0; ii < lex.length; ++ii) {
            lex[ii] = 0;
        }

        int pos = 0;
        int q = 0;
        int posRule = 0;
        int lineInd = 0;
        String readLine = null;
        int readPos = 0;
        int readLen = 0;
        boolean cond = true;

        try {
            int newLexCode;
            TableNode nodeTmp;
            Node node;
            while(stream.ready()) {
                if(readLine == null || readPos >= readLen) {
                    readLine = stream.readLine();
                    if(lineInd == 0 && readLine.charAt(0) == '\ufeff') {
                        readLine = readLine.substring(1);
                    }

                    readLen = readLine.length();
                    ++lineInd;
                }

                for(readPos = 0; readPos < readLen; ++readPos) {
                    char curChar = readLine.charAt(readPos);
                    String strTmp;
                    switch(q) {
                        case 0:
                            if(curChar == 32 || curChar == 9 || curChar == 13 || curChar == 10 || curChar == 8) {
                                break;
                            }

                            if(readPos == 0 && posRule > 0 && (curChar == 33 || curChar == 35)) {
                                node = new Node(rule, posRule);
                                lang.add(node);
                                if(curChar == 33) {
                                    posRule = 1;
                                    break;
                                }

                                posRule = 0;
                            }
                            pos = 1;
                            lex[pos] = curChar;
                            if(curChar == 35) {
                                q = 1;
                            } else if(curChar == 92) {
                                --pos;
                                q = 3;
                            } else {
                                q = 2;
                            }
                            break;
                        case 1:
                            lex[pos++] = curChar;
                            if(curChar != 35 && readPos != 0) {
                                break;
                            }

                            strTmp = new String(lex, 0, pos);
                            nodeTmp = new TableNode(strTmp, lowCode);
                            cond = true;

                            for (TableNode it: lexTable){
                                if(nodeTmp.equals(it)) {
                                    cond = false;
                                    break;
                                }
                            }

                            if(cond) {
                                lexTable.add(nodeTmp);
                            }

                            newLexCode = lexCode(strTmp, lowCode, lexTable);
                            rule[posRule++] = newLexCode;
                            pos = 0;
                            q = 0;
                            break;
                        case 2:
                            if(curChar == 92) {
                                --pos;
                                q = 3;
                            } else {
                                if(curChar != 32 && readPos != 0 && curChar != 35 && curChar != 13 && curChar != 9) {
                                    lex[pos++] = curChar;
                                    continue;
                                }

                                strTmp = new String(lex, 0, pos);
                                nodeTmp = new TableNode(strTmp, upCode);
                                cond = true;

                                for (TableNode it: lexTable){
                                    if(nodeTmp.equals(it)) {
                                        cond = false;
                                        break;
                                    }
                                }

                                if(cond) {
                                    lexTable.add(nodeTmp);
                                }

                                newLexCode = lexCode(strTmp, upCode, lexTable);
                                rule[posRule++] = newLexCode;
                                pos = 0;
                                q = 0;
                                --readPos;
                            }
                            break;
                        case 3:
                            lex[pos++] = curChar;
                            q = 2;
                    }
                }
            }

            if(pos != 0) {
                int code;
                if(q == 1) {
                    code = lowCode;
                } else {
                    code = upCode;
                }

                String strTmp = new String(lex, 0, pos);
                nodeTmp = new TableNode(strTmp, code);
                cond = true;
                for (TableNode it : lexTable){
                    if(nodeTmp.equals(it)) {
                        cond = false;
                        break;
                    }
                }

                if(cond) {
                    lexTable.add(nodeTmp);
                }

                newLexCode = lexCode(strTmp, code, lexTable);
                rule[posRule++] = newLexCode;
            }

            if(posRule > 0) {
                node = new Node(rule, posRule);
                lang.add(node);
            }

            myLang res = new myLang(lang, lexTable);
            return res;
        } catch (IOException var25) {
            System.out.println(var25.toString());
            return null;
        }

    }
}


public class Main {



    public static void main(String[] args) {
        solver s = new solver();
        myLang l = s.readGrammar("input.txt");
    }
}
