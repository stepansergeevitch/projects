import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Macro {

    private Source source;
    private String name;
    private List<String> args;
    private boolean variadic;
    private List<Token> tokens;

    public Macro(Source source, String name) {
        this.source = source;
        this.name = name;
        this.args = null;
        this.variadic = false;
        this.tokens = new ArrayList<Token>();
    }

    public Macro(String name) {
        this(null, name);
    }

    public void setSource(Source s) {
        this.source = s;
    }

    public Source getSource() {
        return source;
    }

    public String getName() {
        return name;
    }

    public void setArgs(List<String> args) {
        this.args = args;
    }

    public boolean isFunctionLike() {
        return args != null;
    }

    public int getArgs() {
        return args.size();
    }

    public void setVariadic(boolean b) {
        this.variadic = b;
    }

    public boolean isVariadic() {
        return variadic;
    }

    public void addToken(Token tok) {
        this.tokens.add(tok);
    }

    
    public void addPaste(Token tok) {
        this.tokens.add(tokens.size() - 1, tok);
    }

    /* pp */ List<Token> getTokens() {
        return tokens;
    }

    public String getText() {
        StringBuilder buf = new StringBuilder();
        boolean paste = false;
        for (Token tok : tokens) {
            if (tok.getType() == Token.M_PASTE) {
                assert paste == false : "Two sequential pastes.";
                paste = true;
                continue;
            } else {
                buf.append(tok.getText());
            }
            if (paste) {
                buf.append(" #" + "# ");
                paste = false;
            }
            // buf.append(tokens.get(i));
        }
        return buf.toString();
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder(name);
        if (args != null) {
            buf.append('(');
            Iterator<String> it = args.iterator();
            while (it.hasNext()) {
                buf.append(it.next());
                if (it.hasNext())
                    buf.append(", ");
                else if (isVariadic())
                    buf.append("...");
            }
            buf.append(')');
        }
        if (!tokens.isEmpty()) {
            buf.append(" => ").append(getText());
        }
        return buf.toString();
    }

}
