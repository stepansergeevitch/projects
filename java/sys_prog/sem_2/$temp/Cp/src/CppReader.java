import java.io.Closeable;
import java.io.IOException;
import java.io.Reader;
import javax.annotation.Nonnull;
import static org.anarres.cpp.Token.CCOMMENT;
import static org.anarres.cpp.Token.CPPCOMMENT;
import static org.anarres.cpp.Token.EOF;


public class CppReader extends Reader implements Closeable {

    private final Preprocessor cpp;
    private String token;
    private int idx;

    public CppReader(@Nonnull final Reader r) {
        cpp = new Preprocessor(new LexerSource(r, true) {
            @Override
            public String getName() {
                return "<CppReader Input@"
                        + System.identityHashCode(r) + ">";
            }
        });
        token = "";
        idx = 0;
    }

    public CppReader(@Nonnull Preprocessor p) {
        cpp = p;
        token = "";
        idx = 0;
    }

    @Nonnull
    public Preprocessor getPreprocessor() {
        return cpp;
    }

    public void addMacro(@Nonnull String name)
            throws LexerException {
        cpp.addMacro(name);
    }

    public void addMacro(@Nonnull String name, @Nonnull String value)
            throws LexerException {
        cpp.addMacro(name, value);
    }

    private boolean refill()
            throws IOException {
        try {
            assert cpp != null : "cpp is null : was it closed?";
            if (token == null)
                return false;
            while (idx >= token.length()) {
                Token tok = cpp.token();
                switch (tok.getType()) {
                    case EOF:
                        token = null;
                        return false;
                    case CCOMMENT:
                    case CPPCOMMENT:
                        if (!cpp.getFeature(Feature.KEEPCOMMENTS)) {
                            token = " ";
                            break;
                        }
                    default:
                        token = tok.getText();
                        break;
                }
                idx = 0;
            }
            return true;
        } catch (LexerException e) {
            // new IOException(String, Throwable) is since 1.6
            IOException _e = new IOException(String.valueOf(e));
            _e.initCause(e);
            throw _e;
        }
    }

    @Override
    public int read()
            throws IOException {
        if (!refill())
            return -1;
        return token.charAt(idx++);
    }

    @Override
    public int read(char cbuf[], int off, int len)
            throws IOException {
        if (token == null)
            return -1;
        for (int i = 0; i < len; i++) {
            int ch = read();
            if (ch == -1)
                return i;
            cbuf[off + i] = (char) ch;
        }
        return len;
    }

    @Override
    public void close()
            throws IOException {
        cpp.close();
        token = null;
    }

}
