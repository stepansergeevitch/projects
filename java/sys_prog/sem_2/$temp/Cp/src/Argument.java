import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.Nonnull;


/* pp */ class Argument extends ArrayList<Token> {

    private List<Token> expansion;

    public Argument() {
        this.expansion = null;
    }

    public void addToken(@Nonnull Token tok) {
        add(tok);
    }

    /* pp */ void expand(@Nonnull Preprocessor p)
            throws IOException,
            LexerException {
        /* Cache expansion. */
        if (expansion == null) {
            this.expansion = p.expand(this);
            // System.out.println("Expanded arg " + this);
        }
    }

    @Nonnull
    public Iterator<Token> expansion() {
        return expansion.iterator();
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append("Argument(");
        // buf.append(super.toString());
        buf.append("raw=[ ");
        for (int i = 0; i < size(); i++)
            buf.append(get(i).getText());
        buf.append(" ];expansion=[ ");
        if (expansion == null)
            buf.append("null");
        else
            for (Token token : expansion)
                buf.append(token.getText());
        buf.append(" ])");
        return buf.toString();
    }

}
