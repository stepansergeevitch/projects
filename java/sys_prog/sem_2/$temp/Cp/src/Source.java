import java.io.Closeable;
import java.io.IOException;
import java.util.Iterator;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnegative;
import javax.annotation.Nonnull;
import static org.anarres.cpp.Token.CCOMMENT;
import static org.anarres.cpp.Token.CPPCOMMENT;
import static org.anarres.cpp.Token.EOF;
import static org.anarres.cpp.Token.NL;
import static org.anarres.cpp.Token.WHITESPACE;

public abstract class Source implements Iterable<Token>, Closeable {

    private Source parent;
    private boolean autopop;
    private PreprocessorListener listener;
    private boolean active;
    private boolean werror;

    public Source() {
        this.parent = null;
        this.autopop = false;
        this.listener = null;
        this.active = true;
        this.werror = false;
    }

    /* pp */ void setParent(Source parent, boolean autopop) {
        this.parent = parent;
        this.autopop = autopop;
    }


    /* pp */ final Source getParent() {
        return parent;
    }


    // @OverrideMustInvoke
	/* pp */ void init(Preprocessor pp) {
        setListener(pp.getListener());
        this.werror = pp.getWarnings().contains(Warning.ERROR);
    }

    public void setListener(PreprocessorListener pl) {
        this.listener = pl;
    }

    @CheckForNull
    public String getPath() {
        Source parent = getParent();
        if (parent != null)
            return parent.getPath();
        return null;
    }

    @CheckForNull
    public String getName() {
        Source parent = getParent();
        if (parent != null)
            return parent.getName();
        return null;
    }

    @Nonnegative
    public int getLine() {
        Source parent = getParent();
        if (parent == null)
            return 0;
        return parent.getLine();
    }

    /**
     * Returns the current column number within this Source.
     */
    public int getColumn() {
        Source parent = getParent();
        if (parent == null)
            return 0;
        return parent.getColumn();
    }

    /**
     * Returns true if this Source is expanding the given macro.
     *
     * This is used to prevent macro recursion.
     */
    /* pp */ boolean isExpanding(@Nonnull Macro m) {
        Source parent = getParent();
        if (parent != null)
            return parent.isExpanding(m);
        return false;
    }

    /**
     * Returns true if this Source should be transparently popped
     * from the input stack.
     *
     * Examples of such sources are macro expansions.
     */
    /* pp */ boolean isAutopop() {
        return autopop;
    }

    /**
     * Returns true if this source has line numbers.
     */
    /* pp */ boolean isNumbered() {
        return false;
    }

    /* This is an incredibly lazy way of disabling warnings when
     * the source is not active. */
    /* pp */ void setActive(boolean b) {
        this.active = b;
    }

    /* pp */ boolean isActive() {
        return active;
    }

    /**
     * Returns the next Token parsed from this input stream.
     *
     * @see Token
     */
    @Nonnull
    public abstract Token token()
            throws IOException,
            LexerException;

    /**
     * Returns a token iterator for this Source.
     */
    @Override
    public Iterator<Token> iterator() {
        return new SourceIterator(this);
    }

    /**
     * Skips tokens until the end of line.
     *
     * @param white true if only whitespace is permitted on the
     *	remainder of the line.
     * @return the NL token.
     */
    @Nonnull
    public Token skipline(boolean white)
            throws IOException,
            LexerException {
        for (;;) {
            Token tok = token();
            switch (tok.getType()) {
                case EOF:
                    /* There ought to be a newline before EOF.
                     * At least, in any skipline context. */
                    /* XXX Are we sure about this? */
                    warning(tok.getLine(), tok.getColumn(),
                            "No newline before end of file");
                    return new Token(NL,
                            tok.getLine(), tok.getColumn(),
                            "\n");
                // return tok;
                case NL:
                    /* This may contain one or more newlines. */
                    return tok;
                case CCOMMENT:
                case CPPCOMMENT:
                case WHITESPACE:
                    break;
                default:
                    /* XXX Check white, if required. */
                    if (white)
                        warning(tok.getLine(), tok.getColumn(),
                                "Unexpected nonwhite token");
                    break;
            }
        }
    }

    protected void error(int line, int column, String msg)
            throws LexerException {
        if (listener != null)
            listener.handleError(this, line, column, msg);
        else
            throw new LexerException("Error at " + line + ":" + column + ": " + msg);
    }

    protected void warning(int line, int column, String msg)
            throws LexerException {
        if (werror)
            error(line, column, msg);
        else if (listener != null)
            listener.handleWarning(this, line, column, msg);
        else
            throw new LexerException("Warning at " + line + ":" + column + ": " + msg);
    }

    public void close()
            throws IOException {
    }

}
