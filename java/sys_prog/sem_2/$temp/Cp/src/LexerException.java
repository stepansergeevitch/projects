public class LexerException extends Exception {

    public LexerException(String msg) {
        super(msg);
    }

    public LexerException(Throwable cause) {
        super(cause);
    }
}
