/**
 * Created by dimal97 on 5/20/17.
 */

import org.anarres.cpp.*;

import java.io.IOException;
import java.io.StringReader;
import java.io.File;
import org.apache.tools.ant.*;

public class Sample {
    public static void main(String[] args) {
        String in = "test0.c";
        String out = "output0.c";
        File f = new File(in);
        File o = new File(out);
        CppTask cpp = new CppTask();
        cpp.setInput(f);
        cpp.setOutput(o);
        System.out.println("Start");
        cpp.execute();
        System.out.println("End");
    }
}
