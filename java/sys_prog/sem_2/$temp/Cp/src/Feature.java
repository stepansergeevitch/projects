public enum Feature {

    /** Supports ANSI digraphs. */
    DIGRAPHS,
    /** Supports ANSI trigraphs. */
    TRIGRAPHS,
    /** Outputs linemarker tokens. */
    LINEMARKERS,
    /** Reports tokens of type INVALID as errors. */
    CSYNTAX,
    /** Preserves comments in the lexed output. Like cpp -C */
    KEEPCOMMENTS,
    /** Preserves comments in the lexed output, even when inactive. */
    KEEPALLCOMMENTS,
    DEBUG,
    /** Supports lexing of objective-C. */
    OBJCSYNTAX,
    INCLUDENEXT,
    /** Random extensions. */
    PRAGMA_ONCE
}
