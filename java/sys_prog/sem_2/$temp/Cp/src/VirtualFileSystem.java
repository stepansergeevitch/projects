import javax.annotation.Nonnull;

/**
 * An extremely lightweight virtual file system interface.
 */
public interface VirtualFileSystem {

    @Nonnull
    public VirtualFile getFile(@Nonnull String path);

    @Nonnull
    public VirtualFile getFile(@Nonnull String dir, @Nonnull String name);
}
