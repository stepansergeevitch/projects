import java.io.IOException;
import javax.annotation.CheckForNull;
import javax.annotation.Nonnull;

/**
 * An extremely lightweight virtual file interface.
 */
public interface VirtualFile {

    // public String getParent();
    public boolean isFile();

    @Nonnull
    public String getPath();

    @Nonnull
    public String getName();

    @CheckForNull
    public VirtualFile getParentFile();

    @Nonnull
    public VirtualFile getChildFile(String name);

    @Nonnull
    public Source getSource() throws IOException;
}
