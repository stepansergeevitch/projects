import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import javax.annotation.Nonnull;
import static org.anarres.cpp.Token.EOF;

/**
 * An Iterator for {@link Source Sources},
 * returning {@link Token Tokens}.
 */
public class SourceIterator implements Iterator<Token> {

    private final Source source;
    private Token tok;

    public SourceIterator(@Nonnull Source s) {
        this.source = s;
        this.tok = null;
    }

    private void advance() {
        try {
            if (tok == null)
                tok = source.token();
        } catch (LexerException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public boolean hasNext() {
        advance();
        return tok.getType() != EOF;
    }

    @Override
    public Token next() {
        if (!hasNext())
            throw new NoSuchElementException();
        Token t = this.tok;
        this.tok = null;
        return t;
    }

    /**
     * Not supported.
     *
     * @throws UnsupportedOperationException unconditionally.
     */
    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
