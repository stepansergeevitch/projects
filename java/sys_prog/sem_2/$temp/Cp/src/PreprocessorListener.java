import javax.annotation.Nonnull;

public interface PreprocessorListener {

    public void handleWarning(@Nonnull Source source, int line, int column,
            @Nonnull String msg)
            throws LexerException;

    public void handleError(@Nonnull Source source, int line, int column,
            @Nonnull String msg)
            throws LexerException;

    public enum SourceChangeEvent {

        SUSPEND, PUSH, POP, RESUME;
    }

    public void handleSourceChange(@Nonnull Source source, @Nonnull SourceChangeEvent event);

}
