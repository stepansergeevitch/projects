import java.io.IOException;
import java.util.List;
import static org.anarres.cpp.Token.EOF;

@Deprecated
/* pp */ class TokenSnifferSource extends Source {

    private final List<Token> target;

    /* pp */ TokenSnifferSource(List<Token> target) {
        this.target = target;
    }

    public Token token()
            throws IOException,
            LexerException {
        Token tok = getParent().token();
        if (tok.getType() != EOF)
            target.add(tok);
        return tok;
    }

    @Override
    public String toString() {
        return getParent().toString();
    }
}
