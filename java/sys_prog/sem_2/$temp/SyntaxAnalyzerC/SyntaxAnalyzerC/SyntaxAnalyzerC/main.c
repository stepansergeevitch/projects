//
//  main.c
//  SyntaxAnalyzerC
//
//  Created by Dima Laba on 4/23/17.
//  Copyright © 2017 Dima Laba. All rights reserved.
//

//#include <stdio.h>

// bison -d c99.y && flex c99.l && gcc -o result lex.yy.c c99.tab.c -ll -std=gnu89 && ./result main.c

int main()
{
    int n, reversedNumber = 0, remainder;
    while(n != 0)
    {
        remainder = n >> 10;
        reversedNumber = reversedNumber*10 + remainder;
        n /= 10;
    }
    return 0;
}
/*
 
 bison -d c99.y
 flex c99.l
 gcc -o result lex.yy.c c99.tab.c -ll -std=gnu89
 
 */
