%{

#include <stdio.h>
#include <stdlib.h>

int yylex(void);

void yyerror(char const *);

extern char yytext[];
extern int column;

enum treetype {operator_node, number_node, variable_node};

typedef struct tree {
    enum treetype nodetype;
    union {
        struct {struct tree *left, *right; char* operator;} an_operator;
        int a_number;
        double a_float;
        char* a_variable;
    } body;
} tree;

static tree *make_operator (tree *l, char* o, tree *r) {
    tree *result= (tree*) malloc (sizeof(tree));
    result->nodetype= operator_node;
    result->body.an_operator.left= l;
    result->body.an_operator.operator= o;
    result->body.an_operator.right= r;
    return result;
}
static tree *make_number (int n) {
    tree *result= (tree*) malloc (sizeof(tree));
    result->nodetype= number_node;
    result->body.a_number= n;
    return result;
}
static tree *make_variable (char* v) {
    tree *result= (tree*) malloc (sizeof(tree));
    result->nodetype= variable_node;
    result->body.a_variable= v;
    return result;
}
static void printtree (tree *t, int level) {
    #define step 4
    if (t)
    switch (t->nodetype)
    {
        case operator_node:
        printtree (t->body.an_operator.right, level+step);
        printf ("%*c%s\n", level, ' ', t->body.an_operator.operator);
        printtree (t->body.an_operator.left, level+step);
        break;
        case number_node:
        printf ("%*c%d\n", level, ' ', t->body.a_number);
        break;
        case variable_node:
        printf ("%*c%s\n", level, ' ', t->body.a_variable);
    }
}


%}

%union {
    int a_number;
    char* a_variable;
    double a_float;
    struct tree* a_tree;
}

%token IDENTIFIER CONSTANT STRING_LITERAL SIZEOF
%token PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token XOR_ASSIGN OR_ASSIGN TYPE_NAME

%token TYPEDEF EXTERN STATIC AUTO REGISTER INLINE RESTRICT
%token CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE CONST VOLATILE VOID
%token BOOL COMPLEX IMAGINARY
%token STRUCT UNION ENUM ELLIPSIS

%token CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%nonassoc LOWER_THAN_ELSE
%nonassoc ELSE

%start translation_unit
%type<a_tree> translation_unit external_declaration function_definition declaration_list declaration declaration_specifiers type_specifier declarator compound_statement block_item_list block_item init_declarator_list init_declarator direct_declarator initializer_list initializer assignment_expression cast_expression multiplicative_expression additive_expression shift_expression relational_expression equality_expression and_expression exclusive_or_expression inclusive_or_expression logical_and_expression logical_or_expression conditional_expression unary_expression expression postfix_expression primary_expression unary_operator argument_expression_list statement expression_statement iteration_statement selection_statement jump_statement
%type<a_number> CONSTANT
%type<a_variable> IDENTIFIER, STRING_LITERAL, assignment_operator
%%

primary_expression
: IDENTIFIER {$$ = make_variable($1);}
| CONSTANT {$$ = make_number($1);}
| STRING_LITERAL {$$ = make_variable($1);}
| '(' expression ')' {$$ = $2;}
;

postfix_expression
: primary_expression {$$ = $1;}
| postfix_expression '[' expression ']' {$$ = make_operator($1, "[]", $3);}
| postfix_expression '(' ')' {$$ = $1;}
| postfix_expression '(' argument_expression_list ')' {$$ = make_operator($1, "()", $3);}
| postfix_expression '.' IDENTIFIER {$$ = make_operator($1, ".", make_variable($3));}
| postfix_expression PTR_OP IDENTIFIER {$$ = make_operator($1, "->", make_variable($3));}
| postfix_expression INC_OP {$$ = make_operator($1, "++", NULL);}
| postfix_expression DEC_OP {$$ = make_operator($1, "--", NULL);}
| '(' type_name ')' '{' initializer_list '}' {$$ = $5;}
| '(' type_name ')' '{' initializer_list ',' '}' {$$ = $5;}
;

argument_expression_list
: assignment_expression {$$ = $1;}
| argument_expression_list ',' assignment_expression {$$ = make_operator($1, ",", $3);}
;

unary_expression
: postfix_expression {$$ = $1;}
| INC_OP unary_expression {$$ = make_operator(NULL, "++", $2);}
| DEC_OP unary_expression {$$ = make_operator(NULL, "--", $2);}
| unary_operator cast_expression {$$ = make_operator($1, "", $2);}
| SIZEOF unary_expression {$$ = make_operator(NULL, "SIZEOF", $2);}
| SIZEOF '(' type_name ')' {$$ = make_operator(NULL, "SIZEOF", NULL);}
;

unary_operator
: '&' {$$ = make_variable("&");}
| '*' {$$ = make_variable("*");}
| '+' {$$ = make_variable("+");}
| '-' {$$ = make_variable("-");}
| '~' {$$ = make_variable("~");}
| '!' {$$ = make_variable("!");}
;

cast_expression
: unary_expression {$$ = $1;}
| '(' type_name ')' cast_expression {$$ = make_operator(NULL, "cast", $4);}
;

multiplicative_expression
: cast_expression {$$ = $1;}
| multiplicative_expression '*' cast_expression {$$ = make_operator($1, "*", $3);}
| multiplicative_expression '/' cast_expression {$$ = make_operator($1, "/", $3);}
| multiplicative_expression '%' cast_expression {$$ = make_operator($1, "%", $3);}
;

additive_expression
: multiplicative_expression {$$ = $1;}
| additive_expression '+' multiplicative_expression {$$ = make_operator($1, "+", $3);}
| additive_expression '-' multiplicative_expression {$$ = make_operator($1, "-", $3);}
;

shift_expression
: additive_expression {$$ = $1;}
| shift_expression LEFT_OP additive_expression {$$ = make_operator($1, "<<", $3);}
| shift_expression RIGHT_OP additive_expression {$$ = make_operator($1, "<<", $3);}
;

relational_expression
: shift_expression {$$ = $1;}
| relational_expression '<' shift_expression {$$ = make_operator($1, "<", $3);}
| relational_expression '>' shift_expression {$$ = make_operator($1, ">", $3);}
| relational_expression LE_OP shift_expression {$$ = make_operator($1, "<=", $3);}
| relational_expression GE_OP shift_expression {$$ = make_operator($1, ">=", $3);}
;

equality_expression
: relational_expression {$$ = $1;}
| equality_expression EQ_OP relational_expression {$$ = make_operator($1, "==", $3);}
| equality_expression NE_OP relational_expression {$$ = make_operator($1, "!=", $3);}
;

and_expression
: equality_expression {$$ = $1;}
| and_expression '&' equality_expression {$$ = make_operator($1, "&", $3);}
;

exclusive_or_expression
: and_expression {$$ = $1;}
| exclusive_or_expression '^' and_expression {$$ = make_operator($1, "^", $3);}
;

inclusive_or_expression
: exclusive_or_expression {$$ = $1;}
| inclusive_or_expression '|' exclusive_or_expression {$$ = make_operator($1, "|", $3);}
;

logical_and_expression
: inclusive_or_expression {$$ = $1;}
| logical_and_expression AND_OP inclusive_or_expression {$$ = make_operator($1, "&&", $3);}
;

logical_or_expression
: logical_and_expression {$$ = $1;}
| logical_or_expression OR_OP logical_and_expression {$$ = make_operator($1, "||", $3);}
;

conditional_expression
: logical_or_expression {$$ = $1;}
| logical_or_expression '?' expression ':' conditional_expression {$$ = $1;}
;

assignment_expression
: conditional_expression {$$ = $1;}
| unary_expression assignment_operator assignment_expression {$$ = make_operator($1, $2, $3);}
;

assignment_operator
: '=' {$$ = "=";}
| MUL_ASSIGN {$$ = "*=";}
| DIV_ASSIGN {$$ = "/=";}
| MOD_ASSIGN {$$ = "%=";}
| ADD_ASSIGN {$$ = "+=";}
| SUB_ASSIGN {$$ = "-=";}
| LEFT_ASSIGN {$$ = "=";}
| RIGHT_ASSIGN {$$ = "=";}
| AND_ASSIGN {$$ = "&=";}
| XOR_ASSIGN {$$ = "^=";}
| OR_ASSIGN {$$ = "|=";}
;

expression
: assignment_expression {$$ = $1;}
| expression ',' assignment_expression {$$ = make_operator($1, ",", $3);}
;

constant_expression
: conditional_expression
;

declaration
: declaration_specifiers ';' {$$ = $1;}
| declaration_specifiers init_declarator_list ';' {$$ = make_operator($1, "declaration_specifiers", $2);}
;

declaration_specifiers
: storage_class_specifier {$$ = make_variable("storage_class_specifier");}
| storage_class_specifier declaration_specifiers {$$ = make_variable("storage_class_specifier declaration_specifiers");}
| type_specifier {$$ = $1;}
| type_specifier declaration_specifiers {$$ = make_variable("type_specifier declaration_specifiers");}
| type_qualifier {$$ = make_variable("type_qualifier");}
| type_qualifier declaration_specifiers {$$ = make_variable("type_qualifier declaration_specifiers");}
| function_specifier {$$ = make_variable("function_specifier");}
| function_specifier declaration_specifiers {$$ = make_variable("function_specifier declaration_specifiers");}
;

init_declarator_list
: init_declarator {$$ = $1;}
| init_declarator_list ',' init_declarator {$$ = make_operator($1, ",", $3);}
;

init_declarator
: declarator {$$ = $1;}
| declarator '=' initializer {$$ = make_operator($1, "=", $3);}
;

storage_class_specifier
: TYPEDEF
| EXTERN
| STATIC
| AUTO
| REGISTER
;

type_specifier
: VOID {$$ = make_variable("void");}
| CHAR {$$ = make_variable("char");}
| SHORT {$$ = make_variable("short");}
| INT {$$ = make_variable("int");}
| LONG {$$ = make_variable("long");}
| FLOAT {$$ = make_variable("float");}
| DOUBLE {$$ = make_variable("double");}
| SIGNED {$$ = make_variable("signed");}
| UNSIGNED {$$ = make_variable("unsigned");}
| BOOL {$$ = make_variable("bool");}
| COMPLEX {$$ = make_variable("copmlex");}
| IMAGINARY {$$ = make_variable("imaginary");}
| struct_or_union_specifier {$$ = make_variable("struct or union");}
| enum_specifier {$$ = make_variable("enum spec");}
| TYPE_NAME {$$ = make_variable("type name");}
;

struct_or_union_specifier
: struct_or_union IDENTIFIER '{' struct_declaration_list '}'
| struct_or_union '{' struct_declaration_list '}'
| struct_or_union IDENTIFIER
;

struct_or_union
: STRUCT
| UNION
;

struct_declaration_list
: struct_declaration
| struct_declaration_list struct_declaration
;

struct_declaration
: specifier_qualifier_list struct_declarator_list ';'
;

specifier_qualifier_list
: type_specifier specifier_qualifier_list
| type_specifier
| type_qualifier specifier_qualifier_list
| type_qualifier
;

struct_declarator_list
: struct_declarator
| struct_declarator_list ',' struct_declarator
;

struct_declarator
: declarator
| ':' constant_expression
| declarator ':' constant_expression
;

enum_specifier
: ENUM '{' enumerator_list '}'
| ENUM IDENTIFIER '{' enumerator_list '}'
| ENUM '{' enumerator_list ',' '}'
| ENUM IDENTIFIER '{' enumerator_list ',' '}'
| ENUM IDENTIFIER
;

enumerator_list
: enumerator
| enumerator_list ',' enumerator
;

enumerator
: IDENTIFIER
| IDENTIFIER '=' constant_expression
;

type_qualifier
: CONST
| RESTRICT
| VOLATILE
;

function_specifier
: INLINE
;

declarator
: pointer direct_declarator {$$ = make_variable("pointer");}
| direct_declarator {$$ = $1;}
;


direct_declarator
: IDENTIFIER {$$ = make_variable($1);}
| '(' declarator ')' {$$ = $2;}
| direct_declarator '[' type_qualifier_list assignment_expression ']' {$$ = $1;}
| direct_declarator '[' type_qualifier_list ']' {$$ = $1;}
| direct_declarator '[' assignment_expression ']' {$$ = $1;}
| direct_declarator '[' STATIC type_qualifier_list assignment_expression ']' {$$ = $1;}
| direct_declarator '[' type_qualifier_list STATIC assignment_expression ']' {$$ = $1;}
| direct_declarator '[' type_qualifier_list '*' ']' {$$ = $1;}
| direct_declarator '[' '*' ']' {$$ = $1;}
| direct_declarator '[' ']' {$$ = $1;}
| direct_declarator '(' parameter_type_list ')' {$$ = $1;}
| direct_declarator '(' identifier_list ')' {$$ = $1;}
| direct_declarator '(' ')' {$$ = $1;}
;

pointer
: '*'
| '*' type_qualifier_list
| '*' pointer
| '*' type_qualifier_list pointer
;

type_qualifier_list
: type_qualifier
| type_qualifier_list type_qualifier
;


parameter_type_list
: parameter_list
| parameter_list ',' ELLIPSIS
;

parameter_list
: parameter_declaration
| parameter_list ',' parameter_declaration
;

parameter_declaration
: declaration_specifiers declarator
| declaration_specifiers abstract_declarator
| declaration_specifiers
;

identifier_list
: IDENTIFIER
| identifier_list ',' IDENTIFIER
;

type_name
: specifier_qualifier_list
| specifier_qualifier_list abstract_declarator
;

abstract_declarator
: pointer
| direct_abstract_declarator
| pointer direct_abstract_declarator
;

direct_abstract_declarator
: '(' abstract_declarator ')'
| '[' ']'
| '[' assignment_expression ']'
| direct_abstract_declarator '[' ']'
| direct_abstract_declarator '[' assignment_expression ']'
| '[' '*' ']'
| direct_abstract_declarator '[' '*' ']'
| '(' ')'
| '(' parameter_type_list ')'
| direct_abstract_declarator '(' ')'
| direct_abstract_declarator '(' parameter_type_list ')'
;

initializer
: assignment_expression {$$ = $1;}
| '{' initializer_list '}' {$$ = $2;}
| '{' initializer_list ',' '}' {$$ = $2;}
;

initializer_list
: initializer {$$ = $1;}
| designation initializer {$$ = make_variable("a");}
| initializer_list ',' initializer {$$ = make_operator($1, ",", $3);}
| initializer_list ',' designation initializer {$$ = make_variable("c");}
;

designation
: designator_list '='
;

designator_list
: designator
| designator_list designator
;

designator
: '[' constant_expression ']'
| '.' IDENTIFIER
;

statement
: labeled_statement {$$ = make_variable("LABELED");}
| compound_statement {$$ = $1;}
| expression_statement {$$ = $1;}
| selection_statement {$$ = $1;}
| iteration_statement {$$ = $1;}
| jump_statement {$$ = $1;}
;

labeled_statement
: IDENTIFIER ':' statement
| CASE constant_expression ':' statement
| DEFAULT ':' statement
;

compound_statement
: '{' '}' {$$ = make_variable("{}");}
| '{' block_item_list '}' {$$ = $2;}
;

block_item_list
: block_item {$$ = $1;}
| block_item_list block_item {$$ = make_operator($1, ";",$2);}
;

block_item
: declaration {$$ = $1;}
| statement {$$ = $1;}
;

expression_statement
: ';' {$$ = make_variable(";");}
| expression ';' {$$ = $1;}
;

selection_statement
: IF '(' expression ')' statement %prec LOWER_THAN_ELSE {$$ = make_operator($3 ,"if", $5);}
| IF '(' expression ')' statement ELSE statement {$$ = make_operator($3, "if", make_operator($5, "else", $7));}
| SWITCH '(' expression ')' statement {$$ = make_operator($3, "switch", $5);}
;

iteration_statement
: WHILE '(' expression ')' statement {$$ = make_operator($3, "while", $5);}
| DO statement WHILE '(' expression ')' ';' {$$ = make_operator($2, "do while", $5);}
| FOR '(' expression_statement expression_statement ')' statement {$$ = make_variable("for");}
| FOR '(' expression_statement expression_statement expression ')' statement {$$ = make_variable("for");}
| FOR '(' declaration expression_statement ')' statement {$$ = make_variable("for");}
| FOR '(' declaration expression_statement expression ')' statement {$$ = make_variable("for");}
;

jump_statement
: GOTO IDENTIFIER ';' {$$ = make_operator(NULL, "goto", make_variable($2));}
| CONTINUE ';' {$$ = make_variable("continue");}
| BREAK ';' {$$ = make_variable("break");}
| RETURN ';' {$$ = make_variable("return");}
| RETURN expression ';' {$$ = make_operator(NULL, "return", $2);}
;

translation_unit
: external_declaration {printtree($1, 1);}
| translation_unit external_declaration {printtree($2, 1);}
;

external_declaration
: function_definition {$$ = make_operator(NULL, "function_definition", $1);}
| declaration {$$ = make_operator(NULL, "decl", make_number(1));}
;

function_definition
: declaration_specifiers declarator declaration_list compound_statement {$$ = make_operator(make_number(1), "+", make_number(1));}
| declaration_specifiers declarator compound_statement {$$ = make_operator($1, "def", make_operator($2, "", $3));}
;

declaration_list
: declaration {$$ = make_operator(make_number(1), "*", make_number(1));}
| declaration_list declaration {$$ = make_operator(make_number(1), "/", make_number(1));}
;
%%

