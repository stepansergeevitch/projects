/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C

   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.3"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     IDENTIFIER = 258,
     CONSTANT = 259,
     STRING_LITERAL = 260,
     SIZEOF = 261,
     PTR_OP = 262,
     INC_OP = 263,
     DEC_OP = 264,
     LEFT_OP = 265,
     RIGHT_OP = 266,
     LE_OP = 267,
     GE_OP = 268,
     EQ_OP = 269,
     NE_OP = 270,
     AND_OP = 271,
     OR_OP = 272,
     MUL_ASSIGN = 273,
     DIV_ASSIGN = 274,
     MOD_ASSIGN = 275,
     ADD_ASSIGN = 276,
     SUB_ASSIGN = 277,
     LEFT_ASSIGN = 278,
     RIGHT_ASSIGN = 279,
     AND_ASSIGN = 280,
     XOR_ASSIGN = 281,
     OR_ASSIGN = 282,
     TYPE_NAME = 283,
     TYPEDEF = 284,
     EXTERN = 285,
     STATIC = 286,
     AUTO = 287,
     REGISTER = 288,
     INLINE = 289,
     RESTRICT = 290,
     CHAR = 291,
     SHORT = 292,
     INT = 293,
     LONG = 294,
     SIGNED = 295,
     UNSIGNED = 296,
     FLOAT = 297,
     DOUBLE = 298,
     CONST = 299,
     VOLATILE = 300,
     VOID = 301,
     BOOL = 302,
     COMPLEX = 303,
     IMAGINARY = 304,
     STRUCT = 305,
     UNION = 306,
     ENUM = 307,
     ELLIPSIS = 308,
     CASE = 309,
     DEFAULT = 310,
     IF = 311,
     ELSE = 312,
     SWITCH = 313,
     WHILE = 314,
     DO = 315,
     FOR = 316,
     GOTO = 317,
     CONTINUE = 318,
     BREAK = 319,
     RETURN = 320,
     LOWER_THAN_ELSE = 321
   };
#endif
/* Tokens.  */
#define IDENTIFIER 258
#define CONSTANT 259
#define STRING_LITERAL 260
#define SIZEOF 261
#define PTR_OP 262
#define INC_OP 263
#define DEC_OP 264
#define LEFT_OP 265
#define RIGHT_OP 266
#define LE_OP 267
#define GE_OP 268
#define EQ_OP 269
#define NE_OP 270
#define AND_OP 271
#define OR_OP 272
#define MUL_ASSIGN 273
#define DIV_ASSIGN 274
#define MOD_ASSIGN 275
#define ADD_ASSIGN 276
#define SUB_ASSIGN 277
#define LEFT_ASSIGN 278
#define RIGHT_ASSIGN 279
#define AND_ASSIGN 280
#define XOR_ASSIGN 281
#define OR_ASSIGN 282
#define TYPE_NAME 283
#define TYPEDEF 284
#define EXTERN 285
#define STATIC 286
#define AUTO 287
#define REGISTER 288
#define INLINE 289
#define RESTRICT 290
#define CHAR 291
#define SHORT 292
#define INT 293
#define LONG 294
#define SIGNED 295
#define UNSIGNED 296
#define FLOAT 297
#define DOUBLE 298
#define CONST 299
#define VOLATILE 300
#define VOID 301
#define BOOL 302
#define COMPLEX 303
#define IMAGINARY 304
#define STRUCT 305
#define UNION 306
#define ENUM 307
#define ELLIPSIS 308
#define CASE 309
#define DEFAULT 310
#define IF 311
#define ELSE 312
#define SWITCH 313
#define WHILE 314
#define DO 315
#define FOR 316
#define GOTO 317
#define CONTINUE 318
#define BREAK 319
#define RETURN 320
#define LOWER_THAN_ELSE 321




/* Copy the first part of user declarations.  */
#line 1 "c99.y"


#include <stdio.h>
#include <stdlib.h>

int yylex(void);

void yyerror(char const *);

extern char yytext[];
extern int column;

enum treetype {operator_node, number_node, variable_node};

typedef struct tree {
    enum treetype nodetype;
    union {
        struct {struct tree *left, *right; char* operator;} an_operator;
        int a_number;
        double a_float;
        char* a_variable;
    } body;
} tree;

static tree *make_operator (tree *l, char* o, tree *r) {
    tree *result= (tree*) malloc (sizeof(tree));
    result->nodetype= operator_node;
    result->body.an_operator.left= l;
    result->body.an_operator.operator= o;
    result->body.an_operator.right= r;
    return result;
}
static tree *make_number (int n) {
    tree *result= (tree*) malloc (sizeof(tree));
    result->nodetype= number_node;
    result->body.a_number= n;
    return result;
}
static tree *make_variable (char* v) {
    tree *result= (tree*) malloc (sizeof(tree));
    result->nodetype= variable_node;
    result->body.a_variable= v;
    return result;
}
static void printtree (tree *t, int level) {
    #define step 4
    if (t)
    switch (t->nodetype)
    {
        case operator_node:
        printtree (t->body.an_operator.right, level+step);
        printf ("%*c%s\n", level, ' ', t->body.an_operator.operator);
        printtree (t->body.an_operator.left, level+step);
        break;
        case number_node:
        printf ("%*c%d\n", level, ' ', t->body.a_number);
        break;
        case variable_node:
        printf ("%*c%s\n", level, ' ', t->body.a_variable);
    }
}




/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
#line 66 "c99.y"
{
    int a_number;
    char* a_variable;
    double a_float;
    struct tree* a_tree;
}
/* Line 193 of yacc.c.  */
#line 300 "c99.tab.c"
	YYSTYPE;
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 216 of yacc.c.  */
#line 313 "c99.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  55
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1567

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  91
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  69
/* YYNRULES -- Number of rules.  */
#define YYNRULES  238
/* YYNRULES -- Number of states.  */
#define YYNSTATES  400

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   321

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    80,     2,     2,     2,    82,    75,     2,
      67,    68,    76,    77,    74,    78,    71,    81,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    88,    90,
      83,    89,    84,    87,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    69,     2,    70,    85,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    72,    86,    73,    79,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     7,     9,    13,    15,    20,    24,
      29,    33,    37,    40,    43,    50,    58,    60,    64,    66,
      69,    72,    75,    78,    83,    85,    87,    89,    91,    93,
      95,    97,   102,   104,   108,   112,   116,   118,   122,   126,
     128,   132,   136,   138,   142,   146,   150,   154,   156,   160,
     164,   166,   170,   172,   176,   178,   182,   184,   188,   190,
     194,   196,   202,   204,   208,   210,   212,   214,   216,   218,
     220,   222,   224,   226,   228,   230,   232,   236,   238,   241,
     245,   247,   250,   252,   255,   257,   260,   262,   265,   267,
     271,   273,   277,   279,   281,   283,   285,   287,   289,   291,
     293,   295,   297,   299,   301,   303,   305,   307,   309,   311,
     313,   315,   317,   323,   328,   331,   333,   335,   337,   340,
     344,   347,   349,   352,   354,   356,   360,   362,   365,   369,
     374,   380,   386,   393,   396,   398,   402,   404,   408,   410,
     412,   414,   416,   419,   421,   423,   427,   433,   438,   443,
     450,   457,   463,   468,   472,   477,   482,   486,   488,   491,
     494,   498,   500,   503,   505,   509,   511,   515,   518,   521,
     523,   525,   529,   531,   534,   536,   538,   541,   545,   548,
     552,   556,   561,   565,   570,   573,   577,   581,   586,   588,
     592,   597,   599,   602,   606,   611,   614,   616,   619,   623,
     626,   628,   630,   632,   634,   636,   638,   642,   647,   651,
     654,   658,   660,   663,   665,   667,   669,   672,   678,   686,
     692,   698,   706,   713,   721,   728,   736,   740,   743,   746,
     749,   753,   755,   758,   760,   762,   767,   771,   773
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int16 yyrhs[] =
{
     156,     0,    -1,     3,    -1,     4,    -1,     5,    -1,    67,
     111,    68,    -1,    92,    -1,    93,    69,   111,    70,    -1,
      93,    67,    68,    -1,    93,    67,    94,    68,    -1,    93,
      71,     3,    -1,    93,     7,     3,    -1,    93,     8,    -1,
      93,     9,    -1,    67,   139,    68,    72,   143,    73,    -1,
      67,   139,    68,    72,   143,    74,    73,    -1,   109,    -1,
      94,    74,   109,    -1,    93,    -1,     8,    95,    -1,     9,
      95,    -1,    96,    97,    -1,     6,    95,    -1,     6,    67,
     139,    68,    -1,    75,    -1,    76,    -1,    77,    -1,    78,
      -1,    79,    -1,    80,    -1,    95,    -1,    67,   139,    68,
      97,    -1,    97,    -1,    98,    76,    97,    -1,    98,    81,
      97,    -1,    98,    82,    97,    -1,    98,    -1,    99,    77,
      98,    -1,    99,    78,    98,    -1,    99,    -1,   100,    10,
      99,    -1,   100,    11,    99,    -1,   100,    -1,   101,    83,
     100,    -1,   101,    84,   100,    -1,   101,    12,   100,    -1,
     101,    13,   100,    -1,   101,    -1,   102,    14,   101,    -1,
     102,    15,   101,    -1,   102,    -1,   103,    75,   102,    -1,
     103,    -1,   104,    85,   103,    -1,   104,    -1,   105,    86,
     104,    -1,   105,    -1,   106,    16,   105,    -1,   106,    -1,
     107,    17,   106,    -1,   107,    -1,   107,    87,   111,    88,
     108,    -1,   108,    -1,    95,   110,   109,    -1,    89,    -1,
      18,    -1,    19,    -1,    20,    -1,    21,    -1,    22,    -1,
      23,    -1,    24,    -1,    25,    -1,    26,    -1,    27,    -1,
     109,    -1,   111,    74,   109,    -1,   108,    -1,   114,    90,
      -1,   114,   115,    90,    -1,   117,    -1,   117,   114,    -1,
     118,    -1,   118,   114,    -1,   129,    -1,   129,   114,    -1,
     130,    -1,   130,   114,    -1,   116,    -1,   115,    74,   116,
      -1,   131,    -1,   131,    89,   142,    -1,    29,    -1,    30,
      -1,    31,    -1,    32,    -1,    33,    -1,    46,    -1,    36,
      -1,    37,    -1,    38,    -1,    39,    -1,    42,    -1,    43,
      -1,    40,    -1,    41,    -1,    47,    -1,    48,    -1,    49,
      -1,   119,    -1,   126,    -1,    28,    -1,   120,     3,    72,
     121,    73,    -1,   120,    72,   121,    73,    -1,   120,     3,
      -1,    50,    -1,    51,    -1,   122,    -1,   121,   122,    -1,
     123,   124,    90,    -1,   118,   123,    -1,   118,    -1,   129,
     123,    -1,   129,    -1,   125,    -1,   124,    74,   125,    -1,
     131,    -1,    88,   112,    -1,   131,    88,   112,    -1,    52,
      72,   127,    73,    -1,    52,     3,    72,   127,    73,    -1,
      52,    72,   127,    74,    73,    -1,    52,     3,    72,   127,
      74,    73,    -1,    52,     3,    -1,   128,    -1,   127,    74,
     128,    -1,     3,    -1,     3,    89,   112,    -1,    44,    -1,
      35,    -1,    45,    -1,    34,    -1,   133,   132,    -1,   132,
      -1,     3,    -1,    67,   131,    68,    -1,   132,    69,   134,
     109,    70,    -1,   132,    69,   134,    70,    -1,   132,    69,
     109,    70,    -1,   132,    69,    31,   134,   109,    70,    -1,
     132,    69,   134,    31,   109,    70,    -1,   132,    69,   134,
      76,    70,    -1,   132,    69,    76,    70,    -1,   132,    69,
      70,    -1,   132,    67,   135,    68,    -1,   132,    67,   138,
      68,    -1,   132,    67,    68,    -1,    76,    -1,    76,   134,
      -1,    76,   133,    -1,    76,   134,   133,    -1,   129,    -1,
     134,   129,    -1,   136,    -1,   136,    74,    53,    -1,   137,
      -1,   136,    74,   137,    -1,   114,   131,    -1,   114,   140,
      -1,   114,    -1,     3,    -1,   138,    74,     3,    -1,   123,
      -1,   123,   140,    -1,   133,    -1,   141,    -1,   133,   141,
      -1,    67,   140,    68,    -1,    69,    70,    -1,    69,   109,
      70,    -1,   141,    69,    70,    -1,   141,    69,   109,    70,
      -1,    69,    76,    70,    -1,   141,    69,    76,    70,    -1,
      67,    68,    -1,    67,   135,    68,    -1,   141,    67,    68,
      -1,   141,    67,   135,    68,    -1,   109,    -1,    72,   143,
      73,    -1,    72,   143,    74,    73,    -1,   142,    -1,   144,
     142,    -1,   143,    74,   142,    -1,   143,    74,   144,   142,
      -1,   145,    89,    -1,   146,    -1,   145,   146,    -1,    69,
     112,    70,    -1,    71,     3,    -1,   148,    -1,   149,    -1,
     152,    -1,   153,    -1,   154,    -1,   155,    -1,     3,    88,
     147,    -1,    54,   112,    88,   147,    -1,    55,    88,   147,
      -1,    72,    73,    -1,    72,   150,    73,    -1,   151,    -1,
     150,   151,    -1,   113,    -1,   147,    -1,    90,    -1,   111,
      90,    -1,    56,    67,   111,    68,   147,    -1,    56,    67,
     111,    68,   147,    57,   147,    -1,    58,    67,   111,    68,
     147,    -1,    59,    67,   111,    68,   147,    -1,    60,   147,
      59,    67,   111,    68,    90,    -1,    61,    67,   152,   152,
      68,   147,    -1,    61,    67,   152,   152,   111,    68,   147,
      -1,    61,    67,   113,   152,    68,   147,    -1,    61,    67,
     113,   152,   111,    68,   147,    -1,    62,     3,    90,    -1,
      63,    90,    -1,    64,    90,    -1,    65,    90,    -1,    65,
     111,    90,    -1,   157,    -1,   156,   157,    -1,   158,    -1,
     113,    -1,   114,   131,   159,   149,    -1,   114,   131,   149,
      -1,   113,    -1,   159,   113,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,    96,    96,    97,    98,    99,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   116,   117,   121,   122,
     123,   124,   125,   126,   130,   131,   132,   133,   134,   135,
     139,   140,   144,   145,   146,   147,   151,   152,   153,   157,
     158,   159,   163,   164,   165,   166,   167,   171,   172,   173,
     177,   178,   182,   183,   187,   188,   192,   193,   197,   198,
     202,   203,   207,   208,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   221,   222,   226,   227,   231,   235,   236,
     240,   241,   242,   243,   244,   245,   246,   247,   251,   252,
     256,   257,   261,   262,   263,   264,   265,   269,   270,   271,
     272,   273,   274,   275,   276,   277,   278,   279,   280,   281,
     282,   283,   287,   288,   289,   293,   294,   298,   299,   303,
     307,   308,   309,   310,   314,   315,   319,   320,   321,   325,
     326,   327,   328,   329,   333,   334,   338,   339,   343,   344,
     345,   349,   353,   354,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   375,   376,   377,
     378,   382,   383,   388,   389,   393,   394,   398,   399,   400,
     404,   405,   409,   410,   414,   415,   416,   420,   421,   422,
     423,   424,   425,   426,   427,   428,   429,   430,   434,   435,
     436,   440,   441,   442,   443,   447,   451,   452,   456,   457,
     461,   462,   463,   464,   465,   466,   470,   471,   472,   476,
     477,   481,   482,   486,   487,   491,   492,   496,   497,   498,
     502,   503,   504,   505,   506,   507,   511,   512,   513,   514,
     515,   519,   520,   524,   525,   529,   530,   534,   535
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "IDENTIFIER", "CONSTANT",
  "STRING_LITERAL", "SIZEOF", "PTR_OP", "INC_OP", "DEC_OP", "LEFT_OP",
  "RIGHT_OP", "LE_OP", "GE_OP", "EQ_OP", "NE_OP", "AND_OP", "OR_OP",
  "MUL_ASSIGN", "DIV_ASSIGN", "MOD_ASSIGN", "ADD_ASSIGN", "SUB_ASSIGN",
  "LEFT_ASSIGN", "RIGHT_ASSIGN", "AND_ASSIGN", "XOR_ASSIGN", "OR_ASSIGN",
  "TYPE_NAME", "TYPEDEF", "EXTERN", "STATIC", "AUTO", "REGISTER", "INLINE",
  "RESTRICT", "CHAR", "SHORT", "INT", "LONG", "SIGNED", "UNSIGNED",
  "FLOAT", "DOUBLE", "CONST", "VOLATILE", "VOID", "BOOL", "COMPLEX",
  "IMAGINARY", "STRUCT", "UNION", "ENUM", "ELLIPSIS", "CASE", "DEFAULT",
  "IF", "ELSE", "SWITCH", "WHILE", "DO", "FOR", "GOTO", "CONTINUE",
  "BREAK", "RETURN", "LOWER_THAN_ELSE", "'('", "')'", "'['", "']'", "'.'",
  "'{'", "'}'", "','", "'&'", "'*'", "'+'", "'-'", "'~'", "'!'", "'/'",
  "'%'", "'<'", "'>'", "'^'", "'|'", "'?'", "':'", "'='", "';'", "$accept",
  "primary_expression", "postfix_expression", "argument_expression_list",
  "unary_expression", "unary_operator", "cast_expression",
  "multiplicative_expression", "additive_expression", "shift_expression",
  "relational_expression", "equality_expression", "and_expression",
  "exclusive_or_expression", "inclusive_or_expression",
  "logical_and_expression", "logical_or_expression",
  "conditional_expression", "assignment_expression", "assignment_operator",
  "expression", "constant_expression", "declaration",
  "declaration_specifiers", "init_declarator_list", "init_declarator",
  "storage_class_specifier", "type_specifier", "struct_or_union_specifier",
  "struct_or_union", "struct_declaration_list", "struct_declaration",
  "specifier_qualifier_list", "struct_declarator_list",
  "struct_declarator", "enum_specifier", "enumerator_list", "enumerator",
  "type_qualifier", "function_specifier", "declarator",
  "direct_declarator", "pointer", "type_qualifier_list",
  "parameter_type_list", "parameter_list", "parameter_declaration",
  "identifier_list", "type_name", "abstract_declarator",
  "direct_abstract_declarator", "initializer", "initializer_list",
  "designation", "designator_list", "designator", "statement",
  "labeled_statement", "compound_statement", "block_item_list",
  "block_item", "expression_statement", "selection_statement",
  "iteration_statement", "jump_statement", "translation_unit",
  "external_declaration", "function_definition", "declaration_list", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,    40,    41,    91,
      93,    46,   123,   125,    44,    38,    42,    43,    45,   126,
      33,    47,    37,    60,    62,    94,   124,    63,    58,    61,
      59
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    91,    92,    92,    92,    92,    93,    93,    93,    93,
      93,    93,    93,    93,    93,    93,    94,    94,    95,    95,
      95,    95,    95,    95,    96,    96,    96,    96,    96,    96,
      97,    97,    98,    98,    98,    98,    99,    99,    99,   100,
     100,   100,   101,   101,   101,   101,   101,   102,   102,   102,
     103,   103,   104,   104,   105,   105,   106,   106,   107,   107,
     108,   108,   109,   109,   110,   110,   110,   110,   110,   110,
     110,   110,   110,   110,   110,   111,   111,   112,   113,   113,
     114,   114,   114,   114,   114,   114,   114,   114,   115,   115,
     116,   116,   117,   117,   117,   117,   117,   118,   118,   118,
     118,   118,   118,   118,   118,   118,   118,   118,   118,   118,
     118,   118,   119,   119,   119,   120,   120,   121,   121,   122,
     123,   123,   123,   123,   124,   124,   125,   125,   125,   126,
     126,   126,   126,   126,   127,   127,   128,   128,   129,   129,
     129,   130,   131,   131,   132,   132,   132,   132,   132,   132,
     132,   132,   132,   132,   132,   132,   132,   133,   133,   133,
     133,   134,   134,   135,   135,   136,   136,   137,   137,   137,
     138,   138,   139,   139,   140,   140,   140,   141,   141,   141,
     141,   141,   141,   141,   141,   141,   141,   141,   142,   142,
     142,   143,   143,   143,   143,   144,   145,   145,   146,   146,
     147,   147,   147,   147,   147,   147,   148,   148,   148,   149,
     149,   150,   150,   151,   151,   152,   152,   153,   153,   153,
     154,   154,   154,   154,   154,   154,   155,   155,   155,   155,
     155,   156,   156,   157,   157,   158,   158,   159,   159
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     1,     3,     1,     4,     3,     4,
       3,     3,     2,     2,     6,     7,     1,     3,     1,     2,
       2,     2,     2,     4,     1,     1,     1,     1,     1,     1,
       1,     4,     1,     3,     3,     3,     1,     3,     3,     1,
       3,     3,     1,     3,     3,     3,     3,     1,     3,     3,
       1,     3,     1,     3,     1,     3,     1,     3,     1,     3,
       1,     5,     1,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     3,     1,     2,     3,
       1,     2,     1,     2,     1,     2,     1,     2,     1,     3,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     5,     4,     2,     1,     1,     1,     2,     3,
       2,     1,     2,     1,     1,     3,     1,     2,     3,     4,
       5,     5,     6,     2,     1,     3,     1,     3,     1,     1,
       1,     1,     2,     1,     1,     3,     5,     4,     4,     6,
       6,     5,     4,     3,     4,     4,     3,     1,     2,     2,
       3,     1,     2,     1,     3,     1,     3,     2,     2,     1,
       1,     3,     1,     2,     1,     1,     2,     3,     2,     3,
       3,     4,     3,     4,     2,     3,     3,     4,     1,     3,
       4,     1,     2,     3,     4,     2,     1,     2,     3,     2,
       1,     1,     1,     1,     1,     1,     3,     4,     3,     2,
       3,     1,     2,     1,     1,     1,     2,     5,     7,     5,
       5,     7,     6,     7,     6,     7,     3,     2,     2,     2,
       3,     1,     2,     1,     1,     4,     3,     1,     2
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,   111,    92,    93,    94,    95,    96,   141,   139,    98,
      99,   100,   101,   104,   105,   102,   103,   138,   140,    97,
     106,   107,   108,   115,   116,     0,   234,     0,    80,    82,
     109,     0,   110,    84,    86,     0,   231,   233,   133,     0,
     144,     0,   157,    78,     0,    88,    90,   143,     0,    81,
      83,   114,     0,    85,    87,     1,   232,     0,   136,     0,
     134,     0,   161,   159,   158,     0,    79,     0,     0,   237,
       0,   236,     0,     0,     0,   142,     0,   121,     0,   117,
       0,   123,     0,     0,   129,     0,   145,   162,   160,    89,
      90,     2,     3,     4,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   209,
      24,    25,    26,    27,    28,    29,   215,     6,    18,    30,
       0,    32,    36,    39,    42,    47,    50,    52,    54,    56,
      58,    60,    62,    75,     0,   213,   214,   200,   201,     0,
     211,   202,   203,   204,   205,     2,     0,   188,    91,   238,
     235,   170,   156,   169,     0,   163,   165,     0,     0,   153,
      25,     0,     0,     0,   120,   113,   118,     0,     0,   124,
     126,   122,   130,     0,    30,    77,   137,   131,   135,     0,
       0,    22,     0,    19,    20,     0,     0,     0,     0,     0,
       0,     0,     0,   227,   228,   229,     0,     0,   172,     0,
       0,    12,    13,     0,     0,     0,    65,    66,    67,    68,
      69,    70,    71,    72,    73,    74,    64,     0,    21,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   216,
     210,   212,     0,     0,   191,     0,     0,     0,   196,     0,
       0,   167,   174,   168,   175,   154,     0,   155,     0,     0,
     152,   148,     0,   147,    25,     0,   112,   127,     0,   119,
       0,   132,   206,     0,     0,     0,   208,     0,     0,     0,
       0,     0,     0,   226,   230,     5,     0,   174,   173,     0,
      11,     8,     0,    16,     0,    10,    63,    33,    34,    35,
      37,    38,    40,    41,    45,    46,    43,    44,    48,    49,
      51,    53,    55,    57,    59,     0,    76,     0,   199,   189,
       0,   192,   195,   197,   184,     0,     0,   178,    25,     0,
     176,     0,     0,   164,   166,   171,     0,     0,   151,   146,
     125,   128,    23,     0,   207,     0,     0,     0,     0,     0,
       0,     0,    31,     9,     0,     7,     0,   198,   190,   193,
       0,   185,   177,   182,   179,   186,     0,   180,    25,     0,
     149,   150,   217,   219,   220,     0,     0,     0,     0,     0,
       0,    17,    61,   194,   187,   183,   181,     0,     0,   224,
       0,   222,     0,    14,     0,   218,   221,   225,   223,    15
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,   117,   118,   292,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   217,
     134,   176,    26,    70,    44,    45,    28,    29,    30,    31,
      78,    79,    80,   168,   169,    32,    59,    60,    33,    34,
      61,    47,    48,    64,   325,   155,   156,   157,   199,   326,
     254,   244,   245,   246,   247,   248,   136,   137,   138,   139,
     140,   141,   142,   143,   144,    35,    36,    37,    72
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -309
static const yytype_int16 yypact[] =
{
    1490,  -309,  -309,  -309,  -309,  -309,  -309,  -309,  -309,  -309,
    -309,  -309,  -309,  -309,  -309,  -309,  -309,  -309,  -309,  -309,
    -309,  -309,  -309,  -309,  -309,    47,  -309,    17,  1490,  1490,
    -309,    58,  -309,  1490,  1490,  1226,  -309,  -309,   -46,    79,
    -309,    49,    25,  -309,   -18,  -309,  1164,    75,    22,  -309,
    -309,    -8,  1515,  -309,  -309,  -309,  -309,    79,    76,   -28,
    -309,    73,  -309,  -309,    25,    49,  -309,   394,   847,  -309,
      17,  -309,  1378,  1123,   341,    75,  1515,  1515,  1293,  -309,
      27,  1515,   124,   988,  -309,     7,  -309,  -309,  -309,  -309,
      88,   105,  -309,  -309,   995,  1002,  1002,   988,   133,    38,
     122,   167,   600,   173,   260,   192,   193,   151,   665,  -309,
    -309,  -309,  -309,  -309,  -309,  -309,  -309,  -309,   166,   298,
     988,  -309,    82,   142,   214,    54,   229,   223,   183,   215,
     273,    37,  -309,  -309,    28,  -309,  -309,  -309,  -309,   472,
    -309,  -309,  -309,  -309,  -309,  -309,   762,  -309,  -309,  -309,
    -309,  -309,  -309,    16,   232,   231,  -309,    72,   104,  -309,
     236,   238,   715,  1332,  -309,  -309,  -309,   988,    43,  -309,
     224,  -309,  -309,    56,  -309,  -309,  -309,  -309,  -309,   600,
     665,  -309,   665,  -309,  -309,   225,   600,   988,   988,   988,
     244,   550,   239,  -309,  -309,  -309,    61,   114,   100,   247,
     323,  -309,  -309,   861,   988,   327,  -309,  -309,  -309,  -309,
    -309,  -309,  -309,  -309,  -309,  -309,  -309,   988,  -309,   988,
     988,   988,   988,   988,   988,   988,   988,   988,   988,   988,
     988,   988,   988,   988,   988,   988,   988,   988,   988,  -309,
    -309,  -309,   988,   329,  -309,   174,   847,    63,  -309,  1073,
     875,  -309,    21,  -309,   101,  -309,  1464,  -309,   330,   769,
    -309,  -309,   988,  -309,   264,   265,  -309,  -309,    27,  -309,
     988,  -309,  -309,   269,   270,   600,  -309,   117,   164,   168,
     272,   567,   567,  -309,  -309,  -309,  1251,   127,  -309,   889,
    -309,  -309,   171,  -309,   -17,  -309,  -309,  -309,  -309,  -309,
      82,    82,   142,   142,   214,   214,   214,   214,    54,    54,
     229,   223,   183,   215,   273,   -23,  -309,   278,  -309,  -309,
     730,  -309,  -309,  -309,  -309,   274,   275,  -309,   281,   282,
     101,  1423,   903,  -309,  -309,  -309,   283,   284,  -309,  -309,
    -309,  -309,   268,   268,  -309,   600,   600,   600,   988,   954,
     981,   762,  -309,  -309,   988,  -309,   988,  -309,  -309,  -309,
     847,  -309,  -309,  -309,  -309,  -309,   287,  -309,   286,   288,
    -309,  -309,   300,  -309,  -309,   187,   600,   196,   600,   203,
     178,  -309,  -309,  -309,  -309,  -309,  -309,   600,   271,  -309,
     600,  -309,   600,  -309,   748,  -309,  -309,  -309,  -309,  -309
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -309,  -309,  -309,  -309,   -20,  -309,  -111,    44,    67,    52,
      65,   128,   126,   129,   130,   131,  -309,   -80,   -67,  -309,
     -76,   -89,   -25,     0,  -309,   297,  -309,   -37,  -309,  -309,
     290,   -41,   -59,  -309,    96,  -309,   311,   -74,   233,  -309,
     -22,   -35,   -26,   -38,   -69,  -309,   113,  -309,     4,  -130,
    -225,   -66,    19,  -308,  -309,   132,   -88,  -309,     9,  -309,
     234,  -185,  -309,  -309,  -309,  -309,   336,  -309,  -309
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint16 yytable[] =
{
      27,   147,   148,   175,   154,    46,   282,   161,   185,   218,
      58,   178,   360,    75,   190,    77,    63,   175,   164,    40,
      40,    69,   171,   253,    40,    40,    57,   330,    49,    50,
      40,   196,   197,    53,    54,    27,   162,   166,    88,    77,
      77,    77,   135,    90,    77,    84,    85,   149,    90,   198,
      38,   238,    40,   355,   236,    71,    65,   238,   170,    58,
       8,    51,   330,   174,    76,   356,   226,   227,   288,    17,
      18,    77,    66,   153,   181,   183,   184,   174,   267,   147,
     177,   150,    58,   249,    41,   250,   360,   175,   249,    41,
     250,   272,    42,    42,    41,   265,   349,   350,   276,   178,
     174,    42,   238,    42,   197,   187,   197,    43,   297,   298,
     299,   277,   278,   279,   135,   167,    41,   268,   239,    39,
     259,   198,   166,   198,   237,    42,    77,   252,   294,   271,
      52,   251,   242,   269,   243,   238,   293,   228,   229,     8,
     257,    86,    73,    77,    74,    77,   258,   174,    17,    18,
     296,   284,   322,   317,   145,    92,    93,    94,   219,    95,
      96,   315,   175,   220,   221,    83,   281,   286,   331,   250,
     332,   316,   287,   200,   201,   202,    42,    68,   352,   147,
     321,   341,   285,   329,   273,   345,   274,   344,   238,   188,
     175,   238,   336,   179,   286,   337,   250,   172,   173,   174,
     174,   174,   174,   174,   174,   174,   174,   174,   174,   174,
     174,   174,   174,   174,   174,   174,   174,    75,   108,   222,
     223,   186,   174,   252,   224,   225,   110,   111,   112,   113,
     114,   115,   346,   203,   189,   204,   347,   205,   238,   353,
     191,   195,   238,   230,   231,   354,   170,   319,   320,   153,
     174,   393,   394,   147,   359,   388,   153,   372,   373,   374,
     287,   238,   366,   192,   390,   369,   300,   301,   233,   174,
     238,   392,   375,   377,   379,    62,   382,   238,   304,   305,
     306,   307,   193,   194,   147,    81,   153,   381,   389,   235,
     391,   302,   303,   147,   383,   308,   309,    87,   232,   395,
     255,   234,   397,   280,   398,   256,   260,    62,   261,    81,
      81,    81,   270,   275,    81,   289,   206,   207,   208,   209,
     210,   211,   212,   213,   214,   215,   290,   147,   359,   283,
     295,   153,   318,   335,   338,   339,   174,   342,   343,   348,
     351,    81,   361,   362,   145,    92,    93,    94,   357,    95,
      96,   363,   364,   370,   371,   384,   385,   387,   386,   311,
     310,   396,    89,   312,   340,   313,   163,   314,    82,   334,
     380,    56,   158,   241,     0,     0,     8,     0,     0,   323,
       0,     0,     0,     0,     0,    17,    18,   216,     0,     0,
       0,    62,     0,     0,     0,    87,    81,    91,    92,    93,
      94,     0,    95,    96,     0,     0,     0,     0,   108,     0,
       0,   159,     0,    81,     0,    81,   110,   160,   112,   113,
     114,   115,     1,     2,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,     0,    97,    98,
      99,     0,   100,   101,   102,   103,   104,   105,   106,   107,
       0,   108,     0,     0,     0,     0,    67,   109,     0,   110,
     111,   112,   113,   114,   115,    91,    92,    93,    94,     0,
      95,    96,     0,     0,   116,     0,     0,     0,     0,     0,
       0,     0,    87,     0,     0,     0,     0,     0,     0,     0,
       1,     2,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,     0,    97,    98,    99,     0,
     100,   101,   102,   103,   104,   105,   106,   107,     0,   108,
       0,     0,     0,     0,    67,   240,     0,   110,   111,   112,
     113,   114,   115,   145,    92,    93,    94,     0,    95,    96,
       0,     0,   116,     0,     0,     0,     0,     0,     0,     0,
     145,    92,    93,    94,     0,    95,    96,     0,     1,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    91,    92,    93,    94,     0,    95,    96,
       0,     0,     0,     0,     0,     0,     0,   108,     0,     0,
       0,     0,     0,     0,     0,   110,   111,   112,   113,   114,
     115,     0,     0,     0,   108,     0,     0,     0,     0,     0,
     116,     0,   110,   111,   112,   113,   114,   115,     0,     0,
       0,     0,     0,     0,    97,    98,    99,   116,   100,   101,
     102,   103,   104,   105,   106,   107,     0,   108,   145,    92,
      93,    94,    67,    95,    96,   110,   111,   112,   113,   114,
     115,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     116,     0,     0,     1,     0,     0,     0,     0,     0,     0,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,   145,    92,
      93,    94,     0,    95,    96,     0,     0,     0,     0,     0,
       0,     0,   108,   145,    92,    93,    94,     0,    95,    96,
     110,   111,   112,   113,   114,   115,   262,     0,     0,     0,
       8,   145,    92,    93,    94,     0,    95,    96,     0,    17,
      18,     0,     0,     0,     0,   145,    92,    93,    94,     0,
      95,    96,   145,    92,    93,    94,     0,    95,    96,     0,
       0,     0,   108,     0,     0,   263,     0,     0,     0,     0,
     110,   264,   112,   113,   114,   115,     0,   108,     0,   242,
       0,   243,   146,   358,     8,   110,   111,   112,   113,   114,
     115,     0,     0,    17,    18,   108,     0,   242,     0,   243,
     146,   399,     0,   110,   111,   112,   113,   114,   115,   108,
       0,   242,     0,   243,   146,     0,   108,   110,   111,   112,
     113,   114,   115,     0,   110,   111,   112,   113,   114,   115,
     145,    92,    93,    94,     0,    95,    96,     0,     0,     0,
       0,     0,     0,     0,   145,    92,    93,    94,     0,    95,
      96,     0,     0,     0,     0,     0,     0,     0,   145,    92,
      93,    94,     0,    95,    96,     0,     0,     0,     0,     0,
       0,     0,   145,    92,    93,    94,     0,    95,    96,     0,
       0,     0,     0,     0,     0,     0,   145,    92,    93,    94,
       0,    95,    96,     0,   108,     0,     0,     0,     0,   146,
       0,     0,   110,   111,   112,   113,   114,   115,   108,   291,
       0,     0,     0,     0,     0,     0,   110,   111,   112,   113,
     114,   115,   108,     0,     0,   327,     0,     0,     0,     0,
     110,   328,   112,   113,   114,   115,   108,   145,    92,    93,
      94,   351,    95,    96,   110,   111,   112,   113,   114,   115,
     108,     0,     0,   367,     0,     0,     0,     0,   110,   368,
     112,   113,   114,   115,   145,    92,    93,    94,     0,    95,
      96,   145,    92,    93,    94,     0,    95,    96,   145,    92,
      93,    94,     0,    95,    96,   145,    92,    93,    94,     0,
      95,    96,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   108,   376,     0,     0,     0,     0,     0,     0,   110,
     111,   112,   113,   114,   115,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   108,   378,
       0,     0,     0,     0,     0,   108,   110,   111,   112,   113,
     114,   115,   180,   110,   111,   112,   113,   114,   115,   182,
     110,   111,   112,   113,   114,   115,    40,   110,   111,   112,
     113,   114,   115,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     1,     2,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,   151,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     249,   324,   250,     0,     0,     0,     0,     0,     0,    42,
       0,     1,     2,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   152,     1,     2,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    55,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    67,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    68,     1,     2,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,     1,
       2,     3,     4,     5,     6,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   286,   324,
     250,     1,     0,     0,     0,     0,     0,    42,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       1,     0,     0,     0,     0,     0,   165,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    23,    24,    25,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   266,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      67,     1,     2,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   365,     1,     2,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,   333,     1,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      23,    24,    25,     1,     0,     0,     0,     0,     0,     0,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25
};

static const yytype_int16 yycheck[] =
{
       0,    68,    68,    83,    73,    27,   191,    74,    97,   120,
       3,    85,   320,    48,   102,    52,    42,    97,    77,     3,
       3,    46,    81,   153,     3,     3,    72,   252,    28,    29,
       3,   107,   108,    33,    34,    35,    74,    78,    64,    76,
      77,    78,    67,    65,    81,    73,    74,    72,    70,   108,
       3,    74,     3,    70,    17,    46,    74,    74,    80,     3,
      35,     3,   287,    83,    72,    88,    12,    13,   198,    44,
      45,   108,    90,    73,    94,    95,    96,    97,   167,   146,
      73,    72,     3,    67,    67,    69,   394,   167,    67,    67,
      69,   179,    76,    76,    67,   162,   281,   282,   186,   173,
     120,    76,    74,    76,   180,    67,   182,    90,   219,   220,
     221,   187,   188,   189,   139,    88,    67,    74,    90,    72,
     158,   180,   163,   182,    87,    76,   163,   153,   204,    73,
      72,   153,    69,    90,    71,    74,   203,    83,    84,    35,
      68,    68,    67,   180,    69,   182,    74,   167,    44,    45,
     217,    90,    89,   242,     3,     4,     5,     6,    76,     8,
       9,   237,   242,    81,    82,    89,   191,    67,    67,    69,
      69,   238,   198,     7,     8,     9,    76,    89,   289,   246,
     246,   270,    68,   250,   180,    68,   182,   275,    74,    67,
     270,    74,   259,    88,    67,   262,    69,    73,    74,   219,
     220,   221,   222,   223,   224,   225,   226,   227,   228,   229,
     230,   231,   232,   233,   234,   235,   236,   252,    67,    77,
      78,    88,   242,   249,    10,    11,    75,    76,    77,    78,
      79,    80,    68,    67,    67,    69,    68,    71,    74,    68,
      67,    90,    74,    14,    15,    74,   268,    73,    74,   249,
     270,    73,    74,   320,   320,    68,   256,   345,   346,   347,
     286,    74,   331,     3,    68,   332,   222,   223,    85,   289,
      74,    68,   348,   349,   350,    42,   356,    74,   226,   227,
     228,   229,    90,    90,   351,    52,   286,   354,   376,    16,
     378,   224,   225,   360,   360,   230,   231,    64,    75,   387,
      68,    86,   390,    59,   392,    74,    70,    74,    70,    76,
      77,    78,    88,    88,    81,    68,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,     3,   394,   394,    90,
       3,   331,     3,     3,    70,    70,   356,    68,    68,    67,
      72,   108,    68,    68,     3,     4,     5,     6,    70,     8,
       9,    70,    70,    70,    70,    68,    70,    57,    70,   233,
     232,    90,    65,   234,   268,   235,    76,   236,    57,   256,
     351,    35,    31,   139,    -1,    -1,    35,    -1,    -1,   247,
      -1,    -1,    -1,    -1,    -1,    44,    45,    89,    -1,    -1,
      -1,   158,    -1,    -1,    -1,   162,   163,     3,     4,     5,
       6,    -1,     8,     9,    -1,    -1,    -1,    -1,    67,    -1,
      -1,    70,    -1,   180,    -1,   182,    75,    76,    77,    78,
      79,    80,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    -1,    54,    55,
      56,    -1,    58,    59,    60,    61,    62,    63,    64,    65,
      -1,    67,    -1,    -1,    -1,    -1,    72,    73,    -1,    75,
      76,    77,    78,    79,    80,     3,     4,     5,     6,    -1,
       8,     9,    -1,    -1,    90,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   259,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    -1,    54,    55,    56,    -1,
      58,    59,    60,    61,    62,    63,    64,    65,    -1,    67,
      -1,    -1,    -1,    -1,    72,    73,    -1,    75,    76,    77,
      78,    79,    80,     3,     4,     5,     6,    -1,     8,     9,
      -1,    -1,    90,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       3,     4,     5,     6,    -1,     8,     9,    -1,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,     3,     4,     5,     6,    -1,     8,     9,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    67,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    75,    76,    77,    78,    79,
      80,    -1,    -1,    -1,    67,    -1,    -1,    -1,    -1,    -1,
      90,    -1,    75,    76,    77,    78,    79,    80,    -1,    -1,
      -1,    -1,    -1,    -1,    54,    55,    56,    90,    58,    59,
      60,    61,    62,    63,    64,    65,    -1,    67,     3,     4,
       5,     6,    72,     8,     9,    75,    76,    77,    78,    79,
      80,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      90,    -1,    -1,    28,    -1,    -1,    -1,    -1,    -1,    -1,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,     3,     4,
       5,     6,    -1,     8,     9,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    67,     3,     4,     5,     6,    -1,     8,     9,
      75,    76,    77,    78,    79,    80,    31,    -1,    -1,    -1,
      35,     3,     4,     5,     6,    -1,     8,     9,    -1,    44,
      45,    -1,    -1,    -1,    -1,     3,     4,     5,     6,    -1,
       8,     9,     3,     4,     5,     6,    -1,     8,     9,    -1,
      -1,    -1,    67,    -1,    -1,    70,    -1,    -1,    -1,    -1,
      75,    76,    77,    78,    79,    80,    -1,    67,    -1,    69,
      -1,    71,    72,    73,    35,    75,    76,    77,    78,    79,
      80,    -1,    -1,    44,    45,    67,    -1,    69,    -1,    71,
      72,    73,    -1,    75,    76,    77,    78,    79,    80,    67,
      -1,    69,    -1,    71,    72,    -1,    67,    75,    76,    77,
      78,    79,    80,    -1,    75,    76,    77,    78,    79,    80,
       3,     4,     5,     6,    -1,     8,     9,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     3,     4,     5,     6,    -1,     8,
       9,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     3,     4,
       5,     6,    -1,     8,     9,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     3,     4,     5,     6,    -1,     8,     9,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     3,     4,     5,     6,
      -1,     8,     9,    -1,    67,    -1,    -1,    -1,    -1,    72,
      -1,    -1,    75,    76,    77,    78,    79,    80,    67,    68,
      -1,    -1,    -1,    -1,    -1,    -1,    75,    76,    77,    78,
      79,    80,    67,    -1,    -1,    70,    -1,    -1,    -1,    -1,
      75,    76,    77,    78,    79,    80,    67,     3,     4,     5,
       6,    72,     8,     9,    75,    76,    77,    78,    79,    80,
      67,    -1,    -1,    70,    -1,    -1,    -1,    -1,    75,    76,
      77,    78,    79,    80,     3,     4,     5,     6,    -1,     8,
       9,     3,     4,     5,     6,    -1,     8,     9,     3,     4,
       5,     6,    -1,     8,     9,     3,     4,     5,     6,    -1,
       8,     9,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    67,    68,    -1,    -1,    -1,    -1,    -1,    -1,    75,
      76,    77,    78,    79,    80,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    67,    68,
      -1,    -1,    -1,    -1,    -1,    67,    75,    76,    77,    78,
      79,    80,    67,    75,    76,    77,    78,    79,    80,    67,
      75,    76,    77,    78,    79,    80,     3,    75,    76,    77,
      78,    79,    80,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,     3,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      67,    68,    69,    -1,    -1,    -1,    -1,    -1,    -1,    76,
      -1,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    68,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     0,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    72,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    89,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
      39,    40,    41,    42,    43,    44,    45,    46,    47,    48,
      49,    50,    51,    52,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    67,    68,
      69,    28,    -1,    -1,    -1,    -1,    -1,    76,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      28,    -1,    -1,    -1,    -1,    -1,    73,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    73,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      72,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    68,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    28,    -1,    -1,    -1,    -1,    -1,    -1,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,   113,   114,   117,   118,
     119,   120,   126,   129,   130,   156,   157,   158,     3,    72,
       3,    67,    76,    90,   115,   116,   131,   132,   133,   114,
     114,     3,    72,   114,   114,     0,   157,    72,     3,   127,
     128,   131,   129,   133,   134,    74,    90,    72,    89,   113,
     114,   149,   159,    67,    69,   132,    72,   118,   121,   122,
     123,   129,   127,    89,    73,    74,    68,   129,   133,   116,
     131,     3,     4,     5,     6,     8,     9,    54,    55,    56,
      58,    59,    60,    61,    62,    63,    64,    65,    67,    73,
      75,    76,    77,    78,    79,    80,    90,    92,    93,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   111,   113,   147,   148,   149,   150,
     151,   152,   153,   154,   155,     3,    72,   109,   142,   113,
     149,     3,    68,   114,   135,   136,   137,   138,    31,    70,
      76,   109,   134,   121,   123,    73,   122,    88,   124,   125,
     131,   123,    73,    74,    95,   108,   112,    73,   128,    88,
      67,    95,    67,    95,    95,   112,    88,    67,    67,    67,
     147,    67,     3,    90,    90,    90,   111,   111,   123,   139,
       7,     8,     9,    67,    69,    71,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    89,   110,    97,    76,
      81,    82,    77,    78,    10,    11,    12,    13,    83,    84,
      14,    15,    75,    85,    86,    16,    17,    87,    74,    90,
      73,   151,    69,    71,   142,   143,   144,   145,   146,    67,
      69,   131,   133,   140,   141,    68,    74,    68,    74,   134,
      70,    70,    31,    70,    76,   109,    73,   112,    74,    90,
      88,    73,   147,   139,   139,    88,   147,   111,   111,   111,
      59,   113,   152,    90,    90,    68,    67,   133,   140,    68,
       3,    68,    94,   109,   111,     3,   109,    97,    97,    97,
      98,    98,    99,    99,   100,   100,   100,   100,   101,   101,
     102,   103,   104,   105,   106,   111,   109,   112,     3,    73,
      74,   142,    89,   146,    68,   135,   140,    70,    76,   109,
     141,    67,    69,    53,   137,     3,   109,   109,    70,    70,
     125,   112,    68,    68,   147,    68,    68,    68,    67,   152,
     152,    72,    97,    68,    74,    70,    88,    70,    73,   142,
     144,    68,    68,    70,    70,    68,   135,    70,    76,   109,
      70,    70,   147,   147,   147,   111,    68,   111,    68,   111,
     143,   109,   108,   142,    68,    70,    70,    57,    68,   147,
      68,   147,    68,    73,    74,   147,    90,   147,   147,    73
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *bottom, yytype_int16 *top)
#else
static void
yy_stack_print (bottom, top)
    yytype_int16 *bottom;
    yytype_int16 *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yyrule)
    YYSTYPE *yyvsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      fprintf (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       		       );
      fprintf (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  YYUSE (yyvaluep);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;
#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  yytype_int16 yyssa[YYINITDEPTH];
  yytype_int16 *yyss = yyssa;
  yytype_int16 *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     look-ahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to look-ahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 96 "c99.y"
    {(yyval.a_tree) = make_variable((yyvsp[(1) - (1)].a_variable));;}
    break;

  case 3:
#line 97 "c99.y"
    {(yyval.a_tree) = make_number((yyvsp[(1) - (1)].a_number));;}
    break;

  case 4:
#line 98 "c99.y"
    {(yyval.a_tree) = make_variable((yyvsp[(1) - (1)].a_variable));;}
    break;

  case 5:
#line 99 "c99.y"
    {(yyval.a_tree) = (yyvsp[(2) - (3)].a_tree);;}
    break;

  case 6:
#line 103 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 7:
#line 104 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (4)].a_tree), "[]", (yyvsp[(3) - (4)].a_tree));;}
    break;

  case 8:
#line 105 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (3)].a_tree);;}
    break;

  case 9:
#line 106 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (4)].a_tree), "()", (yyvsp[(3) - (4)].a_tree));;}
    break;

  case 10:
#line 107 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), ".", make_variable((yyvsp[(3) - (3)].a_variable)));;}
    break;

  case 11:
#line 108 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "->", make_variable((yyvsp[(3) - (3)].a_variable)));;}
    break;

  case 12:
#line 109 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (2)].a_tree), "++", NULL);;}
    break;

  case 13:
#line 110 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (2)].a_tree), "--", NULL);;}
    break;

  case 14:
#line 111 "c99.y"
    {(yyval.a_tree) = (yyvsp[(5) - (6)].a_tree);;}
    break;

  case 15:
#line 112 "c99.y"
    {(yyval.a_tree) = (yyvsp[(5) - (7)].a_tree);;}
    break;

  case 16:
#line 116 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 17:
#line 117 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), ",", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 18:
#line 121 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 19:
#line 122 "c99.y"
    {(yyval.a_tree) = make_operator(NULL, "++", (yyvsp[(2) - (2)].a_tree));;}
    break;

  case 20:
#line 123 "c99.y"
    {(yyval.a_tree) = make_operator(NULL, "--", (yyvsp[(2) - (2)].a_tree));;}
    break;

  case 21:
#line 124 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (2)].a_tree), "", (yyvsp[(2) - (2)].a_tree));;}
    break;

  case 22:
#line 125 "c99.y"
    {(yyval.a_tree) = make_operator(NULL, "SIZEOF", (yyvsp[(2) - (2)].a_tree));;}
    break;

  case 23:
#line 126 "c99.y"
    {(yyval.a_tree) = make_operator(NULL, "SIZEOF", NULL);;}
    break;

  case 24:
#line 130 "c99.y"
    {(yyval.a_tree) = make_variable("&");;}
    break;

  case 25:
#line 131 "c99.y"
    {(yyval.a_tree) = make_variable("*");;}
    break;

  case 26:
#line 132 "c99.y"
    {(yyval.a_tree) = make_variable("+");;}
    break;

  case 27:
#line 133 "c99.y"
    {(yyval.a_tree) = make_variable("-");;}
    break;

  case 28:
#line 134 "c99.y"
    {(yyval.a_tree) = make_variable("~");;}
    break;

  case 29:
#line 135 "c99.y"
    {(yyval.a_tree) = make_variable("!");;}
    break;

  case 30:
#line 139 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 31:
#line 140 "c99.y"
    {(yyval.a_tree) = make_operator(NULL, "cast", (yyvsp[(4) - (4)].a_tree));;}
    break;

  case 32:
#line 144 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 33:
#line 145 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "*", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 34:
#line 146 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "/", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 35:
#line 147 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "%", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 36:
#line 151 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 37:
#line 152 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "+", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 38:
#line 153 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "-", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 39:
#line 157 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 40:
#line 158 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "<<", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 41:
#line 159 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "<<", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 42:
#line 163 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 43:
#line 164 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "<", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 44:
#line 165 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), ">", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 45:
#line 166 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "<=", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 46:
#line 167 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), ">=", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 47:
#line 171 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 48:
#line 172 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "==", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 49:
#line 173 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "!=", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 50:
#line 177 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 51:
#line 178 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "&", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 52:
#line 182 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 53:
#line 183 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "^", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 54:
#line 187 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 55:
#line 188 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "|", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 56:
#line 192 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 57:
#line 193 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "&&", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 58:
#line 197 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 59:
#line 198 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "||", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 60:
#line 202 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 61:
#line 203 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (5)].a_tree);;}
    break;

  case 62:
#line 207 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 63:
#line 208 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), (yyvsp[(2) - (3)].a_variable), (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 64:
#line 212 "c99.y"
    {(yyval.a_variable) = "=";;}
    break;

  case 65:
#line 213 "c99.y"
    {(yyval.a_variable) = "*=";;}
    break;

  case 66:
#line 214 "c99.y"
    {(yyval.a_variable) = "/=";;}
    break;

  case 67:
#line 215 "c99.y"
    {(yyval.a_variable) = "%=";;}
    break;

  case 68:
#line 216 "c99.y"
    {(yyval.a_variable) = "+=";;}
    break;

  case 69:
#line 217 "c99.y"
    {(yyval.a_variable) = "-=";;}
    break;

  case 70:
#line 218 "c99.y"
    {(yyval.a_variable) = "=";;}
    break;

  case 71:
#line 219 "c99.y"
    {(yyval.a_variable) = "=";;}
    break;

  case 72:
#line 220 "c99.y"
    {(yyval.a_variable) = "&=";;}
    break;

  case 73:
#line 221 "c99.y"
    {(yyval.a_variable) = "^=";;}
    break;

  case 74:
#line 222 "c99.y"
    {(yyval.a_variable) = "|=";;}
    break;

  case 75:
#line 226 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 76:
#line 227 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), ",", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 78:
#line 235 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (2)].a_tree);;}
    break;

  case 79:
#line 236 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "declaration_specifiers", (yyvsp[(2) - (3)].a_tree));;}
    break;

  case 80:
#line 240 "c99.y"
    {(yyval.a_tree) = make_variable("storage_class_specifier");;}
    break;

  case 81:
#line 241 "c99.y"
    {(yyval.a_tree) = make_variable("storage_class_specifier declaration_specifiers");;}
    break;

  case 82:
#line 242 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 83:
#line 243 "c99.y"
    {(yyval.a_tree) = make_variable("type_specifier declaration_specifiers");;}
    break;

  case 84:
#line 244 "c99.y"
    {(yyval.a_tree) = make_variable("type_qualifier");;}
    break;

  case 85:
#line 245 "c99.y"
    {(yyval.a_tree) = make_variable("type_qualifier declaration_specifiers");;}
    break;

  case 86:
#line 246 "c99.y"
    {(yyval.a_tree) = make_variable("function_specifier");;}
    break;

  case 87:
#line 247 "c99.y"
    {(yyval.a_tree) = make_variable("function_specifier declaration_specifiers");;}
    break;

  case 88:
#line 251 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 89:
#line 252 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), ",", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 90:
#line 256 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 91:
#line 257 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "=", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 97:
#line 269 "c99.y"
    {(yyval.a_tree) = make_variable("void");;}
    break;

  case 98:
#line 270 "c99.y"
    {(yyval.a_tree) = make_variable("char");;}
    break;

  case 99:
#line 271 "c99.y"
    {(yyval.a_tree) = make_variable("short");;}
    break;

  case 100:
#line 272 "c99.y"
    {(yyval.a_tree) = make_variable("int");;}
    break;

  case 101:
#line 273 "c99.y"
    {(yyval.a_tree) = make_variable("long");;}
    break;

  case 102:
#line 274 "c99.y"
    {(yyval.a_tree) = make_variable("float");;}
    break;

  case 103:
#line 275 "c99.y"
    {(yyval.a_tree) = make_variable("double");;}
    break;

  case 104:
#line 276 "c99.y"
    {(yyval.a_tree) = make_variable("signed");;}
    break;

  case 105:
#line 277 "c99.y"
    {(yyval.a_tree) = make_variable("unsigned");;}
    break;

  case 106:
#line 278 "c99.y"
    {(yyval.a_tree) = make_variable("bool");;}
    break;

  case 107:
#line 279 "c99.y"
    {(yyval.a_tree) = make_variable("copmlex");;}
    break;

  case 108:
#line 280 "c99.y"
    {(yyval.a_tree) = make_variable("imaginary");;}
    break;

  case 109:
#line 281 "c99.y"
    {(yyval.a_tree) = make_variable("struct or union");;}
    break;

  case 110:
#line 282 "c99.y"
    {(yyval.a_tree) = make_variable("enum spec");;}
    break;

  case 111:
#line 283 "c99.y"
    {(yyval.a_tree) = make_variable("type name");;}
    break;

  case 142:
#line 353 "c99.y"
    {(yyval.a_tree) = make_variable("pointer");;}
    break;

  case 143:
#line 354 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 144:
#line 359 "c99.y"
    {(yyval.a_tree) = make_variable((yyvsp[(1) - (1)].a_variable));;}
    break;

  case 145:
#line 360 "c99.y"
    {(yyval.a_tree) = (yyvsp[(2) - (3)].a_tree);;}
    break;

  case 146:
#line 361 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (5)].a_tree);;}
    break;

  case 147:
#line 362 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (4)].a_tree);;}
    break;

  case 148:
#line 363 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (4)].a_tree);;}
    break;

  case 149:
#line 364 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (6)].a_tree);;}
    break;

  case 150:
#line 365 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (6)].a_tree);;}
    break;

  case 151:
#line 366 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (5)].a_tree);;}
    break;

  case 152:
#line 367 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (4)].a_tree);;}
    break;

  case 153:
#line 368 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (3)].a_tree);;}
    break;

  case 154:
#line 369 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (4)].a_tree);;}
    break;

  case 155:
#line 370 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (4)].a_tree);;}
    break;

  case 156:
#line 371 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (3)].a_tree);;}
    break;

  case 188:
#line 434 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 189:
#line 435 "c99.y"
    {(yyval.a_tree) = (yyvsp[(2) - (3)].a_tree);;}
    break;

  case 190:
#line 436 "c99.y"
    {(yyval.a_tree) = (yyvsp[(2) - (4)].a_tree);;}
    break;

  case 191:
#line 440 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 192:
#line 441 "c99.y"
    {(yyval.a_tree) = make_variable("a");;}
    break;

  case 193:
#line 442 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), ",", (yyvsp[(3) - (3)].a_tree));;}
    break;

  case 194:
#line 443 "c99.y"
    {(yyval.a_tree) = make_variable("c");;}
    break;

  case 200:
#line 461 "c99.y"
    {(yyval.a_tree) = make_variable("LABELED");;}
    break;

  case 201:
#line 462 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 202:
#line 463 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 203:
#line 464 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 204:
#line 465 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 205:
#line 466 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 209:
#line 476 "c99.y"
    {(yyval.a_tree) = make_variable("{}");;}
    break;

  case 210:
#line 477 "c99.y"
    {(yyval.a_tree) = (yyvsp[(2) - (3)].a_tree);;}
    break;

  case 211:
#line 481 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 212:
#line 482 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (2)].a_tree), ";",(yyvsp[(2) - (2)].a_tree));;}
    break;

  case 213:
#line 486 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 214:
#line 487 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (1)].a_tree);;}
    break;

  case 215:
#line 491 "c99.y"
    {(yyval.a_tree) = make_variable(";");;}
    break;

  case 216:
#line 492 "c99.y"
    {(yyval.a_tree) = (yyvsp[(1) - (2)].a_tree);;}
    break;

  case 217:
#line 496 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(3) - (5)].a_tree) ,"if", (yyvsp[(5) - (5)].a_tree));;}
    break;

  case 218:
#line 497 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(3) - (7)].a_tree), "if", make_operator((yyvsp[(5) - (7)].a_tree), "else", (yyvsp[(7) - (7)].a_tree)));;}
    break;

  case 219:
#line 498 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(3) - (5)].a_tree), "switch", (yyvsp[(5) - (5)].a_tree));;}
    break;

  case 220:
#line 502 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(3) - (5)].a_tree), "while", (yyvsp[(5) - (5)].a_tree));;}
    break;

  case 221:
#line 503 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(2) - (7)].a_tree), "do while", (yyvsp[(5) - (7)].a_tree));;}
    break;

  case 222:
#line 504 "c99.y"
    {(yyval.a_tree) = make_variable("for");;}
    break;

  case 223:
#line 505 "c99.y"
    {(yyval.a_tree) = make_variable("for");;}
    break;

  case 224:
#line 506 "c99.y"
    {(yyval.a_tree) = make_variable("for");;}
    break;

  case 225:
#line 507 "c99.y"
    {(yyval.a_tree) = make_variable("for");;}
    break;

  case 226:
#line 511 "c99.y"
    {(yyval.a_tree) = make_operator(NULL, "goto", make_variable((yyvsp[(2) - (3)].a_variable)));;}
    break;

  case 227:
#line 512 "c99.y"
    {(yyval.a_tree) = make_variable("continue");;}
    break;

  case 228:
#line 513 "c99.y"
    {(yyval.a_tree) = make_variable("break");;}
    break;

  case 229:
#line 514 "c99.y"
    {(yyval.a_tree) = make_variable("return");;}
    break;

  case 230:
#line 515 "c99.y"
    {(yyval.a_tree) = make_operator(NULL, "return", (yyvsp[(2) - (3)].a_tree));;}
    break;

  case 231:
#line 519 "c99.y"
    {printtree((yyvsp[(1) - (1)].a_tree), 1);;}
    break;

  case 232:
#line 520 "c99.y"
    {printtree((yyvsp[(2) - (2)].a_tree), 1);;}
    break;

  case 233:
#line 524 "c99.y"
    {(yyval.a_tree) = make_operator(NULL, "function_definition", (yyvsp[(1) - (1)].a_tree));;}
    break;

  case 234:
#line 525 "c99.y"
    {(yyval.a_tree) = make_operator(NULL, "decl", make_number(1));;}
    break;

  case 235:
#line 529 "c99.y"
    {(yyval.a_tree) = make_operator(make_number(1), "+", make_number(1));;}
    break;

  case 236:
#line 530 "c99.y"
    {(yyval.a_tree) = make_operator((yyvsp[(1) - (3)].a_tree), "def", make_operator((yyvsp[(2) - (3)].a_tree), "", (yyvsp[(3) - (3)].a_tree)));;}
    break;

  case 237:
#line 534 "c99.y"
    {(yyval.a_tree) = make_operator(make_number(1), "*", make_number(1));;}
    break;

  case 238:
#line 535 "c99.y"
    {(yyval.a_tree) = make_operator(make_number(1), "/", make_number(1));;}
    break;


/* Line 1267 of yacc.c.  */
#line 2962 "c99.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}


#line 537 "c99.y"



