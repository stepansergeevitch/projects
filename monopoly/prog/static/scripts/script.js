/**
 * Created by stepan on 11/6/16.
 */
function worker()
        {
            $.ajax({
                type:'get',
                url:game_id.toString() + '/update',
                data:"",
                success:function(resp){update(resp)},
                complete:function(){
                    setTimeout(worker,100)
                },
                error:function(){
                }
            })
        }

        $(document).ready(
                function(){
                    setTimeout(worker,100)
                }
        );

        function update(resp)
        {
            if(resp == 'error')
                return
            con = document.getElementById('console_out')
            con.innerHTML = ""
            resp['log'].forEach(function(item,index){
                con.innerHTML += '<p>' + item + '</p>'
            })
            resp['players'].forEach(function(item,index){
                var tmp = document.getElementsByName('p_name' + (index + 1).toString())
                tmp[0].innerHTML = item[0]
                tmp = document.getElementsByName('p_money' + (index + 1).toString())
                tmp[0].innerHTML = 'Money:' + item[1].toString()
            })
        }
