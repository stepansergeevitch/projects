/**
 * Created by stepan on 11/7/16.
 */

function worker(){
    $.ajax(
        {
            type:'get',
                url:'game_list/update',
                data:"",
                success:function(resp){update(resp)},
                complete:function(){
                    setTimeout(worker,300)
                },
                error:function(){
                }
        }
    )
}

var templateListItem =  '<div id="game_list_item">' +
                    '<p class="game_list_item">Name: $gamename</p>' +
                    '<p class="game_list_item" >Players: $players ($pnumber/4)</p>' +
                    '<p class="game_list_item" >Status: $gamestatus </p>' +
                    '<p class="game_list_item" >Creator: $gamecreator </p>' +
                    '<p><form action="#">$templatebuttons</form></p>'
var templateEmpty = ''
var templateButtons = []
templateButtons['start'] =  '<input type="hidden" name="gameid" value="$gameind">' +
                                '<input value="Start" type=submit name="startButton"  class="game_list_item">'
templateButtons['join'] =  '<input type="hidden" name="gameid" value="$gameind">' +
                                '<input value="Join" type=submit name="joinButton" class="game_list_item">'
templateButtons['enter'] =     '<input type="hidden" name="gameid" value="$gameind">' +
                                '<input value="Enter" type=submit name="enterButton" class="game_list_item">'

function contains(list,item){
    var res = false
    list.forEach(function(it,i){
        if (it == item){res = true}
    })
    return res
}

function getButtons(list){
    var res = ''
    if(contains(list,'Manageable') && $.contains(list,'Startable') && !contains(list,'IsStarted')) {
        res += templateButtons['start']
    }
    if(contains(list,'Available') && !contains(list,'Joined')) {
        res += templateButtons['join']
    }
    if(contains(list,'IsStarted')) {
        res += templateButtons['enter']
    }
    return res
}

function generateCode(game,gameind){
    var players = game[1].join()
    var pnumber = game[1].length
    var t = templateListItem.replace('$gamename',game[0])
    t = t.replace('$players',players).replace('$pnumber',pnumber.toString())
    t = t.replace('$gamecreator',game[3]).replace('$gamestatus',game[2])
    var buttons = getButtons(game[2])
    t = t.replace('$templatebuttons',buttons).replace('$gameind',gameind)
    return t
}

function makeHeader(gamesCount){
    var head = document.getElementById("game_list_header")
    head.innerHTML = '<p>Games count: ' + gamesCount.toString() + '</p>'
}

function update(resp){
    if(resp == 'error')
        return
    var cont = document.getElementById("game_list")
    cont.innerHTML = templateEmpty
    var gameind = 0
    resp['games'].forEach(function(item,index){
        cont.innerHTML += generateCode(item,index + 1)
    })
    makeHeader(resp['games'].length)
}