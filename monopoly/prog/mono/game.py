from io import *
import random
from copy import copy
import threading


colors={0: "red",1: "yellow",2: "green",3:"blue"}

class Player:
    def __init__(self,name):
        self.name = name
        self.money = 0
        self.color = 'red'

    def set_location(self,_l):
        self.location = _l

    def __str__(self):
        return self.name

    def decrement_id(self):
        if self.id > 0:
            self.id -= 1

player = Player('Bodya iz optimus')


class Cell:
    def __init__(self,_name,_price):
        self.name = _name
        self.price = _price

    def set_action(self,_action):
        self.action = _action


class Game:

    def __init__(self, _players, _id, _name='game',_creator=player):
        self.players = []
        for i in _players:
            self.addPlayer(i)
        self.cells = [copy(i) for i in stdCells]
        self.creator = _creator
        self.name = _name
        self.log = []
        self.is_started = False
        self.id = _id

    def getLog(self):
        return self.log

    def start(self):
        try:
            if len(self.players) > 1 and not self.is_started:
                self.log = ["server:Game starts", "server:First moves %s" % (self.players[0].name)]
                self.cur = 0
                self.is_started = True
                return None
            else:
                if len(self.players) < 2:
                    return 'Not enough players'
                if self.is_started:
                    return 'Game is already started'
        except Exception:
            return 'Game class internal exception'

    def movePlayer(self):
        f_c, s_c = (random.random() * 6) // 1, (random.random() * 6) // 1

    def endMove(self):
        self.cur = (self.cur + 1) % 4
        self.log += ["server:" + self.players[self.cur].name + " turn"]

    def addLog(self, text):
        self.log += [text]

    def getCells(self):
        return self.cells

    def addPlayer(self, p):
        if self.available():
            self.players += [p]

    def getPlayers(self):
        return [i.name for i in self.players]

    def removePlayer(self, name):
        for i in self.players:
            if i.name == name:
                self.players.remove(i)

    def available(self):
        return len(self.players) < 4

    def contains_player(self, name):
        for i in self.players:
            if i.name == name:
                return True
        return False

    def canBeStarted(self):
        return len(self.players) > 1 and not self.is_started

    def getStatus(self,playerName):
        s = []
        if self.is_started:
            s.append('IsStarted')
        if self.creator.name == playerName:
            s.append('Manageable')
        if self.contains_player(playerName):
            s.append('Joined')
        if self.available():
            s.append('Available')
        else:
            s.append('Full')
        if self.canBeStarted():
            s.append('Startable')
        else:
            s.append('NotEnoughPlayers')
        return s

    def getCreator(self):
        return self.creator

    def toJson(self,playerName):
        return (self.name,[str(j) for j in self.players],self.getStatus(playerName),self.getCreator().name)


globalPlayersList = []
stdCells = [Cell("cell%d" % i, i * 1000) for i in range(1, 41)]
gamesList = [Game([], 0), Game([Player('Sanya'), Player('Bodya'), Player('Vanya')], 1)]

def createGame(gameName,playerName):
    gamesList.append(Game([getPlayerByName(playerName),player],len(gamesList),gameName,getPlayerByName(playerName)))

def getPlayerByName(name):
    for i in globalPlayersList:
        if i.name == name:
            return i
    return None

def getPlayersGame(name):
    for i in gamesList:
        for j in i.players:
            if j.name == name:
                return i
    return None

def nameExisits(name):
    for j in globalPlayersList:
        if j.name == name:
            return True
    return False

def registerPlayer(name):
    globalPlayersList.append(Player(name))

def deletePlayer(name):
    if name in globalPlayersList:
        p_id = globalPlayersList.index(name)
        globalPlayersList.remove(name)
        for i in range(p_id, globalPlayersList):
            globalPlayersList[i].decrement_id()



