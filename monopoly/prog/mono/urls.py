from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^login$',views.login),
    url(r'^update$',views.index_up),
    url(r'^logout$',views.logout),
    url(r'^game_list/update$',views.game_list_up),
    url(r'^start_game/(?P<game_id>[0-9]+)$',views.start_game),
    url(r'^game_list$',views.game_list),
    url(r'^game/(?P<game_id>[0-9]+)/update$', views.game_up),
    url(r'^game/(?P<game_id>[0-9]+)$',views.game),
]

