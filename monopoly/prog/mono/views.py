from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse
from django.template import Template,loader,Context
from django.template.loader import render_to_string
import os
import prog.settings
from mono.game import *
# Create your views here.

def checkName(request):
    return ('name' in request.session) and not (getPlayerByName(request.session['name']) is None)

def myRender(request,doc,context={}):
     return render(request,doc,context=context)
    # return render(request,'index.html',context={'document':'mono/' + doc})


def index(request):
    if 'name'not in request.session:
        return redirect(login)
    if not nameExisits(request.session['name']):
        request.session.pop('name',None)
        return redirect(login)
    return redirect(game_list)

def index_up(request):
    pass

def login(request):
    message = 'Enter your name'
    if request.GET.get('login_sub'):
        s = request.GET.get('login_inp')
        if nameExisits(s):
            return redirect(login)
        request.session['name'] = s
        registerPlayer(s)
        return redirect(index)
    context = Context({"available":True,'message':message})
    return HttpResponse(myRender(request,'login.html',context=context))

def logout(request):
    if 'name' in request.session:
        deletePlayer(request.session['name'])
        request.session.pop('name',None)
        return redirect(index)

def game_list(request,message=''):
    if not checkName(request):
        return redirect(login)
    if request.GET.get('joinButton'):
        gamesList[int(request.GET.get('gameid')) - 1].addPlayer(getPlayerByName(request.session['name']))
        return redirect(index)
    if request.GET.get('startButton'):
        gamesList[int(request.GET.get('gameid')) - 1].start()
        return redirect(index)
    if request.GET.get('enterButton'):
        return redirect(game,game_id=(int(request.GET.get('gameid')) - 1))
    if request.GET.get('createButton'):
        createGame(request.GET.get('gameName'),request.session['name'])
        return redirect(index)
    else:
        return HttpResponse(myRender(request,'game_list.html',context=Context({'message':message})))

def game_list_up(request):
     if request.is_ajax():
         #try:
         return JsonResponse({'games': [i.toJson(request.session['name']) for i in gamesList]})
         #except Exception:
             #return HttpResponse('error')

def start_game(request,game_id):
    try:
        t = 'Index is out of gamelist range'
        t = gamesList[int(game_id) - 1].start()
        if t != None:
            raise Exception
        return redirect(game,game_id)
    except Exception:
        return  HttpResponse(t)

def game(request,game_id):
    game_id = int(game_id)
    if 'name' not in request.session \
    or not gamesList[game_id].contains_player(request.session['name']):
        return HttpResponse('error')
        #return redirect(index)
    if(request.GET.get("s_button")):
        gamesList[game_id].addLog(request.session.get('name') + ':' + request.GET.get("con_inp"))
    #return HttpResponse(str(games_list[game_id].players));
    context = Context({"cells_t": gamesList[game_id].getCells()[0:11], "cells_l":gamesList[game_id].getCells()[39:30:-1],\
                        "cells_r": gamesList[game_id].getCells()[11:20], "cells_b":gamesList[game_id].getCells()[30:30:-1],\
                        "players": gamesList[game_id].getPlayers(), \
                        "log": gamesList[game_id].getLog(),
                        'game_id': gamesList[game_id].id + 1})
    return HttpResponse(render(request, 'game.html', context=context))

def game_up(request,game_id):
    game_id = int(game_id) - 1
    if request.is_ajax():
        try:
            return JsonResponse({'log': gamesList[game_id].getLog(), 'players':[(i.name, i.money) for i in gamesList[game_id].players]})
        except Exception:
            return HttpResponse('error')

def delete_game(request,game_id):
    game_id = int(game_id) - 1
    #TODO

def join_game(request,game_id):
    pass
    #TODO