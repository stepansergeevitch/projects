from movies_library.models import *
from django.db import connection as c

# directors films
# studios films
# actors studios

queries = ['select * from movies_library_movies left join movies_library_directors on movies_library_movies.director_id=\
            movies_library_directors.id where movies_library_directors.name = %s',
           \
           'select * from movies_library_movies left join movies_library_studios on movies_library_movies.studio_id=\
            movies_library_studios.id where movies_library_studios.name = %s',
           \
           'select * from movies_library_studios where movies_library_studios.id in\
           (select movies_library_movies.studio_id from movies_library_movies where movies_library_movies.id in \
           (select movies_library_movies_actors.movies_id from movies_library_movies_actors inner join movies_library_actors\
           on movies_library_movies_actors.actors_id = movies_library_actors.id where movies_library_actors.name = %s))',
           \
           'select * from movies_library_genres where movies_library_genres.id in\
           (select movies_library_movies_genres.genres_id from movies_library_movies_genres where movies_library_movies_genres.movies_id in \
           (select movies_library_movies_actors.movies_id from movies_library_movies_actors inner join movies_library_actors\
           on movies_library_movies_actors.actors_id = movies_library_actors.id where movies_library_actors.name = %s))',
           \
           'select * from movies_library_directors where movies_library_directors.id in \
           (select movies_library_movies.director_id from movies_library_movies where movies_library_movies.studio_id in \
           (select movies_library_studios.id from movies_library_studios where movies_library_studios.name = %s))'
           ]

# films with same or less actors
# films of same genre
# directors with same genres

_models = [Movies,Movies,Studios,Genres,Directors]

set_queries = ['select * from movies_library_movies where not(movies_library_movies.name = %s) and not exists  (select *  from movies_library_movies_actors \
                where movies_library_movies_actors.movies_id = movies_library_movies.id and movies_library_movies_actors.actors_id \
                not in (select movies_library_movies_actors.actors_id from movies_library_movies_actors inner join movies_library_movies\
                on movies_library_movies_actors.movies_id = movies_library_movies.id where movies_library_movies.name = %s))',
               \
               'select * from movies_library_movies as movies where not(movies.name = %s) and not exists (select * from movies_library_movies_genres as genres \
                where genres.movies_id = movies.id and genres.genres_id not in (select movies_library_movies_genres.genres_id \
                from movies_library_movies_genres inner join movies_library_movies on movies_library_movies_genres.movies_id = movies_library_movies.id \
                where movies_library_movies.name = %s)) ',
               \
               # 'select * from movies_library_directors as dir where not dir.name = %s and not exists (select 1 from movies_library_movies_genres as m_g inner join movies_library_movies mov \
               #  on m_g.movies_id = mov.id where mov.director_id = dir.id and m_g.genres_id not in (select movies_library_movies_genres.genres_id \
               #  from movies_library_movies_genres inner join movies_library_movies as _mov on movies_library_movies_genres.movies_id = _mov.id where %s in \
               #  (select movies_library_directors.name from movies_library_directors inner join movies_library_movies on movies_library_movies.director_id = movies_library_directors.id \
               #  where movies_library_movies.id = _mov.id)))'
               'select * from movies_library_directors where not movies_library_directors.name = %s and movies_library_directors.id not in \
                (select mov.director_id from movies_library_movies as mov inner join movies_library_movies_actors as m_a \
                on mov.id=m_a.movies_id where m_a.actors_id not in (select movies_library_movies_actors.actors_id from movies_library_movies_actors \
                inner join movies_library_movies on movies_library_movies_actors.movies_id = movies_library_movies.id where movies_library_movies.director_id\
                 in (select movies_library_directors.id from movies_library_directors where movies_library_directors.name = %s)))'
               ]

set_models = [Movies,Movies,Directors]


def GetHeadersNames(m):
    def get_w(f):
        if f.name == 'id':
            return 0
        if type(f) in [models.ForeignKey,models.ManyToOneRel]:
            return 2
        if type(f) in [models.ManyToManyField,models.ManyToManyRel]:
            return 3
        return 1

    return [j.name for j in sorted(m._meta.get_fields(),key=get_w) if type(j) != models.ManyToOneRel]

def GetHeadersTitleNames(m):
    return [i.replace('_',' ').title() for i in GetHeadersNames(m)]

def GetFields(m):
    def get_w(f):
        if f.name == 'id':
            return 0
        if type(f) in [models.ForeignKey,models.ManyToOneRel]:
            return 2
        if type(f) in [models.ManyToManyField,models.ManyToManyRel]:
            return 3
        return 1


    return [j for j in sorted(m._meta.get_fields(),key=get_w) if type(j) != models.ManyToOneRel]

def GetAttributeContainer(c):
    if type(c) == models.CharField:
        return '<input required type="text" class="form-control" name="%s">' % (c.name)
    if type(c) == models.DateField:
        return '<input required type="date" class="form-control" name="%s">' % (c.name)
    if type(c) == models.TimeField:
        return '<input required pattern="[0-9]{0,2}:[0-9]{0,2}:[0-9]{0,2}" class="form-control" name="%s">' % c.name
    if type(c) == models.ForeignKey:
        l = [i.name for i in c.rel.to.objects.all()]
        print(['<option value="%s">%s</option>'%(i,i) for i in l])
        options = ''.join(['<option value="%s">%s</option>'%(i,i) for i in l])
        return '<select class="form-control" required name="%s">%s</select>'%(c.name,options)
    if type(c) == models.ManyToManyField:
        l = [i.name for i in c.rel.to.objects.all()]
        print(['<option value="%s">%s</option>' % (i, i) for i in l])
        options = ''.join(['<option value="%s">%s</option>' % (i, i) for i in l])
        return '<select class="form-control" multiple="multiple" required name="%s">%s</select>' % (c.name, options)
    return '<input type="text">'

def GetFieldInputForm(name):
    l = []
    m = django.apps.apps.get_model('movies_library', name)
    v = [models.ManyToManyRel,models.ManyToOneRel]
    for i in m._meta.get_fields():
        if(i.name != 'id' and type(i) not in v):
            l += [{'name':i.name,'type':GetAttributeContainer(i)}]
    return l

def GetModelFields(m):
    data = [[] for i in range(len(m.objects.all()))]
    for i in range(len(m.objects.all())):
        for j in GetFields(m)[1:]:
             try:
                if(type(j) == models.ForeignKey):
                    s = ''
                    exec('s = m.objects.all()[i].%s.name' % j.name)
                    data[i] += [['text',[s]]]
                elif(type(j) == models.ManyToManyField ):
                    t = []
                    exec('t = [l.name for l in m.objects.all()[i].%s.all()]' % j.name)
                    data[i] += [['list',t]]
                elif(type(j) == models.ManyToManyRel):
                    t = []
                    exec('t = [l.name for l in m.objects.all()[i].%s_set.all()]' % j.name)
                    data[i] += [['list', t]]
                else:
                    s = ''
                    exec('s = m.objects.all()[i].%s' % j.name)
                    data[i] += [['text',[s]]]
             except:
                 data[i] += [['text',["--"]]]
    return [[m.objects.all()[i].id,data[i]] for i in range(len(data))]

def GetModelContext(request):
    name = request.GET.get('active')
    m = django.apps.apps.get_model('movies_library',name)
    return {'headers':GetHeadersTitleNames(m), 'elements':GetModelFields(m)}

def CreateModelField(data, m):
    _data = {}
    m_d = {}
    for i in data:
        t = m._meta.get_field(i)
        v = [models.ManyToManyField, models.ForeignKey]
        if(type(t) in v):
            l = []
            for j in data[i]:
                l += [t.rel.to.objects.get(name=j)]
            if type(t) == models.ForeignKey:
                _data[i] = l[0]
            else:
                m_d[i] = l
        else:
            _data[i] = data[i][0]
    t = m.objects.create(**_data)
    for i in m_d:
        for j in m_d[i]:
            eval('t.%s.add(j)'%(i))
    t.save()

def AddModelField(data):
    data = dict(data.iterlists())
    t = str(data.get('elem_table')[-1])
    m = django.apps.apps.get_model('movies_library',t)
    data.pop('elem_table',None)
    data = dict([(i,data[i]) for i in data])
    data.pop('csrfmiddlewaretoken',None)
    CreateModelField(data, m)

def ClearTable(name):
    m = django.apps.apps.get_model('movies_library', name)
    m.objects.all().delete()

def RemoveField(table, id):
    django.apps.apps.get_model('movies_library', table).objects.get(id=id).delete()

def GetQueryFields(query,name,m):
    t = c.cursor().execute(query, name)
    obj = t.fetchall()
    data = []
    for i in range(len(obj)):
        data += [[]]
        k = 0
        for j in GetFields(m)[1:]:
            k += 1
            try:
                if (type(j) == models.ForeignKey):
                    s = j.rel.to.objects.filter(id=obj[i][k])[0].name
                    data[i] += [['text', [s]]]
                elif (type(j) == models.ManyToManyField):
                    t = m.objects.filter(id=obj[i][0])[0]
                    exec('t = [l.name for l in t.%s.all()]' % j.name)
                    data[i] += [['list', t]]
                elif (type(j) == models.ManyToManyRel):
                    t = m.objects.filter(id=obj[i][0])[0]
                    exec('t = [l.name for l in t.%s_set.all()]' % j.name)
                    data[i] += [['list', t]]
                else:
                    data[i] += [['text', [obj[i][k]]]]
            except:
                data[i] += [['text', ["--"]]]
    return [[obj[i][0],data[i]] for i in range(len(data))]

def GetQueryContext(request):
    query = request.GET.get('query')
    if request.GET.get('name'):
        name = request.GET.get('name')
        ind = int(query) - 1
        m = _models[ind]
        return {'q_name': name, 'headers': GetHeadersTitleNames(m),
                'elements': GetQueryFields(queries[ind],[name],m)}
    return {}

def GetSetQueryContext(request):
    set_query = request.GET.get('set-query')
    if request.GET.get('name'):
        name = request.GET.get('name')
        ind = int(set_query) - 1
        m = set_models[ind]
        return {'q_name': name, 'headers': GetHeadersTitleNames(m),
                'elements': GetQueryFields(set_queries[ind], [name,name], m)}
    return {}


def GetContext(request):
    if request.GET.get('query'):
        query = request.GET.get('query')
        return dict({'active': '', 'query': query, 'set_query': ''},**GetQueryContext(request))
    if request.GET.get('set-query'):
        set_query = request.GET.get('set-query')
        return dict({'active': '', 'query':'', 'set_query': set_query},**GetSetQueryContext(request))
    if request.GET.get('active'):
        active = request.GET.get('active')
        return dict({'active':active,'query':'','set_query':''},**GetModelContext(request))
    return {'active': '', 'query':'', 'set_query': ''}

def SerializeField(obj,heads):
    data = []
    for i in heads:
        if (type(i) == models.ForeignKey):
            s = ''
            exec('s = obj.%s.name' % i.name)
            data += [s]
        elif (type(i) == models.ManyToManyField):
            t = []
            exec('t = [l.name for l in obj.%s.all()]' % i.name)
            data += [t]
        elif (type(i) == models.ManyToManyRel):
            t = []
            exec('t = [l.name for l in obj.%s_set.all()]' % i.name)
            data += [t]
        elif type(i) == models.ManyToOneRel:
            pass
        else:
            s = ''
            exec('s = obj.%s' % i.name)
            data += [s]
    return data

def GetFilledAtributeContainer(c,data):
    if type(c) == models.CharField:
        return '<input required type="text" class="form-control" name="%s" value="%s">' % (c.name,data)
    if type(c) == models.DateField:
        return '<input required type="date" class="form-control" name="%s" value="%s">' % (c.name,data)
    if type(c) == models.TimeField:
        return '<input required pattern="[0-9]{0,2}:[0-9]{0,2}:[0-9]{0,2}" class="form-control" name="%s" value="%s">' % (c.name,data)
    if type(c) == models.ForeignKey:
        l = [i.name for i in c.rel.to.objects.all()]
        options = ''.join(['<option value="%s" %s >%s</option>'%(i,'selected' if i in data else '',i) for i in l])
        return '<select class="form-control" required name="%s">%s</select>'%(c.name,options)
    if type(c) == models.ManyToManyField:
        l = [i.name for i in c.rel.to.objects.all()]
        options = ''.join(['<option value="%s" %s>%s</option>' % (i, 'selected' if i in data else '', i) for i in l])
        return '<select class="form-control" multiple="multiple" required name="%s">%s</select>' % (c.name, options)
    return '<input type="text">'

def GetFieldEditForm(model,id):
    l = []
    m = django.apps.apps.get_model('movies_library', model)
    v = [models.ManyToManyRel, models.ManyToOneRel]
    heads = m._meta.get_fields()
    data = SerializeField(m.objects.filter(id=id)[0],heads)
    for i in range(len(heads)):
        if (heads[i].name != 'id' and type(heads[i]) not in v):
            l += [{'name': heads[i].name, 'type': GetFilledAtributeContainer(heads[i],data[i])}]
    return l

def EditModelField(data,m):
    id = data.get('id')[0]
    data.pop('id',None)
    obj = m.objects.filter(id = id)[0]
    for i in m._meta.get_fields():
        if i.name != 'id':
            if (type(i) == models.ForeignKey):
                print(data.get(i.name))
                tmp = i.rel.to.objects.filter(name=data.get(i.name)[0])[0]
                exec('obj.%s = tmp'%i.name)
            elif (type(i) == models.ManyToManyField):
                l = data.get(i.name)
                tmp = [i.rel.to.objects.filter(name=l[j])[0] for j in range(len(l))]
                exec('obj.%s.clear' % i.name)
                for j in tmp:
                    exec('obj.%s.add(j)' % i.name)
            elif (type(i) in [models.ManyToOneRel,models.ManyToManyRel]):
                pass
            else:
                exec('obj.%s = data[i.name][0]' % i.name)
    obj.save()


def EditField(data):
    data = dict(data.iterlists())
    t = str(data.get('elem_table')[-1])
    m = django.apps.apps.get_model('movies_library', t)
    data.pop('elem_table', None)
    data = dict([(i, data[i]) for i in data])
    data.pop('csrfmiddlewaretoken', None)
    EditModelField(data, m)

def GetRemoveInfo(t_name,id):
    m = django.apps.apps.get_model('movies_library', t_name)
    log = ""
    v = [models.ManyToOneRel,models.ManyToManyField,models.ManyToManyRel]
    objs = m.objects.fiter(id=id)[0]
    f = [j for j in m._meta.get_field() if type(j) in v]
    for i in range(len(f)):
        l1 = []
        if (type(f) == v[0]):
            exec('l1 = [m.%s.name]' % f[i].name)
        if (type(f) == v[1]):
            exec('l1 = [i.name for i in m.%s]' % f[i].name)
        if (type(f) == v[2]):
            exec('l1 = [i.name for i in m.%s_set]' % f[i].name)
        for j in range(i + 1,len(f)):
            l2 = []
            if (type(f) == v[0]):
                exec('l2 = [m.%s.name]' % f[j].name)
            if (type(f) == v[1]):
                exec('l2 = [i.name for i in m.%s]' % f[j].name)
            if (type(f) == v[2]):
                exec('l2 = [i.name for i in m.%s_set]' % f[j].name)
                for k1 in l1:
                    for k2 in l2:
                        log += ['Reference %s to %s will be removed'.format(l1[k1],l2[k2])]
    return log