from django.contrib import admin

from .models import *

admin.site.register(Movies)
admin.site.register(Actors)
admin.site.register(Directors)
admin.site.register(Genres)
admin.site.register(Studios)

# Register your models here.
