from django.http import HttpResponseRedirect
from django.shortcuts import render,redirect
from django.http import HttpResponse
from movies_library.models import *
from movies_library.parse import *

def index(request):
    if(request.GET.get('clear')):
        ClearTable(request.GET.get('clear'))
        return HttpResponse('Success')
    if(request.POST.get('remove')):
        RemoveField(request.POST.get('active'), request.POST.get('id'))
        return HttpResponseRedirect('lib/?active=%s'%request.POST.get('active'))
    if(request.POST.get('edit')):
        return redirect(editField,model=request.POST.get('active'),id=request.POST.get('id'))
    return render(request,'index.html',GetContext(request))


def addField(request):
    if (not request.GET.get('table') and not request.POST.get('elem_table')):
        return HttpResponse('Missing table parameter' + str(dict(request.POST.iterlists())) + str(dict(request.GET.iterlists())))
    if request.POST.get('elem_table'):
        act = request.POST.get('elem_table')
        AddModelField(request.POST)
        return HttpResponseRedirect('/lib?active=%s'%act)
    return render(request,'add.html',{
        'col_list':GetFieldInputForm(request.GET.get('table')),
        'table':request.GET.get('table')
    })

def editField(request,model,id):
    id = int(id)
    if request.POST.get('elem_table'):
        act = request.POST.get('elem_table')
        EditField(request.POST)
        return HttpResponseRedirect('/lib?active=%s'%act)
    return render(request, 'edit.html', {
        'col_list': GetFieldEditForm(model,id),
        'table': model,
        'id':id
    })

def removeInfo(request):
    act = request.GET.get('table')
    id = int(request.GET.get('id'))
    return HttpResponse(GetRemoveInfo(act,id))
