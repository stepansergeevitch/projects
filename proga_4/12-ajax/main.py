import sys
from base64 import b64encode
from io import BytesIO
from os import path as osp

from flask import Flask, Markup, flash, redirect, render_template, request, url_for

sys.path.append(osp.abspath(osp.join(osp.realpath(__file__), '..', '..', '2-dms')))  # dirty
from dms import Database, Table


app = Flask(__name__)
app.config['SECRET_KEY'] = 'zfWYHv46ymmnk6p06oaTZl3ecPz4JHx7'
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024  # limit db file size to 16 MiB

db = None


def make_picture(bytes):
    return Markup(
        '<img style="max-height:200px; max-width:200px; height:auto; width:auto;" '  \
        + 'src="data:image/png;base64,%s" />' % b64encode(bytes.getvalue()).decode('utf-8')
    )
app.jinja_env.globals.update(make_picture=make_picture)


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')

    # handling POST

    if 'file' not in request.files:
        flash('У запиті відсутній файл.', 'danger')
        return redirect(request.url)

    file = request.files['file']
    if file is None or not file.filename or file.stream is None:
        flash('Помилка обробки файлу.', 'danger')
        return redirect(request.url)

    global db
    db = Database.load(file.stream, mode='stream')
    return redirect(url_for('display_table', table=db.tablenames[0]))


@app.route('/display')
def display_table():
    if db is None:
        flash('Спочатку завантажте базу даних.', 'warning')
        return redirect('/')
    tablename = request.args.get('table', type=str)
    if tablename is None or tablename not in db.tables:
        flash('Таблицю не знайдено.', 'warning')
        return redirect(url_for('display_table', table=db.tablenames[0]))
    return render_template(
        'table.html',
        table=db.tables[tablename],
        tablenames=db.tablenames,
    )


@app.route('/<tablename>/rename_column', methods=['GET', 'POST'])
def rename_column(tablename):
    if request.method == 'POST':
        if db is None:
            flash('Спочатку завантажте базу даних.', 'warning')
            return redirect('/')
        column_id = request.form.get('column_id', type=int)
        new_name = request.form.get('new_name', type=str)
        if tablename is None or tablename not in db.tablenames:
            flash('Таблицю не знайдено.', 'warning')
            return redirect(url_for('display_table', table=db.tablenames[0]))
        try:
            db.tables[tablename].rename_column(column_id, new_name)
            flash('Назва колонки успішно змінена.', 'success')
        except:
            flash('Помилка при зміні назви таблиці.', 'warning')
        return redirect(url_for('display_table', table=tablename))
    elif request.method == 'GET':
        return render_template(
            'rename_column.html',
            table=tablename,
        )
    


if __name__ == '__main__':
    app.run()
