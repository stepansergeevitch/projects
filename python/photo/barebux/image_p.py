from sklearn.cluster import KMeans
from skimage import img_as_float
from skimage.io import imread,imsave
import numpy as np

from photo.settings import *
from django.core.files import File


def mean_3(arr):
    a_1 = []
    a_2 = []
    a_3 = []

    for i in arr:
        a_1 += [i[0]]
        a_2 += [i[1]]
        a_3 += [i[2]]

    a_1 = sum(a_1) / len(a_1)
    a_2 = sum(a_2) / len(a_2)
    a_3 = sum(a_3) / len(a_3)
    return (a_1,a_2,a_3)

def parse_image(request):
    img = request.FILES['file']
    print(img.name)
    print('here')
    request.session['original_img'] = 'u_' + img.name
    f = File(img)
    with open(os.path.join(BASE_DIR,'static/images/') + 'u_' + img.name, 'wb+') as destination:
        for chunk in img.chunks():
            destination.write(chunk)
        destination.close()



def transform_and_save(request,n_cl = 3):
    if 'original_img' not in  request.session:
        return
    f_name = request.session['original_img']
    img_rgb = imread(os.path.join(BASE_DIR,'static/images/') + f_name)
    img = np.array(img_as_float(img_rgb))
    shape = img.shape
    data = [img[j][i] for j in range(shape[0]) for i in range(shape[1])]
    clust = KMeans(n_clusters=n_cl,init='k-means++',random_state=1)
    clust.fit(data)
    new_data = clust.predict(data)

    cl_count = [[] for i in range(n_cl)]
    for i in range(len(new_data)):
        cl_count[new_data[i]] += [data[i]]
    cl_count = [mean_3(i) for i in cl_count]
    print(cl_count)
    data = [cl_count[i] for i in new_data]
    img = np.array([[data[i + j * shape[1]] for i in range(shape[1])] for j in range(shape[0])])

    f_name = f_name[2::]
    imsave(os.path.join(BASE_DIR,'static/images/') + ('d_%d' + f_name)%n_cl,img)
    request.session['trans_img'] = ('d_%d' + f_name)%n_cl

