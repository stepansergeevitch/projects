from django.apps import AppConfig


class BarebuxConfig(AppConfig):
    name = 'barebux'
