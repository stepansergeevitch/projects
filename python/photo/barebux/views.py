from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from barebux.image_p import *
from barebux.forms import UploadImageForm

# Create your views here.

def index(request):
    if request.POST:
        if request.POST['purpose'] == 'load':
            parse_image(request)
        if request.POST['purpose'] == 'trans':
            transform_and_save(request,n_cl=int(request.POST['n_cl']))
    return HttpResponse(render(request,'index.html',{'original_image':'images/' + request.session['original_img'] if 'original_img'\
                                                     in request.session else '',
                                                     'trans_image': 'images/' + request.session[
                                                         'trans_img'] if 'trans_img' \
                                                    in request.session else ''
                                                     }))

