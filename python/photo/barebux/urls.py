from django.conf.urls import url
from barebux.views import index

urlpatterns = [
    url(r'', index),
]