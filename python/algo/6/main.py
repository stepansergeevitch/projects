class SegmentTree:
    def __init__(self, data):
        self.n = len(data)
        self.tree = [0] * 4 * self.n

        def build(v, l, r):
            if l == r:
                self.tree[v] = data[l]
            else:
                m = (l + r) // 2
                build(2 * v, l, m)
                build(2 * v + 1, m + 1, r)
                self.tree[v] = self.tree[2 * v] + self.tree[2 * v + 1]

        build(1, 0, self.n - 1)

    def query(self, l, r):
        def _query(v, lp, rp, l, r):
            if l > r:
                return 0
            if l == lp and r == rp:
                return self.tree[v]
            m = (lp + rp) // 2
            return _query(2 * v, lp, m, l, min(m, r)) + _query(2 * v + 1, m + 1, rp, max(l, m + 1), r)

        return _query(1, 0, self.n - 1, l, r)

    def update(self, pos, val):
        def _update(v, lp, rp):
            if lp == rp:
                self.tree[v] = val
            else:
                m = (lp + rp) // 2
                if pos <= m:
                    _update(2 * v, lp, m)
                else:
                    _update(2 * v + 1, m + 1, rp)
                self.tree[v] = self.tree[2 * v] + self.tree[2 * v + 1]

        _update(1, 0, self.n - 1)

    nodes = {}

    def write(self, v, lvl):
        if lvl not in self.nodes:
            self.nodes[lvl] = []
        self.nodes[lvl].append(self.tree[v])
        if 2 * v < 4 * self.n and 2 * v + 1 < 4 * self.n:
            self.write(2 * v, lvl + 1)
            self.write(2 * v + 1, lvl + 1)


A = [11, 24, 5, 7]
t = SegmentTree(A)
t.write(1, 0)
for i in range(3):
    print(t.nodes[i])
print(t.query(1, 3))
t.update(1, 100)
print(t.query(1, 2))