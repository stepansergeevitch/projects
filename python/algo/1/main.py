import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import distributions


def check_uniform(generator, a, b):
    sample_size = int(1e6)
    sample = np.array([generator.__next__() for i in range(sample_size)])
    n_cells = 100
    cell_step = (b - a) / n_cells
    cell_proba = [0] * n_cells
    for i in sample:
        cell_proba[min(int((i - a) / cell_step), n_cells - 1)] += 1
    chi_sq = sum(((i - sample_size / n_cells) / (sample_size / n_cells)) ** 2 for i in cell_proba)
    chi_sq_quantile = distributions.chi2.sf(chi_sq, n_cells)
    print("Chi square max proba: %f" % chi_sq_quantile)

    x = np.linspace(a, b, 1000)[:-1]
    y = [cell_proba[int((i - a) / cell_step)] / sample_size for i in x]
    plt.plot(x, y, '-r')
    plt.show()


def rand(_seed, _M, a, b):
    m = 16
    prev = _seed
    mod = (2 ** m) - 1
    while True:
        prev = (prev * _M) % mod
        yield a + (prev * (b - a) / (mod))

a, b = 0, 10
print("Enter seed and M: ")
seed, M = list(map(int, input().split()))
r = rand(seed, M, a, b)
for i in range(10):
    print(r.__next__())
check_uniform(r, a, b)


