import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import math


def frequency_test(seq, decision_rule=0.01):
    s_n = sum(2 * x - 1 for x in seq)
    s_observed = abs(s_n) / np.sqrt(len(seq))
    p_value = math.erfc(s_observed / np.sqrt(2))
    return p_value >= decision_rule

# def block_frequency_test(seq, block_length, decision_rule=0.01):
#     chunks = [seq[i:i + block_length] for i in range(0, len(seq) // block_length * block_length, block_length)]
#     ratios = [sum(chunk) / block_length for chunk in chunks]
#     chi_sq = 4 * block_length * sum((ratio - 0.5)**2 for ratio in ratios)
#     p_value = 1 - sp.special.gammainc(len(chunks) / 2, chi_sq / 2)
#     return p_value >= decision_rule


def spectral_test(seq, decision_rule=0.01):
    n = len(seq)
    X = [2 * x - 1 for x in seq]
    s = np.fft.fft(X)
    freq = np.absolute(s[:n // 2])
    plt.plot(freq)
    plt.title('Fourier transform frequencies')
    plt.show()
    T = np.sqrt(np.log(1 / 0.05) * n)
    n_0 = 0.95 * n / 2
    n_1 = len(np.where(freq < T)[0])
    d = (n_1 - n_0) / np.sqrt(n * 0.95 * 0.05 / 4)
    p_value = math.erfc(abs(d) / np.sqrt(2))
    return p_value >= decision_rule


def bin_seq_uniform():
    while True:
        yield 1 if np.random.uniform(0, 1) >= 0.5 else 0


def bin_seq_norm():
    while True:
        yield 1 if np.random.normal(0, 1) >= 0 else 0


set_size = 1000
bu = bin_seq_uniform()
bn = bin_seq_norm()

uniform_set = [bu.__next__() for i in range(set_size)]
norm_set = [bn.__next__() for i in range(set_size)]
fail_set = [1, 0] * (set_size // 2)

print(frequency_test(uniform_set), frequency_test(norm_set), frequency_test(fail_set))
print(spectral_test(uniform_set), spectral_test(norm_set), spectral_test(fail_set))
