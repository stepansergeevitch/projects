import scipy as sp


def bits(bytes):
    res = []
    for b in bytes:
        for i in range(8):
            res += [(b >> i) & 1]
    return res


# def group_bytes_by(bytes, batch_size):
#     i = 0
#     while(i + batch_size <= len(bytes)):
#         yield bytes[i : i + batch_size]
#         i += batch_size


def block_frequency_test(seq, block_length, decision_rule=0.01):
    chunks = [seq[i:i + block_length] for i in range(0, len(seq) // block_length * block_length, block_length)]
    ratios = [sum(chunk) / block_length for chunk in chunks]
    chi_sq = 4 * block_length * sum((ratio - 0.5)**2 for ratio in ratios)
    p_value = 1 - sp.special.gammainc(len(chunks) / 2, chi_sq / 2)
    return p_value >= decision_rule

f = open('audio.mp3', 'rb')
block_size = 8
bytes = f.read()[:100]
print(block_frequency_test(bits(bytes), block_size))


