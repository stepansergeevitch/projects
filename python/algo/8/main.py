class DSU:
    def __init__(self, n):
        self.n = n
        self.parent = list(range(n))
        self.size = [1] * n

    def rep(self, v):
        if v == self.parent[v]:
            return v
        self.parent[v] = self.rep(self.parent[v])
        return self.parent[v]

    def union(self, x, y):
        x, y = self.rep(x), self.rep(y)
        if x == y:
            return
        if self.size[x] < self.size[y]:
            x, y = y, x
        self.parent[y] = x
        self.size[x] += self.size[y]


def kruskal(edges, n):
    edges = sorted(edges, key=lambda x: x[2])
    dsu = DSU(n)
    res = 0
    for edge in edges:
        if dsu.rep(edge[0]) != dsu.rep(edge[1]):
            dsu.union(edge[0], edge[1])
            res += edge[2]
    return res

n, m = 4, 4
edges = [
        [1, 2, 1],
        [2, 3, 2],
        [3, 4, 5],
        [4, 1, 4]
]
kruskal(edges, n + 1)