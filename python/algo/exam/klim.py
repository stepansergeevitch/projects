arr = list(map(int, input().split()))
if len(arr) != 0:
    l = [arr[0]] * len(arr)
    for i in range(1, len(arr)):
        l[i] = l[i - 1] * arr[i]
    r = [arr[-1]] * len(arr)
    for i in range(len(arr) - 2, 0, -1):
        r[i] = r[i + 1] * arr[i]
    res = [0] * len(arr)
    for i in range(len(arr)):
        res[i] = (l[i - 1] if i > 0 else 1) * (r[i + 1] if i < len(arr) - 1 else 1)
    print(res)
else:
    print("Array of length 0")