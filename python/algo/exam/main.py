with open("testfile.txt") as inp:
    n = int(inp.readline())
    m = int(inp.readline())
    edges = set()
    for i in range(m):
        t = tuple(map(int, inp.readline().split()))
        edges.add((min(t), max(t)))

vertPow = {}
vertices = set([i for i in range(n)])
for i in edges:
    if i[0] not in vertPow:
        vertPow[i[0]] = set()
        vertPow[i[0]].add(i)
    else:
        vertPow[i[0]].add(i)
    if i[1] not in vertPow:
        vertPow[i[1]] = set()
        vertPow[i[1]].add(i)
    else:
        vertPow[i[1]].add(i)
print(vertPow)
i = 0
while i < len(vertices):
    k = list(vertices)[i]
    if len(vertPow[k]) == 2:
        print("Removing vertex with index %d" % k)
        l = list(vertPow[k])[0][0] if list(vertPow[k])[0][0] != k else list(vertPow[k])[0][1]
        r = list(vertPow[k])[1][0] if list(vertPow[k])[1][0] != k else list(vertPow[k])[1][1]
        vertPow[l].discard((l, k))
        vertPow[l].discard((k, l))
        vertPow[r].discard((r, k))
        vertPow[r].discard((k, r))
        vertPow[l].add((min(l, r), max(l, r)))
        vertPow[r].add((min(l, r), max(l, r)))
        edges.discard((list(vertPow[k])[0]))
        edges.discard((list(vertPow[k])[1]))
        edges.add((min(l, r), max(l, r)))
        vertices.discard(k)
        print("Vertices left", vertices)
        print("Edges left", edges)
        i = 0
    else:
        i += 1
