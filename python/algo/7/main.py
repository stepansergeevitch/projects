class FenwickTree:
    def __init__(self, n):
        self.n = n
        self.tree = [0] * n

    def query(self, l, r):
        def _query(pos):
            res = 0
            while pos >= 0:
                res += self.tree[pos]
                pos = (pos & (pos + 1)) - 1
            return res

        return _query(r) - _query(l - 1)

    def update(self, pos, val):
        while pos < self.n:
            self.tree[pos] += val
            pos = pos | (pos + 1)


t = FenwickTree(16)
for i in range(16):
    t.update(i, i)
print([i for i in range(16)])
print(t.tree)
print(t.query(0, 9))