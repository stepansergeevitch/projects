import numpy as np
from scipy.fftpack import fft2, ifft2
from PIL import Image
import sys


def blur_matrix(_n, _m, radius):
    res = np.zeros((_n, _m))
    for i in range(_n):
        for j in range(_m):
            if (i - radius)**2 + (j - radius)**2 <= radius**2:
                res[i, j] = 1 / (np.pi * radius**2)
    return res

def unfocus(_img, _r):
    img_n, img_m = _img.shape
    return ifft2(np.multiply(fft2(_img), fft2(blur_matrix(img_n, img_m, _r))))


def postprocess_image(img, type='bmp'):
    if type == 'png':
        return np.array([[[np.uint8(min(255, i)) for i in j] for j in k] for k in img])
    return np.array([[np.uint8(min(255, i)) for i in j] for j in img])


def restore_img(_r, _g, _b):
    res = np.zeros((_r.shape[0], _r.shape[1], 3))
    for i in range(_r.shape[0]):
        for j in range(_r.shape[1]):
            res[i, j, 0] = _r[i, j]
            res[i, j, 1] = _g[i, j]
            res[i, j, 2] = _b[i, j]
    return res


def split_img(_img):
    return _img[:, :, 0], _img[:, :, 1], _img[:, :, 2]


def focus(_img, _r):
    img_n, img_m = _img.shape
    return ifft2(fft2(_img) / fft2(blur_matrix(img_n, img_m, _r)))


def focus_err(_img):
    n, m = _img.shape
    err = 0
    for y in range(n):
        for x in range(m):
            e = 0
            c = 0
            if y > 0:
                e = e + abs(_img[y, x] - _img[y - 1, x])
                c = c + 1
            if y < n - 1:
                e = e + abs(_img[y, x] - _img[y + 1, x])
                c = c + 1
            if x > 0:
                e = e + abs(_img[y, x] - _img[y, x - 1])
                c = c + 1
            if x < m - 1:
                e = e + abs(_img[y, x] - _img[y, x + 1])
                c = c + 1
            err = err + e / c
    return err


def autofocus(_img, lr, rr):
    best_err = float('Inf')
    best_r = lr
    for i in range(lr, rr + 1):
        focused_img = focus(_img, i)
        cur_err = focus_err(focused_img)
        if cur_err < best_err:
            best_err = cur_err
            best_r = i
    return focus(_img, best_r), best_r


img_path = 'blur_input.bmp'
rad = 7
# img_path = 'ivan.png'
with Image.open(img_path) as im:
    image = np.array(im)
# r, g, b = split_img(image)
# ur = unfocus(r, rad)
# ug = unfocus(g, rad)
# ub = unfocus(b, rad)
# out = restore_img(ur, ug, ub)
# image_res = Image.fromarray(postprocess_image(out, type='png'))
# image_res.save
blured = unfocus(image, rad)
image_res = Image.fromarray(postprocess_image(blured))
image_res.save('blur_output.bmp')

autofocused, best_rad = autofocus(blured, 3, 10)
image_autof = Image.fromarray(postprocess_image(autofocused))
image_autof.save('autofocused_output.bmp')
print(best_rad)




