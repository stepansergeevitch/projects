import numpy as np
from scipy.integrate import odeint


eps = 1e-3


def fmap(fs, x):
    return np.array([f(*x) for f in fs])


def runge_kutta_sys_4(df, x, y0):
    h = x[1] - x[0]
    y = np.empty((len(x), len(y0)))
    y[0] = y0
    for i in range(1, len(x)):
        k1 = h * fmap(df, [x[i - 1], *y[i - 1]])
        k2 = h * fmap(df, [x[i - 1] + h / 2, *(y[i - 1] + k1 / 2)])
        k3 = h * fmap(df, [x[i - 1] + h / 2, *(y[i - 1] + k2 / 2)])
        k4 = h * fmap(df, [x[i - 1] + h, *(y[i - 1] + k3)])
        y[i] = y[i - 1] + (k1 + 2 * k2 + 2 * k3 + k4) / 6
    return y


def generate_a(_m1, _m2, _m3, _c1, _c2, _c3, _c4):
    _a = np.zeros((6, 6))
    _a[0, 1] = 1
    _a[1, 0] = -(_c2 + _c1) / _m1
    _a[1, 2] = _c2 / _m1
    _a[2, 3] = 1
    _a[3, 0] = _c2 / _m2
    _a[3, 2] = -(_c3 + _c2) / _m2
    _a[3, 4] = _c3 / _m2
    _a[4, 5] = 1
    _a[5, 2] = _c3 / _m3
    _a[5, 4] = -(_c4 + _c3) / _m3
    return _a


def get_y(_a, _t, _y0):
    fs = [lambda *a: np.dot(_a[i], a[1:]) for i in range(6)]
    return runge_kutta_sys_4(fs, _t, _y0)


def generate_b(_y, _m1, _m2, _m3, _c1, _c2, _c3, _c4):
    _b = np.zeros((_y.shape[0], 6, 3))
    _b[:, 1, 0] = -1/_m1 * _y[:, 0]
    _b[:, 1, 1] = (_c2 + _c1) / _m1**2 * _y[:, 0] - _c2 / _m1**2 * _y[:, 2]
    _b[:, 3, 2] = -_c2 / _m2**2 * _y[:, 0] + (_c3 + _c2) / _m2**2 * _y[:, 2] - _c3 / _m2**2 * _y[:, 4]
    return _b


def get_u(_a, _b, _t):
    h = _t[1] - _t[0]
    q, m, n = _b.shape
    _u = np.empty((q // 2 + q % 2, m, n))
    for i in range(n):
        fs = [lambda *a:([int(round(a[0] / h)), j, i] + np.dot(_a[j], np.array(a[1:]))) for j in range(m)]
        x = t[::2]
        y0 = np.zeros(m)
        _u[:, :, i] = runge_kutta_sys_4(fs, x, y0)
    return _u


def get_delta(_u, _y, _y_inp):
    q = _u.shape[0]
    t1 = np.zeros((3, 3))
    for i in range(q):
        t1 = t1 + np.dot(_u[i].transpose(), _u[i])
    t2 = np.zeros((3, 1))
    _y = _y[::2]
    _y_inp = _y_inp[::2]
    for i in range(q):
        t2 = t2 + np.dot(_u[i].T, np.reshape(_y_inp[i] - _y[i], (6, 1)))
    return np.dot(np.linalg.inv(t1), t2)


def get_diff(_y, _y_inp):
    q = _y.shape[0]
    ans = 0
    for i in range(q):
        ans = ans + np.dot(_y_inp[i] - _y[i], _y_inp[i] - _y[i])
    return ans


def get_beta(beta0, other, ys_ans, t, max_iter=100):
    beta = beta0
    y0 = ()#?
    for i in range(max_iter):
        a = generate_a(*beta, *other)
        ys = get_y(a, t, y0)
        err = get_diff(ys, ys_ans)
        if err < eps:
            break
        b = generate_b(ys, *beta, *other)
        u = get_u(a, b, t)
        delta = get_delta(u, ys, ys_ans)
        beta = beta + delta[:, 0]
    return beta


with open('data.txt') as in_f:
    y_inp = list(map(int, input().split()))
t_max = 50
t = np.linspace(0, t_max, len(y_inp))
