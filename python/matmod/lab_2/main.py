from PIL import Image
import numpy as np
from numpy.linalg import inv


def z_func(m, m_pinv):
    _e = np.zeros((m.shape[1], m.shape[1]))
    np.fill_diagonal(_e, 1)
    return _e - m_pinv @ m


def r_func(m_pinv):
    return m_pinv @ m_pinv.T


def pseudo_invert(a):
    """Greville inverse"""
    a_0 = a[0][np.newaxis].T
    cur_inv = a_0 / ((a_0.T @ a_0) if (a_0.T @ a_0) != 0 else 1)
    for i in range(1, len(a)):
        z = z_func(a[:i], cur_inv)
        a_v = a[i][np.newaxis].T
        if a_v.T @ z @ a_v == 0:
            cur_inv = np.hstack((cur_inv - np.divide((z @ a_v @ a_v.T @ cur_inv), (a_v.T @ z @ a_v)),
                                np.divide((z @ a_v), (a_v.T @ z @ a_v))))
        else:
            r = r_func(cur_inv)
            cur_inv = np.hstack((cur_inv - np.divide((r @ a_v @ a_v.T @ cur_inv), (1 + a_v.T @ r @ a_v)),
                                 np.divide((r @ a_v), (1 + a_v.T @ r @ a_v))))
    return cur_inv


def pseudo_invert_2(a):
    diff = np.inf
    eps = 1e-4
    sigm = 1
    cur, prev = 0, inv(a.T @ a + np.identity(a.T.shape[0])) @ a.T
    while diff > eps:
        cur = inv(a.T @ a + sigm * np.identity(a.T.shape[0])) @ a.T
        diff = np.linalg.norm(cur - prev)
        prev = cur
        sigm /= 2
    return cur


def postprocess_image(img):
    return np.array([[np.uint8(min(255, i)) for i in j] for j in img])

with Image.open('x1.bmp') as im:
    image_in = np.array(im)
with Image.open('y5.bmp') as im:
    image_out = np.array(im)
ext_in = np.vstack((image_in, np.full(image_in.shape[1], 1)))
pinv_ext_in = pseudo_invert(ext_in)
transform = image_out @ pinv_ext_in
my_res = transform @ ext_in
print(np.linalg.norm(image_out - my_res))
res_img = Image.fromarray(postprocess_image(my_res))
res_img.save('my_res.bmp')
