import numpy as np
from math import sin,pi
import matplotlib.pyplot as plt

t = 5
delta = 0.01


def apply(f, args):
    return np.matrix([[i(j) for i in f] for j in args])


def power(k):
    def inner(_x):
        return _x**k
    return inner


def freq_sin(k):
    def inner(_x):
        return sin(2 * pi * k * _x)
    return inner


def evaluate(val, f, coef):
    return np.sum(np.multiply(list(map(lambda _f: _f(val), f)), coef))


def fourier_freq(n, d):
    val = 1.0 / (n * d)
    results = np.empty(n, int)
    N = (n - 1) // 2 + 1
    p1 = np.arange(0, N, dtype=int)
    results[:N] = p1
    p2 = np.arange(-(n // 2), 0, dtype=int)
    results[N:] = p2
    return results * val


def fourier(y):
    n = len(y)
    res = np.array([np.complex(0)] * n)
    for i in range(n):
        res[i] += np.sum(y * np.exp((-2 * pi * i * np.array(range(n))) / (n * np.complex('j'))))
    res /= n
    return res


def get_peaks(_freq):
    eps = 1e-2
    grid = fourier_freq(len(_freq), delta)
    freq_ind = list(enumerate(_freq))[1:len(freq)//2]
    pred = lambda t: np.absolute(_freq[t[0]]) - np.absolute(_freq[t[0] - 1]) > eps \
        and np.absolute(_freq[t[0]]) - np.absolute(_freq[t[0] + 1]) > eps
    return list(map(lambda t: grid[t[0]], filter(pred, freq_ind)))


def fit(functions, _x, _y):
    m = apply(functions, x)
    coef = (np.linalg.inv(np.transpose(m) @ m) @ np.transpose(m) @ y).transpose()
    return np.squeeze(np.array(coef))


with open('data.txt') as file:
    y = list(map(float, file.read().split()))
# y = [i**3 for i in np.linspace(0, 5, 500)]
x = np.linspace(0, 5, len(y))
freq = fourier(y)
freq_peaks = get_peaks(freq)
func = [power(i) for i in range(4)] + [freq_sin(k) for k in freq_peaks]

coefs = fit(func, x, y)
print('Coef: %s'% str(coefs))
aprox_y = np.array([evaluate(i, func, coefs) for i in x])
aprox_y_0 = np.array([evaluate(i, func[:-len(freq_peaks)], coefs[:-len(freq_peaks)]) for i in x])
aprox_y_1 = np.array([evaluate(i, func[-len(freq_peaks):], coefs[-len(freq_peaks):]) for i in x])
fig = plt.figure()
# ax1 = fig.add_subplot(412)
# ax1.plot(x, y, '-r')
# ax2 = fig.add_subplot(421)
# ax2.plot(x, aprox_y_0, '-b')
# ax3 = fig.add_subplot(422)
# ax3.plot(x, aprox_y_1, '-g')
# ax4 = fig.add_subplot(413)
# ax4.plot(np.fft.fftfreq(len(freq),5/len(freq)), freq, '-k')
# ax5 = fig.add_subplot(414)
# ax5.plot(x, aprox_y, '-c')

ax1 = fig.add_subplot(311)
ax1.plot(x, y, '-r')
ax4 = fig.add_subplot(312)
ax4.plot(np.fft.fftfreq(len(freq),5/len(freq)), freq, '-k')
ax5 = fig.add_subplot(313)
ax5.plot(x, aprox_y, '-c')
plt.show()