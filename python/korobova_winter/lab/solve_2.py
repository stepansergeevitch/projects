import numpy as np

from functools import partial
from matplotlib import pyplot as plt
from scipy.optimize import fsolve

from solve_1 import get_demand_parameters, get_supply_parameters,\
					 function, input_data, selectors

def difference_function(f1, f2):
	def inner(value):
		return f1(value) - f2(value)

	return inner

def render_point(plot, point):
    plot.plot(
        point[selectors['demand']],
        point[selectors['price']],
    	'ro'
    )
    plot.plot(
        point[selectors['supply']],
    	point[selectors['price']],
    	'ro', color='b'
    )

def render_data(plot, data):
	list(map(partial(render_point, plot), data))

def render_function(plot, function):
	x_axis = np.arange(0.5, 6, 0.01)
	y_axis = function(x_axis)
	return plot.plot(y_axis, x_axis)

if __name__ == '__main__':

    demand_parameters = get_demand_parameters()
    supply_parameters = get_supply_parameters()

    demand_func = function(demand_parameters)
    supply_func = function(supply_parameters)
    diff_func = difference_function(demand_func, supply_func)

    render_data(plt, input_data)

    demand_plot = render_function(plt, demand_func)
    supply_plot = render_function(plt, supply_func)

    # equilibrium = fsolve(diff_func, )[0]
    plt.legend(
        [demand_plot, supply_plot],
        ['Попит', 'Пропозиція']
        # [plt_dem, plt_sup, plt_eq],
        # ['Попит', 'Пропозиція', 'Еквілібріум']
    )
    plt.xlabel('Кількість')
    plt.ylabel('Ціна')

    plt.show()
