import math
import numpy as np
from collections import namedtuple
from scipy.optimize import fsolve


input_data = (
    (1,    280, 5),
    (1.25, 245, 20),
    (1.57, 190, 51),
    (1.81, 141, 89),
    (2.09, 135, 120),
    (2.45, 110, 153),
    (2.8,  95,  180),
    (3.19, 65,  201),
    (3.58, 58,  215),
    (3.85, 44,  228),
    (4.5,  21,  240),
    (5,    10,  248),
)

selectors = {
    'price': 0,
    'demand': 1,
    'supply': 2
}

def demsup(value, selector, data):
    y = np.array([.0, .0, .0])
    price_selector = selectors['price']
    for p in data:
        t = value[0] + (value[1] * math.exp(value[2] * p[price_selector])) - p[selector]
        y += np.array((
            t,
            t * math.exp(value[2] * p[price_selector]),
            t * value[1] * p[price_selector] * math.exp(value[2] * p[price_selector])
        ))
    return y

def demand_criteria(data):
    def inner(value):
        return demsup(value, selectors['demand'], data)

    return inner

def supply_criteria(data):
    def inner(value):
        return demsup(value, selectors['supply'], data)

    return inner

def function(parameters):
    def inner(value):
        return parameters[0] +\
               parameters[1] * np.exp(parameters[2] * value)

    return inner

def approximate_function(criteria, func_builder, start_approx=None):
    # start_params = start_approx if start_approx is not None \
    #                             else (np.random.random([3]) * 1000)
    return fsolve(criteria, start_approx)

def get_demand_parameters():
    return approximate_function(
        demand_criteria(input_data),
        None,
        start_approx=np.array([-13.42, 964.3, -0.7344])
    )

def get_supply_parameters():
    return approximate_function(
        supply_criteria(input_data),
        None,
        start_approx=np.array([10.5, -1615.6, -0.4296])
    )



if __name__ == '__main__':
    demand_parameters = get_demand_parameters()
    supply_parameters = get_supply_parameters()

    print('demand:', demand_parameters)
    print('supply:', supply_parameters)


