import numpy as np
from matplotlib import pyplot as plt
from scipy.optimize import fsolve
import math

# варіант 15
# квота і еквівалентний податок
DATA = (
    # prc  dem  sup
    (6.5,  600, 200),
    (7.3,  550, 250),
    (8,    500, 275),
    (9.5,  450, 306),
    (11.5, 420, 375),
    (12,   390, 408),
    (13.4, 350, 486),
    (15,   328, 549),
    (16,   300, 625),
    (17.2, 250, 678),
    (18.7, 210, 700),
    (20,   198, 735),
    (21,   185, 750),
    (22.5, 180, 765)
)
PRICE, DEMAND, SUPPLY = 0, 1, 2


if __name__ == '__main__':

    def demsup(x, switch):
        y = [.0, .0, .0]
        for p in DATA:
            t = x[0] + (x[1] * math.exp(x[2] * p[PRICE])) - p[switch]
            y[0] += t
            y[1] += t * math.exp(x[2] * p[PRICE])
            y[2] += t * x[1] * p[PRICE] * math.exp(x[2] * p[PRICE])
        return [i * 2 for i in y]

    def demand(x):
        return demsup(x, DEMAND)

    def supply(x):
        return demsup(x, SUPPLY)

    params_dem = fsolve(demand, np.array([-13.42,   964.3, -0.07344]))  # voo
    params_sup = fsolve(supply, np.array([100.5, -1615.6, -0.04296]))  # doo

    print('demand:', params_dem)
    print('supply:', params_sup)

    def plotf(x, params):
        return params[0] + params[1] * np.exp(params[2] * x)

    def plotf_system(x):
        return plotf(x, params_dem) - plotf(x, params_sup)

    for point in DATA:
        plt.plot(point[DEMAND], point[PRICE], 'ro', color='b')
        plt.plot(point[SUPPLY], point[PRICE], 'ro')

    px = np.arange(5, 25, 0.01)
    dy = plotf(px, params_dem)
    sy = plotf(px, params_sup)
    plt_dem, = plt.plot(dy, px)
    plt_sup, = plt.plot(sy, px)

    eq = fsolve(plotf_system, np.array(11))[0]
    print('equi x:', eq)
    plt_eq, = plt.plot(plotf(eq, params_dem), eq, 'ro', color='g')

    plt.legend(
        [plt_dem, plt_sup, plt_eq],
        ['Попит', 'Пропозиція', 'Еквілібріум']
    )
    plt.xlabel('Кількість')
    plt.ylabel('Ціна')

    plt.show()

    def elasticity(p, params):
        dp = 1e-7
        dq = plotf(p + dp, params) - plotf(p, params)
        return (dq * p) / (dp * plotf(p, params))

    el_sup = elasticity(eq, params_sup)
    el_dem = elasticity(eq, params_dem)
    print('demand elasticity at the equilibrium:', el_dem)
    print('supply elasticity at the equilibrium:', el_sup)
    print('stable' if abs(el_dem) > abs(el_sup) else 'unstable')
