from functools import partial
from matplotlib import pyplot as plt
from scipy.optimize import fsolve

from lab.solve_1 import get_demand_parameters, get_supply_parameters,\
					 function, input_data, selectors

def difference_function(f1, f2):
	def inner(value):
		return f1(value) - f2(value)

	return inner

def render_point(plot, point):
    plot.plot(
    	point[selectors['demand']],
    	point[selectors['price']],
    	'ro'
    )
    plot.plot(
    	point[selectors['supply']],
    	point[selectors['price']],
    	'ro', color='b'
    )

def render_data(plot, data):
	list(map(partial(render_point, plot), data))

def render_function(plot, function):
	x_axis = np.arange(0, 6, 0.01)
	y_axis = function(x_axis)
	plot.plot(x_axis, y_axis)


demand_params = get_demand_parameters()
supply_parameters = get_supply_parameters()

demand_func = function(demand_params)
supply_func = function(supply_params)
diff_func = difference_function(demand_func, supply_func)

render_data(plt, input_data)

render_function(demand_func)
render_function(supply_func)

# equilibrium = fsolve(diff_func, )[0]



