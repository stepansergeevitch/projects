import numpy as np

eps = 1e-3

def checkLen(m):
	def inner(a, b):
		if a.size != b.size:
			raise ValueError(a.size, b.size)
		return m(a, b)
	return inner

@checkLen
def dist1(a, b):
	return np.sum(abs(a[i] - b[i] for i in range(len(a))))

@checkLen
def dist2(a, b):
	return np.sqrt(np.sum((a[i] - b[i]) ** 2 for i in range(len(a))))

@checkLen
def distInf(a, b):
	return np.max(abs(a[i] - b[i]) for i in range(len(a)))

methods = {
	'dist_1': dist1,
	'dist_2': dist2,
	'distInf': distInf
}

criteria = lambda x: x

def getIdealVector(vectors):
	global cmp
	return np.array([np.max(vectors[:,i]) for i in range(len(vectors))])

def maxIds(items, key=lambda a: a):
	if len(items) == 0:
		return []
	maxVal = key(items[0])
	ids = [0]
	for i in range(1, len(items)):
		if key(items[i]) - maxVal > eps:
			ids = [i]
			maxVal = key(items[i])
		elif abs(key(items[i]) - maxVal) < eps:
			ids.append(i)
		else:
			print((abs(key(items[i]) - maxVal), eps))
	return ids

n = int(input('Enter vectors count: '))
vectors = []
for i in range(n):
	vectors.append(list(map(int, input().split())))
vectors = np.array(vectors)
methodName = input(
	'Enter metric type,\navailable are: {}\n'\
	.format(', '.join(methods.keys()))
)
while methodName not in methods:
	methodName = input(
		'Wrong method type,\navailable are: {}\n'\
		.format(', '.join(methods.keys()))
	)
method = methods[methodName]
idealVector = getIdealVector(vectors)

resIds = maxIds([method(vectors[i], idealVector) for i in range(n)], key=lambda a: -a)
print(*(vectors[i] for i in resIds), sep='\n')