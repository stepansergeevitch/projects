from math import *
import numpy as np

# d_l = -10
# d_r = 10
# d_n = 40
# def f(x):
#     return sin(x)

d_l = -10
d_r = 10
d_n = 100
def f(x):
    return x * sin(x)

data = [0 for i in range(d_n + 1)]
i = 0
for i in range(d_n + 1):
    data[i] = (d_l + i * (d_r - d_l)/d_n,f(d_l + i* (d_r - d_l)/d_n))
    i += 1
d_r = np.arange(d_l,d_r,0.01)