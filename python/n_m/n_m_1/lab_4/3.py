import matplotlib.pyplot as plt
from common import *


def get_c(data):
    n = len(data)
    alpha = [0 for i in range(n - 1)]
    beta = [0 for i in range(n - 1)]
    A,B,C,F = (0,0,0,0)
    for i in range(1,n - 1):
        h_i = data[i][0] - data[i - 1][0]
        h_i1 = data[i + 1][0] - data[i][0]
        A = h_i
        C = 2. * (h_i + h_i1)
        B = h_i1
        F = 6. * ((data[i + 1][1] - data[i][1]) / h_i1 - (data[i][1] - data[i - 1][1]) / h_i)
        z = (A * alpha[i - 1] + C)
        alpha[i] = -B / z
        beta[i] = (F - A * beta[i - 1]) / z

    c = [0 for i in range(n)]
    c[n-1] = (F - A * beta[n - 2]) / (C + A * alpha[n - 2])
    for i in range(1,n-1)[::-1]:
        c[i] = alpha[i] * c[i + 1] + beta[i]
    return c


def get_spline(data):
    r = []
    n = len(data)
    _c = get_c(data)
    for i in range(1,n):
        h = data[i][0] - data[i-1][0]
        hy = data[i][1] - data[i-1][1]
        a = data[i][1]
        b = hy/h + (h/6) * (2 * _c[i] + _c[i-1])
        c = _c[i]/2
        d = ((_c[i] - _c[i - 1])/h) / 6
        k = np.poly1d([1,-data[i][0]])
        r.append(np.poly1d([a]) + b * k + c * np.polymul(k,k) + d * np.polymul(np.polymul(k,k),k))
    return r



res = get_spline(data)

x = [data[i][0] for i in range(len(data))]
y = [data[i][1] for i in range(len(data))]
plt.plot(x,y,'-wo')


y = [f(i) for i in x]
plt.plot(x,y,'--')

for i in range(len(data) - 1):
    x = np.arange(data[i][0],data[i + 1][0],0.01)
    y = [np.polyval(res[i],j) for j in x]
    plt.plot(x,y)

plt.show()



