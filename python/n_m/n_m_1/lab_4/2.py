import matplotlib.pyplot as plt
from common import *


def get_dif(data):
    n = len(data)
    res = []
    tmp = [data[i][1] for i in range(n)]
    res += [tmp[0]]
    for i in range(1,n):
        next = [(tmp[j + 1] - tmp[j])/(data[j + i][0] - data[j][0]) for j in range(n-i)]
        tmp = next
        res += [tmp[0]]
    return res

def get_N(data):
    n = len(data)
    d = get_dif(data)
    res = np.poly1d([0])
    for i in range(n):
        t = np.poly1d([1])
        for j in range(i):
            t = np.polymul(t,np.poly1d([1,-data[j][0]]))
        res += d[i] * t
    return res

def pairs(arr):
    for i in range(int(len(arr)/2)):
        yield arr[2 * i:2 * i + 2]

# n = int(input())
# data = list(pairs(list(map(int,input().split()))))
# data = sorted(data,key=lambda a:a[0])
# data = [(i,sin(i)) for i in range(10)]
# res = get_N(data)
#
# print(res)
# for i in data:
#     print('f(%f) = %f'%(i[0],np.polyval(res,i[0])),'Given y = %f'%(i[1]))
#
# x = [data[i][0] for i in range(len(data))]
# y = [data[i][1] for i in range(len(data))]
# plt.plot(x,y,'-wo')
#
#
# x = np.arange(0,10,0.01)
# y = [np.polyval(res,i) for i in x]
# plt.plot(x,y)
# plt.show()

res = get_N(data)

print(res)
for i in data:
    print('f(%f) = %f'%(i[0],np.polyval(res,i[0])),'Given y = %f'%(i[1]))

x = [data[i][0] for i in range(len(data))]
y = [data[i][1] for i in range(len(data))]
plt.plot(x,y,'-wo')

x = d_r
y = [f(i) for i in x]
plt.plot(x,y,'--')

x = d_r
y = [np.polyval(res,i) for i in x]
plt.plot(x,y)

plt.show()

