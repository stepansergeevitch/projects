import matplotlib.pyplot as plt
from common import *


def get_L(data):
    n = len(data) - 1
    p = np.poly1d([0])
    for i in range(n + 1):
        tmp = np.poly1d([1])
        for j in range(n + 1):
            if(j != i):
                tmp = np.polymul(tmp,np.poly1d([1/(data[i][0] - data[j][0]),-data[j][0]/(data[i][0] - data[j][0])]))
        p += tmp * data[i][1]
    return p

def pairs(arr):
    for i in range(int(len(arr)/2)):
        yield arr[2 * i:2 * i + 2]

# n = int(input())
# data = list(pairs(list(map(int,input().split()))))
# data = [(i,sin(i)) for i in range(10)]
# res = getL(data
# print(res)
# for i in data:
#     print('f(%f) = %f'%(i[0],np.polyval(res,i[0])),'Given y = %f'%(i[1]))
# x = [data[i][0] for i in range(len(data))]
# y = [data[i][1] for i in range(len(data))]
# plt.plot(x,y,'-wo')
#
#
# x = np.arange(0,10,0.01)
# y = [np.polyval(res,i) for i in x]
# plt.plot(x,y)
# plt.show()

res = get_L(data)

print(res)
for i in data:
    print('f(%f) = %f'%(i[0],np.polyval(res,i[0])),'Given y = %f'%(i[1]))

x = [data[i][0] for i in range(len(data))]
y = [data[i][1] for i in range(len(data))]
plt.plot(x,y,'-wo')

x = np.arange(-5,5,0.01)
y = [f(i) for i in x]
plt.plot(x,y,'--')

x = np.arange(-5,5,0.01)
y = [np.polyval(res,i) for i in x]
plt.plot(x,y)

plt.show()
#5
#1 2 4 2 5 6 3 5 7 4
