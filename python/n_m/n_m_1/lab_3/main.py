from matr import *


eps = 1e-7

def pageRank(m):
    n, cur_eps = len(m), eps + eps
    x = np.full(n, 1 / n)
    count = 0
    while cur_eps > eps:
        #m = m @ m
        print(x)
        next_x = x @ m
        x, cur_eps = next_x, sum(abs(next_x - x))
        count += 1
    return x,count

n = 10
m = rank_mod(norm_r(np.array(make_m(n,diag=True))))
# list([print(i)for i in m])
res,n = pageRank(m)
print("Number of iterations:%d"%n)
print(res)