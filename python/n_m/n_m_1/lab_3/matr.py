import math,random
import numpy as np

def make_m(n,sym=False,diag=False):
    if not sym:
        m = [[random.betavariate(0.5,0.5) * n for i in range(n)] for j in range(n)]
        if (diag):
            for i in range(n):
                m[i][i] += sum([abs(m[i][j]) for j in range(n)])
        return m

    else:
        m = [[0 for i in range(n)] for j in range(n)]
        for i in range(n):
            for j in range(i,n):
                m[i][j] = m[j][i] = random.random()*n
            if(diag):
                m[i][i] += sum([abs(m[i][j]) for j in range(n)])
        return m


def make_m_h(n):
    return [[1/(i + j + 1) for i in range(n)]for j in range(n)]

def make_b(m,n):
    x = [i + 1 for i in range(n)]
    return [sum([m[i][j] * x[j] for j in range(n)])for i in range(n)]

def norm_r(m):
    for i in m:
        l = sum(i)
        for j in range(len(i)):
            i[j] /= l
    return m

def rank_mod(m,a=0.85):
    return a * m + (1 - a) * np.full_like(m,1/len(m))