import numpy as np
import random as r
from math import *
import matplotlib.pyplot as plt

pow = 2
n = 101
d_l = -4
d_r = 4

def f(x):q
    return sin(x) + r.random()


_x = np.arange(d_l,d_r,(d_r - d_l + 1)/n)
y = [f(i) for i in _x]
m = np.matrix([[_x[i] ** j for j in range(pow + 1)[::-1]] for i in range(len(_x))])
mt = np.matrix([[_x[i] ** j for j in range(pow + 1)[::-1]] for i in range(len(_x))]).transpose()
t = mt @ m
x = np.linalg.inv(t) @ mt @ y

res = np.poly1d(np.squeeze(np.asarray(x)))

plt.plot(_x,y,'-wo')

x = np.arange(d_l,d_r,0.01)
y = [np.polyval(res,i) for i in x]
plt.plot(x,y)
plt.show()

