from matr import *

n = 3
m = make_m(n,diag=True)
b = make_b(m,n)
x = [b[i]/m[i][i] for i in range(n)]
eps = 1e-6
delta = eps + 1
it = 0
while delta > eps:
    delta = 0
    it += 1
    for i in range(n):
        t = x[i]
        x[i] = (b[i] - (sum([m[i][j] * x[j] for j in range(n)]) - x[i] * m[i][i]))/m[i][i]
        delta = max(delta,math.fabs(x[i] - t))

print("Iterations:",it)
print("Solution:",x)