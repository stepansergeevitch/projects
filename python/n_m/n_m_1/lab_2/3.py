from matr import *

n = 3
m = make_m(n,diag=True)
b = make_b(m,n)
x = [b[i]/m[i][i] for i in range(n)]
eps = 1e-6
delta = eps + 1
it = 0
while delta > eps:
    delta = 0
    it += 1
    x_p = x[:]
    for i in range(n):
        x[i] = (b[i] - (sum([m[i][j] * x_p[j] for j in range(n)]) - x_p[i] * m[i][i]))/m[i][i]
        delta = max(delta,math.fabs(x[i] - x_p[i]))

print("Iterations:",it)
print("Solution:",x)