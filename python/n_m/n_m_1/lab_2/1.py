from matr import *

def diagonalize(m,b,n):
    for i in range(n):
        for j in range(n):
            if(i != j):
                koef = m[j][i]/m[i][i]
                for k in range(n):

                    m[j][k] -= koef * m[i][k]
                b[j] -= b[i] * koef
    for i in range(n):
        b[i] /= m[i][i]
        m[i][i] = 1
    return m,b

n = 20
m = make_m_p(n)
b = make_b(m,n)
m,b = diagonalize(m,b,n)

print(b)