import numpy as np
from matr import *


def get_u(m, n):
    result = np.zeros((n, n))
    result[0][0] = np.sqrt(m[0][0])
    result[0][1:] = m[0][1:] / result[0][0]
    for i in range(1, n):
        result[i][i] = np.sqrt(m[i][i] - np.sum(np.square([result[k][i] for k in range(i)])))
        if i + 1 < n:
            delta = np.sum([np.multiply(result[k][i], result[k][i + 1:]) for k in range(i)])
            result[i][i + 1:] = (m[i][i + 1:] - delta) / result[i][i]
    return result, np.transpose(result)


def solve_t_d(m, b, n):
    result = np.copy(b)
    result[0] /= m[0][0]
    for i in range(1, n):
        result[i] -= np.sum(np.multiply(result[:i], m[i][:i]))
        result[i] /= m[i][i]
    return result


def solve_t_u(m, b, n):
    result = np.copy(b)
    result[-1] /= m[-1][-1]
    for i in range(n - 2, -1, -1):
        result[i] -= np.sum(np.multiply(result[i + 1:], m[i][i + 1:]))
        result[i] /= m[i][i]
    return result

dim = 3
matrix = make_m(dim, sym=True, diag=True)
bias = make_b(matrix, dim)
u, u_t = get_u(matrix, dim)
y = solve_t_d(u_t, bias, dim)
x = solve_t_u(u, y, dim)
print("y: %s\nSolution: %s" % (str(y), str(x)))

