from math import fabs


def f(_x):
    return _x ** 3 - 5 * _x ** 2 + 4 * _x + 0.092


def df(_x):
    return 3 * _x ** 2 - 10 * _x + 4


def get_zero(i, x):
    return (i, x) if fabs(f(x)) <= eps else get_zero(i + 1, x - f(x) / l)

initial_x, eps = 2, 1e-6
l = df(initial_x) * 10
iter_count, result = get_zero(0, initial_x)
print("Iterations: %d\nSolution: %f\nf(x) = %f" %
      (iter_count, result, f(result)))