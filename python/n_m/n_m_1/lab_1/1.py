from math import fabs


def f(_x):
    return _x ** 3 - 5 * _x ** 2 + 4 * _x + 0.092


def get_zero_on_range(i, _a, _b):
    return (i, _a if fabs(f(_a)) <= eps else _b) if (fabs(f(_a)) <= eps or fabs(f(_b)) <= eps) else \
            get_zero_on_range(i + 1, _a, (_a + _b) / 2) if f(_a) * f((_a + _b) / 2) <= 0 \
            else get_zero_on_range(i + 1, (_a + _b) / 2, _b)

a, b, eps = -2, 4, 1e-6
(f(a) * f(b) > 0 and (print("Wrong range") or exit()))
iter_count, result = get_zero_on_range(0, a, b)
print("Iterations: %d\nSolution: %f\nf(x) = %f" %
      (iter_count, result, f(result)))