import numpy as np
import matplotlib.pyplot as plt


def y_ans(x):
    return np.sin(x)


def fmap(fs, x):
    return np.array([f(*x) for f in fs])


def euler(df, x, y0):
    h = x[1] - x[0]
    y = np.empty_like(x)
    y[0] = y0
    for i in range(1, len(x)):
        y[i] = y[i - 1] + h * df(x[i - 1], y[i - 1])
    return y


def runge_kutta_4(df, x, y0):
    h = x[1] - x[0]
    y = np.empty_like(x)
    y[0] = y0
    for i in range(1, len(x)):
        k1 = h * df(x[i - 1], y[i - 1])
        k2 = h * df(x[i - 1] + h / 2, y[i - 1] + k1 / 2)
        k3 = h * df(x[i - 1] + h / 2, y[i - 1] + k2 / 2)
        k4 = h * df(x[i - 1] + h, y[i - 1] + k3)
        y[i] = y[i - 1] + (k1 + 2 * k2 + 2 * k3 + k4) / 6
    return y


def runge_kutta_sys_4(df, x, y0):
    h = x[1] - x[0]
    y = np.empty((len(x), len(y0)))
    y[0] = y0
    for i in range(1, len(x)):
        k1 = h * fmap(df, [x[i - 1], *y[i - 1]])
        k2 = h * fmap(df, [x[i - 1] + h / 2, *(y[i - 1] + k1 / 2)])
        k3 = h * fmap(df, [x[i - 1] + h / 2, *(y[i - 1] + k2 / 2)])
        k4 = h * fmap(df, [x[i - 1] + h, *(y[i - 1] + k3)])
        y[i] = y[i - 1] + (k1 + 2 * k2 + 2 * k3 + k4) / 6
    return y

a, b, n = 0, 10, 500
y0 = 0
x = np.linspace(a, b, n)
dy = lambda x, y: np.cos(x)
eul_y = euler(dy, x, y0)
rk_y = runge_kutta_4(dy, x, y0)
fig = plt.figure()
pl_1 = fig.add_subplot(211)
pl_1.plot(x, y_ans(x))
pl_1.plot(x, eul_y)
pl_1.plot(x, rk_y)
pl_1.legend(['original', 'euler', 'runge-kutta 4'], loc='best')
pl_1.set_title('Euler vs Runge-Kutta 4')

pp1, pp2, pp3, pp4 = 0.5, 5/6, 1, 3
rk_a, rk_b, rk_n = 0, 30, 500
rk_dx = lambda t, x, y: pp1 * x - pp2 * x * y
rk_dy = lambda t, x, y: pp3 * x * y - pp4 * y
t = np.linspace(rk_a, rk_b, rk_n)
y0 = np.array([1, 2])
z = runge_kutta_sys_4((rk_dx, rk_dy), t, y0)

pl_2 = fig.add_subplot(212)
pl_2.set_xlabel('time (sec)')
pl_2.set_ylabel('population')
pl_2.plot(t, z[:, 0], t, z[:, 1])
pl_2.legend(['prey', 'predator'], loc='best')
pl_2.set_title('Predator-prey')

plt.show()