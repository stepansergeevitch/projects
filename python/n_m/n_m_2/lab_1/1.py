import numpy as np
from math import pi
import matplotlib.pyplot as plt


def f(x):
    return np.sin(x) + 1


def f_0(f, a):
    def inner(b):
        x = np.linspace(a, b, n)
        d = (b - a)/(n - 1)
        return sum(d * f(i) for i in x)
    return inner


def f_1(f, a):
    def inner(b):
        x = np.linspace(a, b, n)
        d = (b - a) / (n - 1)
        return sum(d * (f(x[i]) + f(x[i + 1])) / 2 for i in range(len(x) - 1))
    return inner


def f_2(f, a):
    def inner(b):
        x = np.linspace(a, b, n)
        return sum((x[i + 1] - x[i]) * (f(x[i]) + 4 * f((x[i] + x[i + 1])/2) + f(x[i + 1]))/6 for i in range(len(x) - 1))
    return inner

a, b, n = 0, 10, 20

x = np.linspace(a, b, 1000)
y = list(map(f, x))
y_0 = list(map(f_0(f, a), x))
y_1 = list(map(f_1(f, a), x))
y_2 = list(map(f_2(f, a), x))

plt.plot(x,y, 'r')
plt.plot(x,y_0, 'g')
plt.plot(x,y_1, 'b')
plt.plot(x,y_2, 'y')

plt.show()

print('Rectange method (%f, %f): %f' % (a, b, f_0(f, a)(b)))
print('Trapezium method (%f, %f): %f' % (a, b, f_1(f, a)(b)))
print('Simpson method (%f, %f): %f' % (a, b, f_2(f, a)(b)))