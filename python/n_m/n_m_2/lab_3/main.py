import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
from mpl_toolkits.mplot3d import Axes3D

coef = 1

def thomas_method(a, b, c, d):
    n = len(d)
    A = np.empty_like(d)
    B = np.empty_like(d)
    A[0] = -c[0] / b[0]
    B[0] = d[0] / b[0]
    for i in range(1, n):
        A[i] = -c[i] / (b[i] + a[i] * A[i - 1])
        B[i] = (d[i] - a[i] * B[i - 1]) / (b[i] + a[i] * A[i - 1])
    y = np.empty_like(d)
    y[n - 1] = B[n - 1]
    for i in range(n - 2, -1, -1):
        y[i] = A[i] * y[i + 1] + B[i]
    return y


def explicit_method(_h1, _h2, _n, _m, _u0, _y0, _y1, _f):
    ans = np.empty((_n, _m))
    ans[:, 0] = _u0
    ans[0, :] = _y0
    ans[_n - 1, :] = _y1
    global coef
    for j in range(1, _m):
        for i in range(1, _n - 1):
            ans[i, j] = ans[i, j - 1] + _h2 * ((ans[i + 1, j - 1] - 2 * ans[i, j - 1] + ans[i - 1, j - 1]) / (coef * _h1**2)
                                               + _f[i, j - 1])
    return ans


def implicit_method(_h1, _h2, _n, _m, _u0, _y0, _y1, _f):
    y = np.empty((_n, _m))
    y[:, 0] = _u0
    y[0, :] = _y0
    y[_n - 1, :] = _y1
    global coef
    for j in range(1, _m):
        a = np.ones(_n - 2) * 1 / (coef * _h1 ** 2)
        a[0] = 0
        b = np.ones(_n - 2) * -(1 / _h2 + 2 / (coef * _h1 ** 2))
        c = np.ones(_n - 2) * 1 / (coef * _h1 ** 2)
        c[-1] = 0
        d = -y[1:-1, j - 1] / _h2 - _f[1:-1, j]
        d[0] = d[0] - _y0[j] / (coef * _h1 ** 2)
        d[-1] = d[-1] - _y1[j] / (coef * _h1 ** 2)
        y[1:-1, j] = thomas_method(a, b, c, d)
    return y


class Nav:
    cur_exp_t, cur_imp_t = 0, 0
    max_t = 5

    @classmethod
    def init(cls):
        global x_v, y_exp, y_imp

        cls.fig = plt.figure(figsize=plt.figaspect(2.))
        # fig.suptitle('Explicit and implicit schemas')
        cls.ax_exp = cls.fig.add_subplot(2, 1, 1)
        cls.ax_exp.plot(x_v, y_exp[cls.cur_exp_t], '-b')
        cls.ax_exp.set_xlabel('position')
        cls.ax_exp.set_ylabel('temp')
        cls.ax_exp.set_title('Explicit')

        cls.ax_imp = cls.fig.add_subplot(2, 1, 2)
        cls.ax_imp.plot(x_v, y_imp[cls.cur_imp_t], '-b')
        cls.ax_imp.set_xlabel('position')
        cls.ax_imp.set_ylabel('temp')
        cls.ax_imp.set_title('Implicit')

        cls.ax_exp.set_ylim([-1, cls.max_t])
        cls.ax_imp.set_ylim([-1, cls.max_t])

        ax_exp_next = plt.axes([0.7, 0.55, 0.05, 0.02])
        ax_exp_prev = plt.axes([0.81, 0.55, 0.05, 0.02])
        ax_imp_next = plt.axes([0.7, 0.05, 0.05, 0.02])
        ax_imp_prev = plt.axes([0.81, 0.05, 0.05, 0.02])

        bt_next_exp = Button(ax_exp_next, '+')
        bt_next_exp.on_clicked(cls.exp_next)
        bt_prev_exp = Button(ax_exp_prev, '-')
        bt_prev_exp.on_clicked(cls.exp_prev)
        bt_next_imp = Button(ax_imp_next, '+')
        bt_next_imp.on_clicked(cls.imp_next)
        bt_prev_imp = Button(ax_imp_prev, '-')
        bt_prev_imp.on_clicked(cls.imp_prev)

        cls.fig.tight_layout()
        plt.show()

    @classmethod
    def exp_next(cls, event):
        global x_v, y_exp, M

        if(cls.cur_exp_t < M - 1):
            cls.cur_exp_t += 1

        cls.ax_exp.clear()
        cls.ax_exp.plot(x_v, y_exp[cls.cur_exp_t], '-b')
        cls.ax_exp.set_ylim([-1, cls.max_t])
        cls.ax_exp.set_xlabel('position')
        cls.ax_exp.set_ylabel('temp')
        cls.ax_exp.set_title('Explicit')
        cls.fig.canvas.draw()

    @classmethod
    def exp_prev(cls, event):
        global x_v, y_exp

        if (cls.cur_exp_t > 0):
            cls.cur_exp_t -= 1

        cls.ax_exp.clear()
        cls.ax_exp.plot(x_v, y_exp[cls.cur_exp_t], '-b')
        cls.ax_exp.set_ylim([-1, cls.max_t])
        cls.ax_exp.set_xlabel('position')
        cls.ax_exp.set_ylabel('temp')
        cls.ax_exp.set_title('Explicit')
        cls.fig.canvas.draw()

    @classmethod
    def imp_next(cls, event):
        global x_v, y_imp, M

        if (cls.cur_imp_t < M - 1):
            cls.cur_imp_t += 1

        cls.ax_imp.clear()
        cls.ax_imp.plot(x_v, y_imp[cls.cur_imp_t], '-b')
        cls.ax_imp.set_ylim([-1, cls.max_t])
        cls.ax_imp.set_xlabel('position')
        cls.ax_imp.set_ylabel('temp')
        cls.ax_imp.set_title('Implicit')
        cls.fig.canvas.draw()

    @classmethod
    def imp_prev(cls, event):
        global x_v, y_imp

        if (cls.cur_imp_t > 0):
            cls.cur_imp_t -= 1

        cls.ax_imp.clear()
        cls.ax_imp.plot(x_v, y_imp[cls.cur_imp_t], '-b')
        cls.ax_imp.set_ylim([-1, cls.max_t])
        cls.ax_imp.set_xlabel('position')
        cls.ax_imp.set_ylabel('temp')
        cls.ax_imp.set_title('Implicit')
        cls.fig.canvas.draw()


T, N, M = 10, 40, 80
x_v = np.linspace(0, 1, N)
t_v = np.linspace(0, T, M)
x, t = np.meshgrid(x_v, t_v, sparse=True)
h1, h2 = x_v[1] - x_v[0], t_v[1] - t_v[0]
# u0, y0, y1, f = x, np.zeros(M), (1/2) * t_v + 1, 3 * t * np.sin(10 * x)
u0, y0, y1, f = [i/N for i in range(N)], np.full(M, 0), 2 * np.sin(t_v) + 2, 3 * t * np.sin(10 * x)#np.full_like(np.full_like(t, 1) * np.sin(np.pi * x), 1)


y_ans = x**2 / 2 * t + x
y_exp = explicit_method(h1, h2, N, M, u0, y0, y1, f.transpose()).transpose()
y_imp = implicit_method(h1, h2, N, M, u0, y0, y1, f.transpose()).transpose()

Nav.init()
# fig_exp = plt.figure(figsize=(15, 10))
# ax = fig_exp.gca(projection='3d')
# ax.plot_wireframe(x, t, y_exp)
# ax.set_xlabel('position')
# ax.set_ylabel('time')
# ax.set_zlabel('temp')
# ax.set_title('Explicit')
#
# fig_exp = plt.figure(figsize=(15, 10))
# ax = fig_exp.gca(projection='3d')
# ax.plot_wireframe(x, t, y_imp)
# ax.set_xlabel('position')
# ax.set_ylabel('time')
# ax.set_zlabel('temp')
# ax.set_title('Implicit')



# fig = plt.figure(figsize=plt.figaspect(2.))
# fig.suptitle('Explicit and implicit schemas')
# ax = fig.add_subplot(2, 1, 1, projection='3d')
# ax.plot_wireframe(x, t, y_exp)
# ax.set_xlabel('position')
# ax.set_ylabel('time')
# ax.set_zlabel('temp')
# ax.set_title('Explicit')
#
# ax = fig.add_subplot(2, 1, 2, projection='3d')
# ax.plot_wireframe(x, y_imp)
# ax.set_xlabel('position')
# ax.set_ylabel('time')
# ax.set_zlabel('temp')
# ax.set_title('Implicit')
# fig = plt.figure(figsize=plt.figaspect(2.))
# fig.suptitle('Explicit and implicit schemas')
# ax_exp = fig.add_subplot(2, 1, 1)
# ax_exp.plot(x, y_exp[cur_exp_t])
# ax_exp.set_xlabel('position')
# ax_exp.set_ylabel('time')
# ax_exp.set_zlabel('temp')
# ax_exp.set_title('Explicit')
#
# ax_imp = fig.add_subplot(2, 1, 2)
# ax_imp.plot(x, y_imp[cur_imp_t])
# ax_imp.set_xlabel('position')
# ax_imp.set_ylabel('time')
# ax_imp.set_zlabel('temp')
# ax_imp.set_title('Implicit')


